import { Injectable } from '@angular/core';
import {Router, CanActivate, CanActivateChild} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()

export class AdminGuard implements CanActivate, CanActivateChild {
    key: any;
    constructor(
        private authService: AuthService,
        private router: Router
    ) {}

    canActivate() {
        if (this.authService.isManager()) {
            return true;
        } else {
            this.router.navigate(['/dashboard']);
            return false;
        }
    }
    canActivateChild() {
        return this.authService.getUserProfile().map((res) => {
            if (res.accountLevel >= 9) {
                console.log(true);
                return true;
            }
            console.log(false);
            this.router.navigate(['/dashboard']);
            return false
        });
    }
}
