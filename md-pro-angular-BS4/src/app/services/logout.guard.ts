import { Injectable } from '@angular/core';
import {Router, CanActivate} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()

export class LogoutGuard implements CanActivate {
    constructor(
        private authService: AuthService,
        private router: Router
    ) {}

    canActivate() {
        if (this.authService.loggedIn()) {
            this.router.navigate(['/dashboard']);
            return false;
        } else {
            return true;
        }
    }
}
