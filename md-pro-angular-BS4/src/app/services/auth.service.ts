import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {

    // localhost: String = 'http://localhost:9999/';
    localhost: String = '';

    authToken: any;
    user: any;

    constructor(private http: Http) { }

    registerUser(user) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'users/register', user, {headers: headers}).map(res => res.json());
    }
    depositMoney(user) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'users/deposite', user, {headers: headers}).map(res => res.json());
    }

    changePassword(changePassBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'users/changepassword', changePassBundle, {headers: headers}).map(res => res.json());
    }
    resetUserPassword(resetPassBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'users/resetpassword', resetPassBundle, {headers: headers}).map(res => res.json());
    }
    storeUserData(token, user) {
        window.localStorage.setItem('id_token', token);
        window.localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    }

    getErrLogs() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'logs/err', {headers: headers}).map(res => res.json());
    }
    getUserProfile() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'users/profile', {headers: headers}).map(res => res.json());
    }
    testConnection() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'users/botsvtestconnection', {headers: headers}).map(res => res.json());
    }
    getAutoTymPrice() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'reactionserv/getprice', {headers: headers}).map(res => res.json());
    }
    getBuffLikePrice() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'bufflikeserv/getprice', {headers: headers}).map(res => res.json());
    }
    getCommentPrice() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'commentserv/getprice', {headers: headers}).map(res => res.json());
    }
    getReactionRunningList() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'reactionserv/getlist', {headers: headers}).map(res => res.json());
    }
    getCommentBundleList() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'commentserv/getcommentbundlelist', {headers: headers}).map(res => res.json());
    }
    getBuffLikeRunningList() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'bufflikeserv/getlist', {headers: headers}).map(res => res.json());
    }

    getCommentProfileRunningList() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'commentserv/getprofilelinklist', {headers: headers}).map(res => res.json());
    }

    updateAutoTymPrice(updateBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'reactionserv/updateprice', updateBundle, {headers: headers}).map(res => res.json());
    }
    updateCommentServPrice(updateBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/updateprice', updateBundle, {headers: headers}).map(res => res.json());
    }
    updateCommentProfileLink(updateBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/updateprofilelink', updateBundle, {headers: headers}).map(res => res.json());
    }
    updateBuffLikePrice(updateBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'bufflikeserv/updateprice', updateBundle, {headers: headers}).map(res => res.json());
    }
    addReactionService(reactionBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'reactionserv/reactions', reactionBundle, {headers: headers}).map(res => res.json());
    }
    giaHanReactionService(reactionBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'reactionserv/giahan', reactionBundle, {headers: headers}).map(res => res.json());
    }
    getCheckPoints() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'logs/checkpoints',{headers: headers}).map(res => res.json());
    }

    addBuffLikeService(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'bufflikeserv/bufflike', bufflikeBundle, {headers: headers}).map(res => res.json());
    }

    buyModeBuffLikeService(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'bufflikeserv/renewservice', bufflikeBundle, {headers: headers}).map(res => res.json());
    }

    addCommentService(commentBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/buynew', commentBundle, {headers: headers}).map(res => res.json());
    }
    addCommentBundle(commentBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/addnewcommentbundle', commentBundle, {headers: headers}).map(res => res.json());
    }
    updateCommentBundle(commentBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/updatecommentbundle', commentBundle, {headers: headers}).map(res => res.json());
    }
    updateReactionServBundle(reactionBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'reactionserv/update', reactionBundle, {headers: headers}).map(res => res.json());
    }
    updateBuffLikeBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'bufflikeserv/update', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    pauseReactionServBundle(reactionBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'reactionserv/pause', reactionBundle, {headers: headers}).map(res => res.json());
    }
    pauseBuffLikeServBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'bufflikeserv/pause', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    pauseCommentProfileServBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/pauseprofilelink', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    unPauseReactionServBundle(reactionBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'reactionserv/unpause', reactionBundle, {headers: headers}).map(res => res.json());
    }
    unPauseBuffLikeServBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'bufflikeserv/unpause', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    unPauseCommentProfileServBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/unpauseprofilelink', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    deleteCommentBundleServBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/removebundle', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    giaHanCommentBundleServBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/giahan', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    muaThemPostCommentBundleServBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/muathempost', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    muaThemCommentCommentBundleServBundle(bufflikeBundle) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        headers.append('content-Type', 'application/json');
        return this.http.post(this.localhost + 'commentserv/muathemcomment', bufflikeBundle, {headers: headers}).map(res => res.json());
    }
    getUserInfo(user) {
        const headers = new Headers();
        this.loadToken();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', this.authToken);
        return this.http.post(this.localhost + 'users/getinfo', user, {headers: headers}).map(res => res.json());
    }

    getAccountLogs() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'logs/accountlog', {headers: headers}).map(res => res.json());
    }

    getMoneyLogs() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'logs/moneylog', {headers: headers}).map(res => res.json());
    }

    loadToken() {
        const token = localStorage.getItem('id_token');
        this.authToken = token;
    }

    isManager() {
        return this.getUserProfile().map(profile => {
            this.user = profile.user;
                // console.log(this.user);
            return true;
        },
            err => {
            return false;
            }
            );
    }
    isAdmin() {
        this.getUserProfile().subscribe(profile =>{
            this.user = profile.user;
        });
        return this.user.accountLevel > 9;
    }

    isSuperAdmin() {
        this.getUserProfile().subscribe(profile =>{
            this.user = profile.user;
        });
        return this.user.accountLevel > 99;
    }
    loggedIn() {
        const token1 = window.localStorage.getItem('id_token');
        if (token1 == null) {
            return false;
        } else {
            return tokenNotExpired('id_token');
        }
    }

    getAccType() {
        const headers = new Headers();
        this.loadToken();
        headers.append('Authorization', this.authToken);
        return this.http.get(this.localhost + 'services/role', {headers: headers}).map(res => res.json());
    }

    authenticateUser(user) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.localhost + 'users/authen', user, {headers: headers}).map(res => res.json());
    }

    logout() {
        this.authToken = null;
        this.user = null;
        window.localStorage.clear();
    }
}
