import { Injectable } from '@angular/core';
import {Router, CanActivate, CanActivateChild} from '@angular/router';
import {AuthService} from './auth.service';

@Injectable()

export class ManagerGuard implements CanActivate, CanActivateChild {
    key: any;
    constructor(
        private authService: AuthService,
        private router: Router
    ) {}

    canActivate() {
        return this.authService.getUserProfile().map((res) => {
            if (res.accountLevel >= 6) {
                return true;
            }
            console.log(false);
            this.router.navigate(['/dashboard']);
            return false;
        });
    }
    canActivateChild() {
        return this.authService.getUserProfile().map((res) => {
            if (res.accountLevel >= 6) {
                return true;
            }
            this.router.navigate(['/dashboard']);
            return false;
        });
    }
}
