import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';
import {Router} from '@angular/router';

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    level: number;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
    level: number;
}

//Menu Items
export const ROUTES: RouteInfo[] = [{
        path: '/dashboard',
        title: 'Dashboard',
        type: 'link',
        level: 1,
        icontype: 'dashboard'
    },{
        path: '/manage',
        title: 'Công Cụ Quản Lý',
        type: 'sub',
        level: 6,
        icontype: 'apps',
        collapse: 'manage',
        children: [
            {path: 'superadmin', title: 'SuperAdmin Tool', ab:'done', level: 99},
            {path: 'logs', title: 'Logs', ab:'done', level: 99},
            {path: 'checkpoints', title: 'Check Points', ab:'done', level: 6},
            {path: 'createaccount', title: 'Tạo Tài Khoản Khách Hàng', ab:'done', level: 1},
            {path: 'deposit', title: 'Nạp Tiền Vào Tài Khoản', ab:'done', level: 1}
        ]
    },{
        path: '/iservi',
        title: 'Tương Tác',
        type: 'sub',
        level: 1,
        icontype: 'thumb_up_alt',
        collapse: 'iservi',
        children: [
            {path: 'autotym', title: 'Tự Động Thả Tym', ab:'done', level: 1},
            {path: 'bufflike', title: 'Tăng Like Bài Viết', ab:'done', level: 1},
            {path: 'comments', title: 'Tăng Bình Luận', ab:'done', level: 1}
        ]
    },{
        path: '/tables',
        title: 'Live Stream',
        type: 'sub',
    level: 1,
        icontype: 'remove_red_eye',
        collapse: 'tables',
        children: [
            {path: 'regular', title: 'Tăng Mắt', ab:'undone', level: 1},
            {path: 'extended', title: 'Bình Luận Thông Minh', ab:'undone', level: 1}
        ]
    },{
        path: '/maps',
        title: 'SPAM',
        type: 'sub',
    level: 1,
        icontype: 'reply_all',
        collapse: 'maps',
        children: [
            {path: 'google', title: 'Share Bài Không Link', ab:'done', level: 5},
            {path: 'fullscreen', title: 'Share Bài Có Link', ab:'undone', level: 1},
            {path: 'vector', title: 'SPAM link', ab:'undone', level: 1}
        ]
    }
    // , {
    //     path: '/admin/tools',
    //     title: 'Super Admin Tools',
    //     type: 'link',
    //     level: 99,
    //     icontype: 'widgets'
    //
    // }
    // ,{
    //     path: '/charts',
    //     title: 'Charts',
    //     type: 'link',
    //     icontype: 'timeline'
    //
    // },{
    //     path: '/calendar',
    //     title: 'Calendar',
    //     type: 'link',
    //     icontype: 'date_range'
    // },{
    //     path: '/pages',
    //     title: 'Pages',
    //     type: 'sub',
    //     icontype: 'image',
    //     collapse: 'pages',
    //     children: [
    //         {path: 'pricing', title: 'Pricing', ab:'P'},
    //         {path: 'timeline', title: 'Timeline Page', ab:'TP'},
    //         {path: 'login', title: 'Login Page', ab:'LP'},
    //         {path: 'register', title: 'Register Page', ab:'RP'},
    //         {path: 'lock', title: 'Lock Screen Page', ab:'LSP'},
    //         {path: 'user', title: 'User Page', ab:'UP'}
    //     ]
    // }
];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {

    public menuItems: any[];
    username: String;
    userProfile: any;
    accountLevel: Number;
    money: any;
    constructor(private authService: AuthService,
                private router: Router) {}
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        this.authService.getUserProfile().subscribe( profile => {
            this.userProfile = profile;
            this.accountLevel = profile.accountLevel;
            this.money = this.moneySeparate(profile.money.toFixed(2));
            this.username = profile.username;
        });
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            let ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
    showSwal(type) {
        if (type == 'undone') {
            swal({
                title: "Comming Soon!",
                text: "Chức Năng Chưa Sẵn Sàng AE Quay Lại Sau Nhé!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        } else if (type == 'maintenance') {
            swal({
                title: "Chức Năng Đang Bảo Trì!",
                text: "Bảo Trì Chút Thôi AE Chờ Chút :D",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
    }
    onLogoutClick(){swal({
        title: 'Bạn Muốn Đăng Xuất?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        confirmButtonText: 'Chắc Chắn',
        buttonsStyling: false
    }).then((result) => {
        if (result.value) {
            this.authService.logout();
            this.router.navigate(['pages/login']);
            return false;
        }
    })
    }

    moneySeparate(amount) {
        const parts = amount.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
}
