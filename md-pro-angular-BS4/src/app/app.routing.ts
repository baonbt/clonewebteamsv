import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth.guard';
import {SuperadminGuard} from './services/superadmin.guard';

export const AppRoutes: Routes = [
    {
      path: '',
        canActivate: [AuthGuard],
      redirectTo: 'dashboard',
      pathMatch: 'full',
    }, {
      path: '',
      component: AdminLayoutComponent,
        canActivate: [AuthGuard],
      children: [
          {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    }, {
        path: 'manage',
        loadChildren: './components/components.module#ComponentsModule'
    }
    , {
        path: 'iservi',
        loadChildren: './forms/forms.module#Forms'
    }
    // , {
    //     path: 'tables',
    //     loadChildren: './tables/tables.module#TablesModule'
    // }
    , {
        path: 'maps',
        loadChildren: './maps/maps.module#MapsModule'
    }
    // , {
    //     path: 'admin',
    //           canActivate: [SuperadminGuard],
    //     loadChildren: './widgets/widgets.module#WidgetsModule'
    // }
    // , {
    //     path: 'charts',
    //     loadChildren: './charts/charts.module#ChartsModule'
    // }
    // , {
    //     path: 'calendar',
    //     loadChildren: './calendar/calendar.module#CalendarModule'
    // }

    // , {
    //     path: '',
    //     loadChildren: './timeline/timeline.module#TimelineModule'
    // }
  ]}
  , {
      path: '',
      component: AuthLayoutComponent,
      children: [{
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
      }]
    }
];
