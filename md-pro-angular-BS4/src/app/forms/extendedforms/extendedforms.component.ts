import {Component, OnInit, ElementRef, AfterViewInit} from '@angular/core';
import { DateAdapter } from '@angular/material';
import {AuthService} from '../../services/auth.service';
import swal from "sweetalert2";
import {DataTableDirective} from "angular-datatables";
import {Subject} from "rxjs";


declare const require: any;

declare const $: any;

@Component({
    selector: 'app-extendedforms-cmp',
    templateUrl: 'extendedforms.component.html',
    styles: [`md-calendar {
      width: 300px;
  }`]
})

export class ExtendedFormsComponent implements OnInit , AfterViewInit{
    public dataTable: any;
    rushHourList: any = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'];
    skipRate: Number = 0;
    likeRate: Number = 0;
    loveRate: Number = 100;
    hahaRate: Number = 0;
    wowRate: Number = 0;
    sadRate: Number = 0;
    angryRate: Number = 0;
    duration: String = 'oneMonth';
    likeAmount: String;
    selectedPageSize: Number = 10;
    currentPage: Number  = 1;
        basePrice: any = 0;
        fiftyLikeDiscount: any;
        oneHundredLikeDiscount: any;
        oneMonthDiscount: any;
        oneThousandTwoHundredLikeDiscount: any;
        sixHundredLikeDiscount: any;
        sixMonthDiscount: any;
        threeHundredLikeDiscount: any;
    twoHundredLikeDiscount: any;
        threeMonthDiscount: any;
        totalPrice: String;
        totalDiscount: String;
        finalPrice: String;
        period: any;
        likeNum: any;
    totalPriceNum: any;
    likeDiscountNum: any;
    durationDiscountNum: any;
    totalDiscountNum: any;
    finalPriceNum: any;
    profileLink: String;
    editMode: boolean  = false;
    editingId: String;
    maxPerDay: any = 10;
    rushHour: any = [];
    buffDuration: any = 1;
    registerNewMode: boolean = false;

    buyMoreLike: any = 0;
    buyMorePost: any = 0;
    buyMoreMode: boolean = false;
    expiredTime: any = 0;
    maxMaxPerDay: any = 10;
    maxLikeNum: any = 0;
    editLikeNum: any = 0;

    constructor(
        private authService: AuthService
    ) {}
    ngOnInit() {
        this.dataTable = {
            dataRows: []
        };

    }

    onEditLikeNumeChange(){
        if(this.editLikeNum === undefined || this.editLikeNum === null || this.editLikeNum ===''){
            this.editLikeNum = 0;
        } else if(this.editLikeNum > this.maxLikeNum){
            this.editLikeNum = this.maxLikeNum;
        }

    }

    addnewOnTableButton(){
        this.registerNewMode = true;
        this.buyMoreMode = false;
        this.editMode = false;

        this.likeNum = 0;
        this.skipRate = 0 ;
        this.likeRate = 0;
        this.loveRate = 100;
        this.hahaRate = 0;
        this.wowRate = 0;
        this.sadRate = 0;
        this.angryRate = 0;
        this.profileLink = undefined;
        this.maxPerDay = 10;
        this.rushHour = [];
        this.editingId = undefined;
        this.duration = 'oneMonth';
        this.totalDiscountNum = 0;
        this.totalPrice = '0';
        this.finalPrice = '0';
    }

    pretyReactionRate(reactionRate) {
        const segment = reactionRate.split(';');
        let newReactionRate = '';
        segment.forEach(x => {
            const y = x.replace('0=', '' ).replace('1=', '' )
                .replace('2=', '' ).replace('3=', '' )
                .replace('4=', '' ).replace('5=', '' )
                .replace('6=', '' );
            if (newReactionRate.length > 0 ) {
                newReactionRate = newReactionRate + '-' + y;
            } else {
                newReactionRate = y;
            }
        });
        return newReactionRate;
    }
    onSelectChange() {
        if (this.duration === 'oneMonth') {
            this.period = 1;
            this.durationDiscountNum = this.oneMonthDiscount;
        } else if (this.duration === 'threeMonth') {
            this.period = 3;
            this.durationDiscountNum = this.threeMonthDiscount;
        } else if (this.duration === 'sixMonth') {
            this.period = 6;
            this.durationDiscountNum = this.sixMonthDiscount;
        }

        if (this.likeNum >= 1200) {
            this.likeDiscountNum = this.oneThousandTwoHundredLikeDiscount;
        } else if (this.likeNum >= 600) {
            this.likeDiscountNum = this.sixHundredLikeDiscount;
        } else if (this.likeNum >= 300) {
            this.likeDiscountNum = this.threeHundredLikeDiscount;
        } else if (this.likeNum >= 200) {
            this.likeDiscountNum = this.twoHundredLikeDiscount;
        } else if (this.likeNum >= 100) {
            this.likeDiscountNum = this.oneHundredLikeDiscount;
        } else if (this.likeNum >= 50) {
            this.likeDiscountNum = this.fiftyLikeDiscount;
        } else {
            this.likeDiscountNum = 0;
        }


        this.totalPriceNum = this.period * this.likeNum * this.basePrice;
        this.totalDiscountNum = this.likeDiscountNum + this.durationDiscountNum;
        if(this.maxPerDay > 10 ){
            this.totalPriceNum = this.totalPriceNum*(this.maxPerDay/10);
        }
        this.finalPriceNum = this.totalPriceNum - this.totalDiscountNum * this.totalPriceNum;
        this.totalPrice = this.moneySeparate(this.totalPriceNum.toFixed(2));
        this.finalPrice = this.moneySeparate(this.finalPriceNum.toFixed(2));
    }

    onBuyMoreChange(){
        if(this.buyMorePost === undefined || this.buyMorePost === null || this.buyMorePost == ''){
            this.buyMorePost = 0;
        }
        if(this.buyMoreLike === undefined || this.buyMoreLike === null || this.buyMoreLike == ''){
            this.buyMoreLike = 0;
        }

        if (this.duration === 'oneMonth') {
            this.period = 1;
        } else if (this.duration === 'threeMonth') {
            this.period = 3;
        } else if (this.duration === 'sixMonth') {
            this.period = 6;
        } else if (this.duration === 'zero') {
            this.period = 0;
        }

        const remainingPeriod = this.expiredTime/30;
        if (this.period + remainingPeriod >= 6) {
            this.durationDiscountNum = this.sixMonthDiscount;
        } else if (this.period + remainingPeriod >= 3) {
            this.durationDiscountNum = this.threeMonthDiscount;
        } else if (this.period + remainingPeriod >= 1) {
            this.durationDiscountNum = this.oneMonthDiscount;
        } else if (this.period + remainingPeriod >= 0) {
            this.durationDiscountNum = 0;
        }

        if (this.likeNum + this.buyMoreLike >= 1200) {
            this.likeDiscountNum = this.oneThousandTwoHundredLikeDiscount;
        } else if (this.likeNum + this.buyMoreLike >= 600) {
            this.likeDiscountNum = this.sixHundredLikeDiscount;
        } else if (this.likeNum + this.buyMoreLike >= 300) {
            this.likeDiscountNum = this.threeHundredLikeDiscount;
        } else if (this.likeNum + this.buyMoreLike >= 200) {
            this.likeDiscountNum = this.twoHundredLikeDiscount;
        } else if (this.likeNum + this.buyMoreLike >= 100) {
            this.likeDiscountNum = this.oneHundredLikeDiscount;
        } else if (this.likeNum + this.buyMoreLike >= 50) {
            this.likeDiscountNum = this.fiftyLikeDiscount;
        } else {
            this.likeDiscountNum = 0;
        }

        if(this.period == 0 && this.buyMoreLike == 0 && this.buyMorePost > 0){
            this.totalPriceNum = remainingPeriod * this.likeNum * this.basePrice*(this.buyMorePost/10);
        }
        if(this.period == 0 && this.buyMoreLike > 0 && this.buyMorePost == 0){
            this.totalPriceNum = remainingPeriod * this.buyMoreLike * this.basePrice*(this.maxMaxPerDay/10);
        }
        if(this.period == 0 && this.buyMoreLike > 0 && this.buyMorePost > 0){
            this.totalPriceNum = remainingPeriod * this.likeNum * this.basePrice*(this.buyMorePost/10);
            this.totalPriceNum = this.totalPriceNum + remainingPeriod * this.buyMoreLike * this.basePrice*(this.buyMorePost/10 + this.maxMaxPerDay/10);
        }
        if(this.period > 0 && this.buyMoreLike == 0 && this.buyMorePost == 0){
            this.totalPriceNum = this.period*this.likeNum*this.basePrice*(this.maxMaxPerDay/10);
        }
        if(this.period > 0 && this.buyMoreLike == 0 && this.buyMorePost > 0){
            this.totalPriceNum = remainingPeriod * this.likeNum * this.basePrice*(this.buyMorePost/10);
            this.totalPriceNum = this.totalPriceNum + this.period * this.likeNum * this.basePrice*(this.buyMorePost/10+this.maxMaxPerDay/10);
        }
        if(this.period > 0 && this.buyMoreLike > 0 && this.buyMorePost == 0){
            this.totalPriceNum = remainingPeriod * this.buyMoreLike * this.basePrice*(this.maxMaxPerDay/10);
            this.totalPriceNum = this.totalPriceNum + this.period * (this.likeNum +this.buyMoreLike)* this.basePrice*(this.maxMaxPerDay/10);
        }
        if(this.period > 0 && this.buyMoreLike > 0 && this.buyMorePost > 0){
            this.totalPriceNum = remainingPeriod * this.likeNum * this.basePrice*(this.buyMorePost/10);
            this.totalPriceNum = this.totalPriceNum + (this.period+remainingPeriod) * (this.likeNum +this.buyMoreLike)* this.basePrice*(this.buyMorePost/10+this.maxMaxPerDay/10);
        }

        this.totalDiscountNum = this.likeDiscountNum + this.durationDiscountNum;
        this.finalPriceNum = this.totalPriceNum - this.totalDiscountNum * this.totalPriceNum;

        if(this.period == 0 && this.buyMoreLike == 0 && this.buyMorePost == 0){
            this.finalPriceNum = 0;
            this.totalPriceNum = 0;
            this.totalDiscountNum = 0;
        }

        this.totalPrice = this.moneySeparate(this.totalPriceNum.toFixed(2));
        this.finalPrice = this.moneySeparate(this.finalPriceNum.toFixed(2));

    }

    onAddNewClick() {
        if (this.profileLink === undefined || this.profileLink === null || this.profileLink === '') {
            swal({
                title: "Fill In ProfileLink Field!",
                text: "Hãy Nhập ProfileLink!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop);
            return false;
        }

        if (this.likeNum === undefined || this.likeNum === null || this.likeNum === '') {
            swal({
                title: "Fill In Like Amount Field!",
                text: "Hãy Nhập Số Like Bạn Muốn!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop);
            return false;
        }

        if (this.skipRate === undefined || this.skipRate === null) {
            this.skipRate = 0;
        }
        if (this.likeRate === undefined || this.likeRate === null) {
            this.likeRate = 0;
        }
        if (this.loveRate === undefined || this.loveRate === null) {
            this.loveRate = 0;
        }
        if (this.hahaRate === undefined || this.hahaRate === null) {
            this.hahaRate = 0;
        }
        if (this.wowRate === undefined || this.wowRate === null) {
            this.wowRate = 0;
        }
        if (this.sadRate === undefined || this.sadRate === null) {
            this.sadRate = 0;
        }
        if (this.angryRate === undefined || this.angryRate === null) {
            this.angryRate = 0;
        }

        let tempRh = '';

        if(this.rushHour != undefined || this.rushHour != null || this.rushHour != ''){
            for(let rhh of this.rushHour){
                if(tempRh.length == 0){
                    tempRh = rhh;
                } else {
                    tempRh = tempRh + ';' + rhh;
                }
            }
        }

        const reactionRateString = '0=' + this.skipRate + ';1=' + this.likeRate + ';2=' + this.loveRate + ';3=' + this.hahaRate + ';4=' + this.wowRate + ';5=' + this.sadRate + ';6=' + this.angryRate;
        const newDataBundle = {
            profileLink: this.profileLink,
            likeNum: this.likeNum,
            reactionRate: reactionRateString,
            period: this.duration,
            delay: this.buffDuration,
            rushHour: tempRh,
            maxPerDay: this.maxPerDay
        };
        swal({
            title: 'Xác Nhận',
            text: 'Thanh Toán ' + this.finalPrice ,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.addBuffLikeService(newDataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        ).then((_result)=>{
                        });
                        this.loadBuffLikeRunningList();
                        this.profileLink = undefined;
                        this.editMode = false;
                        this.buyMoreMode = false;
                        this.registerNewMode = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

    buyModeButton(){
        if (this.duration === 'zero' && this.buyMoreLike == 0 && this.buyMorePost == 0) {
            swal({
                title: "Bạn Không Mua Thêm Gì À?",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop);
            return false;
        }

        const newDataBundle = {
            id: this.editingId,
            buyMoreLike: this.buyMoreLike,
            buyMorePost: this.buyMorePost,
            buyMoreDuration: this.duration
        };
        swal({
            title: 'Xác Nhận',
            text: 'Thanh Toán ' + this.finalPrice ,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.buyModeBuffLikeService(newDataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        ).then((_result)=>{
                        });
                        this.loadBuffLikeRunningList();
                        this.profileLink = undefined;
                        this.editMode = false;
                        this.buyMoreMode = false;
                        this.registerNewMode = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

    moneySeparate(amount) {
        const parts = amount.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

    buyMoreTableButton(tableData){
        this.registerNewMode = false;
        this.buyMoreMode = true;
        this.editMode = false;
        this.editingId = tableData._id;
        this.profileLink = 'https://www.facebook.com/'+tableData.profileId;
        this.duration = 'zero';
        this.likeNum = tableData.likeNum;
        this.expiredTime = tableData.expiredTime;
        this.maxMaxPerDay = tableData.maxMaxPerDay;
    }


    editButton(tableData) {
        this.registerNewMode = false;
        this.buyMoreMode = false;
        this.editMode = true;
        this.editLikeNum = tableData.likeNum;
        this.maxLikeNum = tableData.maxLikeNum;
        this.editingId = tableData._id;
        this.profileLink = 'https://www.facebook.com/'+tableData.profileId;
        this.maxPerDay = tableData.maxPerDay;
        const rhSegment = tableData.rushHour.split(';');
        this.rushHour = [];
        for(let rhh of rhSegment){
            this.rushHour.push(rhh);
        }
        const segments = tableData.reactionRate.split("-");

        this.skipRate = segments[0];
        this.likeRate = segments[1];
        this.loveRate = segments[2];
        this.hahaRate = segments[3];
        this.wowRate = segments[4];
        this.sadRate = segments[5];
        this.angryRate = segments[6];
    }
    pauseButton(tableData) {
        const reactionServBundle = {
            id: tableData._id
        };
        swal({
            title: 'Tạm Dừng Buff Like?',
            text: 'Thời Hạn Sử Dụng Vẫn Được Bảo Toàn',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Tạm Dừng',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.pauseBuffLikeServBundle(reactionServBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        tableData.runningState = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }
    playButton(tableData) {
        const reactionServBundle = {
            id: tableData._id
        };
        swal({
            title: 'Tiếp Tục Buff Like?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Tiếp Tục',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.unPauseBuffLikeServBundle(reactionServBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        tableData.runningState = true;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }
    doneButton() {
        if (this.profileLink === undefined || this.profileLink === null || this.profileLink === '') {
            swal({
                title: "Fill In ProfileLink Field!",
                text: "Hãy Nhập ProfileLink!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        if (this.skipRate === undefined || this.skipRate === null) {
            this.skipRate = 0;
        }
        if (this.likeRate === undefined || this.likeRate === null) {
            this.likeRate = 0;
        }
        if (this.loveRate === undefined || this.loveRate === null) {
            this.loveRate = 0;
        }
        if (this.hahaRate === undefined || this.hahaRate === null) {
            this.hahaRate = 0;
        }
        if (this.wowRate === undefined || this.wowRate === null) {
            this.wowRate = 0;
        }
        if (this.sadRate === undefined || this.sadRate === null) {
            this.sadRate = 0;
        }
        if (this.angryRate === undefined || this.angryRate === null) {
            this.angryRate = 0;
        }

        const reactionRateString = '0=' + this.skipRate + ';1=' + this.likeRate + ';2=' + this.loveRate + ';3=' + this.hahaRate + ';4=' + this.wowRate + ';5=' + this.sadRate + ';6=' + this.angryRate;

        let tempRh = '';

        if(this.rushHour != undefined || this.rushHour != null || this.rushHour != ''){
            for(let rhh of this.rushHour){
                if(tempRh.length == 0){
                    tempRh = rhh;
                } else {
                    tempRh = tempRh + ';' + rhh;
                }
            }
        }

        const newDataBundle = {
            id: this.editingId,
            profileLink: this.profileLink,
            reactionRate: reactionRateString,
            rushHour: tempRh,
            maxPerDay: this.maxPerDay,
            likeNum: this.editLikeNum,
            delay: this.buffDuration
        };
        swal({
            title: 'Bạn Muốn Cập Nhật ProfileLink Và Tỷ Lệ Tương Tác?',
            text: 'Tỷ Lệ Tương Tác: ' + this.pretyReactionRate(reactionRateString),
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.updateBuffLikeBundle(newDataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        ).then((_result)=>{
                        });
                        this.loadBuffLikeRunningList();
                        this.profileLink = undefined;
                        this.editMode = false;
                        this.buyMoreMode = false;
                        this.registerNewMode = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }
    cancelButton(tableData) {
        this.likeNum = 0;
        this.skipRate = 0 ;
        this.likeRate = 0;
        this.loveRate = 100;
        this.hahaRate = 0;
        this.wowRate = 0;
        this.sadRate = 0;
        this.angryRate = 0;
        this.profileLink = undefined;
        this.editMode = false;
        this.registerNewMode = false;
        this.maxPerDay = 10;
        this.rushHour = [];
        this.buyMoreMode = false;
        this.editingId = undefined;
        this.duration = 'oneMonth';
        this.totalDiscountNum = 0;
        this.totalPrice = '0';
        this.finalPrice = '0';
    }
    reversePretyReactionRate(reactionRate) {
        const segment = reactionRate.split('-');
        let newReactionRate = '';
        if(segment.length < 7) {
            newReactionRate = '0=0;1=0;2=100;3=0;4=0;5=0;6=0';
        } else {
            newReactionRate = '0='+segment[0]+';1='+segment[1]+';2='+segment[2]+';3='+segment[3]+';4='+segment[4]+';5='+segment[5]+';6='+segment[6];
        }
        return newReactionRate;
    }
    editModeState(editMode) {
        if (editMode === undefined || editMode === false) {
            return false;
        }
        return true;
    }
    updateTableData(user, index) {
    }

    onEditClickTrue(tableIndex) {
    }
    editClickedCheck(editValue) {
        if (editValue === 'editClickedFalse') {
            return false;
        }
        return true;
    }

    loadBuffLikeRunningList(){
        this.authService.getBuffLikeRunningList().subscribe(data => {
            if(data.success){
                this.dataTable.dataRows = data.bundle;
                for ( let x of this.dataTable.dataRows){
                    x.reactionRate = this.pretyReactionRate(x.reactionRate);
                    x.oldProfileLink = x.profileLink + '';
                    x.oldReactionRate = x.reactionRate + '';
                }
            }
            if(this.dataTable.dataRows.length == 0){
                this.registerNewMode = true;
            }
        });
    };

    ngAfterViewInit() {
        this.loadBuffLikeRunningList();
        this.authService.getBuffLikePrice().subscribe(data => {
            if(data.success) {
                this.basePrice = data.price.basePrice;
                this.fiftyLikeDiscount = data.price.fiftyLikeDiscount;
                this.oneHundredLikeDiscount = data.price.oneHundredLikeDiscount;
                this.oneMonthDiscount = data.price.oneMonthDiscount;
                this.oneThousandTwoHundredLikeDiscount = data.price.oneThousandTwoHundredLikeDiscount;
                this.sixHundredLikeDiscount = data.price.sixHundredLikeDiscount;
                this.sixMonthDiscount = data.price.sixMonthDiscount;
                this.threeHundredLikeDiscount = data.price.threeHundredLikeDiscount;
                this.threeMonthDiscount = data.price.threeMonthDiscount;
                this.twoHundredLikeDiscount = data.price.twoHundredLikeDiscount;
                this.onSelectChange();
            }
        });
    }
}
