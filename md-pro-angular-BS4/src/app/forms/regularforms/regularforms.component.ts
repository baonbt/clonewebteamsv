import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import swal from "sweetalert2";
import {Router} from "@angular/router";

@Component({
    selector: 'app-regularforms-cmp',
    templateUrl: 'regularforms.component.html'
})

export class RegularFormsComponent implements OnInit, AfterViewInit {
    public dataTable: any;
    authToken: any;
    newDataRow: any;
    skipRate: Number;
    likeRate: Number;
    loveRate: Number;
    hahaRate: Number;
    wowRate: Number;
    sadRate: Number;
    angryRate: Number;
    duration: String;
    selectedPageSize: Number;
    currentPage: Number;
    basePrice: Number;
    oneMonthPrice: Number;
    threeMonthPrice: Number;
    sixMonthPrice: Number;
    autoTymPriceOneMonthDiscount: Number;
    autoTymPriceThreeMonthDiscount: Number;
    autoTymPriceSixMonthDiscount: Number;
    cookie: String;
    reactionRate: String;
    oldCookie: String;
    oldReactionRate: String;
    likePagePost: String;
    editMode: boolean;
    tableDatas: any;
    editingId: String;
    addNewMode: boolean = false;
    renewMode: boolean = false;
    facebookUid: String;

    constructor(
        private authService: AuthService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.editMode = false;
        this.likePagePost = "1";
        this.loadToken();
        this.duration = 'oneMonth';
        this.skipRate = 0;
        this.likeRate = 0;
        this.loveRate = 100;
        this.hahaRate = 0;
        this.wowRate = 0;
        this.sadRate = 0;
        this.angryRate = 0;
        this.selectedPageSize = 10;
        this.currentPage = 1;
        this.authService.getAutoTymPrice().subscribe(data => {
            if (data.success) {
                this.basePrice = data.price.basePrice;
                this.oneMonthPrice = 1 * data.price.basePrice - 1 * data.price.basePrice * data.price.oneMonthDiscount;
                this.threeMonthPrice = 3 * data.price.basePrice - 3 * data.price.basePrice * data.price.threeMonthDiscount;
                this.sixMonthPrice = 6 * data.price.basePrice - 6 * data.price.basePrice * data.price.sixMonthDiscount;
                this.autoTymPriceOneMonthDiscount = data.price.oneMonthDiscount;
                this.autoTymPriceSixMonthDiscount = data.price.sixMonthDiscount;
                this.autoTymPriceThreeMonthDiscount = data.price.threeMonthDiscount;
            }
        });

    }

    renewTableButton(data){
        this.renewMode = true;
        this.editMode = false;
        this.addNewMode = false;
        this.cookie = data.cookie;
        this.editingId = data._id;
        this.duration = 'oneMonth';
        this.facebookUid = data.fbUid;
    }

    renewButton(){
        const newDataBundle = {
            id: this.editingId,
            period: this.duration
        };
        let payPrice;
        if (this.duration === 'oneMonth') {
            payPrice = this.oneMonthPrice
        } else if (this.duration === 'threeMonth') {
            payPrice = this.threeMonthPrice
        } else if (this.duration === 'sixMonth') {
            payPrice = this.sixMonthPrice
        }
        swal({
            title: 'Xác Nhận',
            text: 'Thanh Toán ' + payPrice,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.giaHanReactionService(newDataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadReactionRunningList();
                        this.cancelButton();
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });

    }

    loadReactionRunningList(){
        this.authService.getReactionRunningList().subscribe(data => {
            if (data.success) {
                this.tableDatas = data.bundle;
                for (let x of this.tableDatas) {
                    x.reactionRate = this.pretyReactionRate(x.reactionRate);
                }
            }
            if(this.tableDatas.length == 0){
                this.addNewMode = true;
            }
        });
    }
    pretyReactionRate(reactionRate) {
        const segment = reactionRate.split(';');
        let newReactionRate = '';
        segment.forEach(x => {
            const y = x.replace('0=', '').replace('1=', '')
                .replace('2=', '').replace('3=', '')
                .replace('4=', '').replace('5=', '')
                .replace('6=', '');
            if (newReactionRate.length > 0) {
                newReactionRate = newReactionRate + '-' + y;
            } else {
                newReactionRate = y;
            }
        });
        return newReactionRate;
    }

    // onDataReloadClicked() {
    //     console.log(11111);
    //     $('#datatables').DataTable().ajax.reload();
    // }
    reversePretyReactionRate(reactionRate) {
        const segment = reactionRate.split('-');
        let newReactionRate = '';
        if (segment.length < 7) {
            newReactionRate = '0=0;1=0;2=100;3=0;4=0;5=0;6=0';
        } else {
            newReactionRate = '0=' + segment[0] + ';1=' + segment[1] + ';2=' + segment[2] + ';3=' + segment[3] + ';4=' + segment[4] + ';5=' + segment[5] + ';6=' + segment[6];
        }
        return newReactionRate;
    }

    openAddnewCard(){
        this.renewMode = false;
        this.addNewMode = true;
        this.editMode = false;
        this.skipRate = 0;
        this.likeRate = 0;
        this.loveRate = 100;
        this.hahaRate = 0;
        this.wowRate = 0;
        this.sadRate = 0;
        this.angryRate = 0;
        this.cookie = undefined;
    }

    editButton(tableData) {
        this.renewMode = false;
        this.addNewMode = false;
        this.editingId = tableData._id;
        this.cookie = tableData.cookie;
        this.likePagePost = tableData.likePagePost;
        const segments = tableData.reactionRate.split("-");

        this.skipRate = segments[0];
        this.likeRate = segments[1];
        this.loveRate = segments[2];
        this.hahaRate = segments[3];
        this.wowRate = segments[4];
        this.sadRate = segments[5];
        this.angryRate = segments[6];

        this.editMode = true;
    }

    pauseButton(tableData) {
        const reactionServBundle = {
            id: tableData._id
        };
        swal({
            title: 'Tạm Dừng Thả Tym?',
            text: 'Thời Hạn Sử Dụng Vẫn Được Bảo Toàn',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Tạm Dừng',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.pauseReactionServBundle(reactionServBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        tableData.runningState = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

    playButton(tableData) {
        const reactionServBundle = {
            id: tableData._id
        };
        swal({
            title: 'Tiếp Tục Thả Tym?',
            text: 'Nếu Tạm Dừng Quá 3 Ngày Bạn Nên Cập Nhật Lại Cookie',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Tiếp Tục',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.unPauseReactionServBundle(reactionServBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        tableData.runningState = true;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

    doneButton() {
        if (this.cookie === undefined || this.cookie === null || this.cookie === '') {
            swal({
                title: "Fill In Cookie Field!",
                text: "Hãy Nhập Cookie!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop);
            return false;
        }
        if (this.skipRate === undefined || this.skipRate === null) {
            this.skipRate = 0;
        }
        if (this.likeRate === undefined || this.likeRate === null) {
            this.likeRate = 0;
        }
        if (this.loveRate === undefined || this.loveRate === null) {
            this.loveRate = 0;
        }
        if (this.hahaRate === undefined || this.hahaRate === null) {
            this.hahaRate = 0;
        }
        if (this.wowRate === undefined || this.wowRate === null) {
            this.wowRate = 0;
        }
        if (this.sadRate === undefined || this.sadRate === null) {
            this.sadRate = 0;
        }
        if (this.angryRate === undefined || this.angryRate === null) {
            this.angryRate = 0;
        }
        if (this.likePagePost === null || this.likePagePost === undefined) {
            this.likePagePost = "0";
        }

        const fbUid = this.getFbUidFromCookie(this.cookie);
        const reactionRateString = '0=' + this.skipRate + ';1=' + this.likeRate + ';2=' + this.loveRate + ';3=' + this.hahaRate + ';4=' + this.wowRate + ';5=' + this.sadRate + ';6=' + this.angryRate;
        const reactionBundle = {
            id: this.editingId,
            fbUid: fbUid,
            cookie: this.cookie,
            reactionRate: reactionRateString,
            likePagePost: this.likePagePost
        };
        swal({
            title: 'Bạn Muốn Cập Nhật Cookie Và Tỷ Lệ Tương Tác?',
            text: 'Tỷ Lệ Tương Tác: ' + this.pretyReactionRate(reactionRateString),
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.updateReactionServBundle(reactionBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadReactionRunningList();
                        this.cookie = undefined;
                        this.editMode = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

    cancelButton() {
        this.skipRate = 0;
        this.likeRate = 0;
        this.loveRate = 100;
        this.hahaRate = 0;
        this.wowRate = 0;
        this.sadRate = 0;
        this.angryRate = 0;
        this.cookie = undefined;
        this.editMode = false;
        this.addNewMode = false;
        this.renewMode = false;
    }


    onAddNewClick() {
        if (this.cookie === undefined || this.cookie === null || this.cookie === '') {
            swal({
                title: "Fill In Cookie Field!",
                text: "Hãy Nhập Cookie!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        if (this.skipRate === undefined || this.skipRate === null) {
            this.skipRate = 0;
        }
        if (this.likeRate === undefined || this.likeRate === null) {
            this.likeRate = 0;
        }
        if (this.loveRate === undefined || this.loveRate === null) {
            this.loveRate = 0;
        }
        if (this.hahaRate === undefined || this.hahaRate === null) {
            this.hahaRate = 0;
        }
        if (this.wowRate === undefined || this.wowRate === null) {
            this.wowRate = 0;
        }
        if (this.sadRate === undefined || this.sadRate === null) {
            this.sadRate = 0;
        }
        if (this.angryRate === undefined || this.angryRate === null) {
            this.angryRate = 0;
        }

        const fbUid = this.getFbUidFromCookie(this.cookie);
        const reactionRateString = '0=' + this.skipRate + ';1=' + this.likeRate + ';2=' + this.loveRate + ';3=' + this.hahaRate + ';4=' + this.wowRate + ';5=' + this.sadRate + ';6=' + this.angryRate;
        const newDataBundle = {
            fbUid: fbUid,
            cookie: this.cookie,
            reactionRate: reactionRateString,
            period: this.duration,
            likePagePost: this.likePagePost
        };
        let payPrice;
        if (this.duration === 'oneMonth') {
            payPrice = this.oneMonthPrice
        } else if (this.duration === 'threeMonth') {
            payPrice = this.threeMonthPrice
        } else if (this.duration === 'sixMonth') {
            payPrice = this.sixMonthPrice
        }
        swal({
            title: 'Xác Nhận',
            text: 'Thanh Toán ' + payPrice,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.addReactionService(newDataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadReactionRunningList();
                        this.cookie = undefined;
                        this.addNewMode = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

    getFbUidFromCookie(cookie) {
        if (cookie !== undefined && cookie !== null && cookie !== '') {
            let keyValues = cookie.split(";");

            if (keyValues.length < 2){
                keyValues = cookie.split('\n');
                if (keyValues.length < 2){
                    keyValues = cookie.split(' ');
                }
            }
            if (keyValues != null && keyValues.length > 0) {
                for (let keyValue of keyValues){
                    if (keyValue.includes("c_user=")) {
                        return keyValue.split("=")[1];
                    }
                    if (keyValue.includes("c_user:")) {
                        return keyValue.split(":")[1];
                    }
                }
            }
        }
        return null;
    }

    onEditClickTrue(tableIndex) {
    }

    editClickedCheck(editValue) {
        if (editValue === 'editClickedFalse') {
            return false;
        }
        return true;
    }

    loadToken() {
        const token = localStorage.getItem('id_token');
        this.authToken = token;
    }

    ngAfterViewInit() {
        this.loadReactionRunningList();
    }
}
