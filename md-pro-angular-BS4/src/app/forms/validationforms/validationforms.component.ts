import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import swal from "sweetalert2";


declare var $:any;

@Component({
    selector: 'app-validationforms-cmp',
    templateUrl: 'validationforms.component.html'
})

export class ValidationFormsComponent implements OnInit{

  textAreaContent: any;
  registered: boolean;
  editCommentBundle: boolean;
  addNewIntableClicked: boolean;
  registeredTable: any;
  commentBundleTable: any;
    oneMonthDiscount: number;
    threeMonthDiscount: number;
    sixMonthDiscount: number;
    tenDiscount: number;
    twentyDiscount: number;
    thirtyDiscount: number;
    basePrice: number;
    duration: String;
    period: number;
    commentDiscount: number;
    durationDiscountNum: number;
    commentNum: number;
    totalPriceNum: number;
    totalDiscountNum: number;
    finalPriceNum: number;
    totalPrice: String;
    finalPrice: String;
    profileLink: String;
    profileTableEdit: boolean = false;
    profileEditingId: String;

    bundleName: String;
    hashTag: String;
    commentInDays: Number;
    commentInTime: String;
    stickerCommentRate: any;
    commentOrder: String = "random";
    commentContent: String;
    commentBundleEditMode: boolean;
    commentBundleEditingId: String;

    rushHourList: any = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'];
    delay: any = 1;
    bundleIdList: any = [];
    rushHour: any = [];
    maxPerDay: any = 10;
    maxMaxPerDay: any;
    maxCommentNum: any;

    isCommentBundleEmpty: boolean = true;
    addNewProfileLinkMode: boolean = false;

    buyMoreDurationMode: boolean = false;
    buyMorePostMode: boolean = false;
    buyMoreCommentMode: boolean = false;

  constructor(private authService: AuthService) {}
  ngOnInit() {
      this.commentBundleEditMode = false;
      this.stickerCommentRate = 10;
      this.totalDiscountNum = 0;
      this.commentDiscount = 0;
      this.durationDiscountNum = 0;
      this.period = 1;
      this.duration = 'oneMonth';
      this.basePrice = 0;
      this.oneMonthDiscount = 0;
      this.threeMonthDiscount = 0;
      this.sixMonthDiscount = 0;
      this.tenDiscount = 0;
      this.twentyDiscount = 0;
      this.thirtyDiscount = 0;
      this.authService.getCommentPrice().subscribe(data=>{
          if(data.success){
              this.oneMonthDiscount = data.price.oneMonthDiscount;
              this.threeMonthDiscount = data.price.threeMonthDiscount;
              this.sixMonthDiscount = data.price.sixMonthDiscount;
              this.tenDiscount = data.price.tenCmtDiscount;
              this.twentyDiscount = data.price.twentyCmtDiscount;
              this.thirtyDiscount = data.price.thirtyCmtDiscount;
              this.basePrice = data.price.basePrice;
              this.onSelectChange();
          }
      });
      this.registered = false;
      this.editCommentBundle = false;
      this.addNewIntableClicked = false;

      this.loadProfileData();
      this.loadCommentBundleData();
  }

    buyMoreCommentButton(){
        const newCommentBundle = {
            id: this.profileEditingId,
            commentNum: this.commentNum
        };
        console.log(this.commentContent);
        swal({
            title: 'Xác Nhận Mua Thêm '+ this.commentNum + ' Bình Luận',
            text: 'Thanh Toán ' + this.finalPrice ,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.muaThemCommentCommentBundleServBundle(newCommentBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadProfileData();
                        this.addNewIntableClicked = false;
                        this.profileTableEdit = false;
                        this.commentBundleEditMode = false;
                        this.isCommentBundleEmpty = false;
                        this.addNewProfileLinkMode = false;

                        this.buyMoreDurationMode = false;
                        this.buyMorePostMode = false;
                        this.buyMoreCommentMode = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });

    };

  buyMoreDurationButton(){
      const newCommentBundle = {
      id: this.profileEditingId,
      duration: this.duration
  };
      console.log(this.commentContent);
      swal({
          title: 'Xác Nhận Gia Hạn '+ this.period + ' Tháng',
          text: 'Thanh Toán ' + this.finalPrice ,
          type: 'warning',
          showCancelButton: true,
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          confirmButtonText: 'Chắc Chắn',
          buttonsStyling: false
      }).then((result) => {
          if (result.value) {
              this.authService.giaHanCommentBundleServBundle(newCommentBundle).subscribe(data => {
                  if (data.success) {
                      swal(
                          {
                              title: data.msg,
                              type: 'success',
                              confirmButtonClass: "btn btn-success",
                              buttonsStyling: false
                          }
                      );
                      this.loadProfileData();
                      this.addNewIntableClicked = false;
                      this.profileTableEdit = false;
                      this.commentBundleEditMode = false;
                      this.isCommentBundleEmpty = false;
                      this.addNewProfileLinkMode = false;

                      this.buyMoreDurationMode = false;
                      this.buyMorePostMode = false;
                      this.buyMoreCommentMode = false;
                  } else {
                      swal(
                          {
                              title: data.msg,
                              type: 'error',
                              confirmButtonClass: "btn btn-success",
                              buttonsStyling: false
                          }
                      )
                  }
              });
          }
      });

  };

    buyMorePostButton(){
        const newCommentBundle = {
            id: this.profileEditingId,
            maxPerDay: this.maxPerDay
        };
        console.log(this.commentContent);
        swal({
            title: 'Xác Nhận Mua Thêm '+ this.maxPerDay + ' Bài Viết',
            text: 'Thanh Toán ' + this.finalPrice ,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.muaThemPostCommentBundleServBundle(newCommentBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadProfileData();
                        this.addNewIntableClicked = false;
                        this.profileTableEdit = false;
                        this.commentBundleEditMode = false;
                        this.isCommentBundleEmpty = false;
                        this.addNewProfileLinkMode = false;

                        this.buyMoreDurationMode = false;
                        this.buyMorePostMode = false;
                        this.buyMoreCommentMode = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });

    };

    buyMorePostChange(){
        if(this.maxPerDay === undefined || this.maxPerDay === null){
            this.maxPerDay =1;
        }
        if (this.period >= 6) {
            this.durationDiscountNum = this.sixMonthDiscount;
        } else if (this.period >= 3) {
            this.durationDiscountNum = this.threeMonthDiscount;
        } else if (this.period >= 1) {
            this.durationDiscountNum = this.oneMonthDiscount;
        }
        if(this.maxCommentNum >= 20){
            this.commentDiscount = this.thirtyDiscount;
        } else if(this.maxCommentNum >= 10){
            this.commentDiscount = this.twentyDiscount;
        } else if(this.maxCommentNum >= 1){
            this.commentDiscount = this.tenDiscount;
        }
        this.totalPriceNum = this.period * this.maxCommentNum * this.basePrice*(this.maxPerDay/10);
        this.totalDiscountNum = this.commentDiscount + this.durationDiscountNum;
        this.finalPriceNum = this.totalPriceNum - this.totalDiscountNum * this.totalPriceNum;
        this.totalPrice = this.moneySeparate(this.totalPriceNum.toFixed(2));
        this.finalPrice = this.moneySeparate(this.finalPriceNum.toFixed(2));
    }

    buyMoreCommentChange(){
        if(this.commentNum === undefined || this.commentNum === null){
            this.commentNum =1;
        }
        if (this.period >= 6) {
            this.durationDiscountNum = this.sixMonthDiscount;
        } else if (this.period >= 3) {
            this.durationDiscountNum = this.threeMonthDiscount;
        } else if (this.period >= 1) {
            this.durationDiscountNum = this.oneMonthDiscount;
        }
        if(this.commentNum >= 20){
            this.commentDiscount = this.thirtyDiscount;
        } else if(this.commentNum >= 10){
            this.commentDiscount = this.twentyDiscount;
        } else if(this.commentNum >= 1){
            this.commentDiscount = this.tenDiscount;
        }
        this.totalPriceNum = this.period * this.commentNum * this.basePrice*(this.maxMaxPerDay/10);
        this.totalDiscountNum = this.commentDiscount + this.durationDiscountNum;
        this.finalPriceNum = this.totalPriceNum - this.totalDiscountNum * this.totalPriceNum;
        this.totalPrice = this.moneySeparate(this.totalPriceNum.toFixed(2));
        this.finalPrice = this.moneySeparate(this.finalPriceNum.toFixed(2));
    }

  buyMoreDurationChange(){
      if (this.duration === 'oneMonth') {
          this.period = 1;
          this.durationDiscountNum = this.oneMonthDiscount;
      } else if (this.duration === 'threeMonth') {
          this.period = 3;
          this.durationDiscountNum = this.threeMonthDiscount;
      } else if (this.duration === 'sixMonth') {
          this.period = 6;
          this.durationDiscountNum = this.sixMonthDiscount;
      }
      if(this.maxCommentNum >= 20){
          this.commentDiscount = this.thirtyDiscount;
      } else if(this.maxCommentNum >= 10){
          this.commentDiscount = this.twentyDiscount;
      } else if(this.maxCommentNum >= 1){
          this.commentDiscount = this.tenDiscount;
      }
      this.totalPriceNum = this.period * this.maxCommentNum * this.basePrice*(this.maxMaxPerDay/10);
      this.totalDiscountNum = this.commentDiscount + this.durationDiscountNum;
      this.finalPriceNum = this.totalPriceNum - this.totalDiscountNum * this.totalPriceNum;
      this.totalPrice = this.moneySeparate(this.totalPriceNum.toFixed(2));
      this.finalPrice = this.moneySeparate(this.finalPriceNum.toFixed(2));
  }

    buyMoreCommentTable(data){
        this.profileEditingId = data._id;
        this.addNewIntableClicked = false;
        this.profileTableEdit = false;
        this.commentBundleEditMode = false;
        this.isCommentBundleEmpty = false;
        this.addNewProfileLinkMode = false;

        this.buyMoreDurationMode = false;
        this.buyMorePostMode = false;
        this.buyMoreCommentMode = true;

        this.profileLink = 'https://www.facebook.com/'+data.profileId;
        this.maxMaxPerDay = data.maxMaxPerDay;
        this.maxCommentNum = data.maxCommentNum;
        this.period = data.expiredTime/30;
        this.commentNum = 1;
    }

    buyMorePostTable(data){
        this.maxPerDay = 1;
        this.profileEditingId = data._id;
        this.addNewIntableClicked = false;
        this.profileTableEdit = false;
        this.commentBundleEditMode = false;
        this.isCommentBundleEmpty = false;
        this.addNewProfileLinkMode = false;

        this.buyMoreDurationMode = false;
        this.buyMorePostMode = true;
        this.buyMoreCommentMode = false;

        this.profileLink = 'https://www.facebook.com/'+data.profileId;
        this.maxMaxPerDay = data.maxMaxPerDay;
        this.maxCommentNum = data.maxCommentNum;
        this.period = data.expiredTime/30;
        this.commentNum = 1;
    }

  buyMoreDurationTable(data){
      this.profileEditingId = data._id;
      this.addNewIntableClicked = false;
      this.profileTableEdit = false;
      this.commentBundleEditMode = false;
      this.isCommentBundleEmpty = false;
      this.addNewProfileLinkMode = false;

      this.buyMoreDurationMode = true;
      this.buyMorePostMode = false;
      this.buyMoreCommentMode = false;

      this.profileLink = 'https://www.facebook.com/'+data.profileId;
      this.maxMaxPerDay = data.maxMaxPerDay;
      this.maxCommentNum = data.maxCommentNum;
  }

  onEditCommentNumChange(){
        if(this.commentNum === undefined || this.commentNum === null){
            this.commentNum = 0;
        }
        if(this.commentNum > this.maxCommentNum){
            this.commentNum = this.maxCommentNum;
        }
  }

  loadProfileData(){
      this.authService.getCommentProfileRunningList().subscribe(data=>{
          if(data.success){
              this.registeredTable = data.bundle;
              console.log(data.bundle);
          }
          if(this.registeredTable.length == 0){
              this.addNewProfileLinkMode = true;
          }
      });
  }

  loadCommentBundleData(){
      this.authService.getCommentBundleList().subscribe(data=>{
      if(data.success){
          this.commentBundleTable = data.bundle;
          for(let x of this.commentBundleTable){
              const segment = x.replyRate.split(';');
              x.stickerCommentRate = segment[2].replace('2=','');
          }
      }
      if(this.commentBundleTable.length > 0){
          this.isCommentBundleEmpty = false;
      }
  });
  };

  addNewProfileInTableButton(){
      this.addNewProfileLinkMode = true;
      this.commentBundleEditMode = false;
      this.addNewIntableClicked = false;
      this.profileTableEdit = false;
      this.commentNum = 0;
      this.bundleIdList = [];
      this.maxPerDay = 10;
      this.rushHour = [];
  }

  addNewInTableClick(){
      this.addNewProfileLinkMode = false;
      this.commentBundleEditingId = undefined;
      this.bundleName = undefined;
      this.hashTag = undefined;
      this.stickerCommentRate = 10;
      this.commentInDays = undefined;
      this.commentInTime = undefined;
      this.commentContent = undefined;
      this.commentBundleEditMode = false;
      this.addNewIntableClicked = true;
      this.profileTableEdit = false;
      this.bundleIdList = [];
      this.rushHour = [];
  }

  addNewCommentBundle(){
          if(
              this.bundleName === undefined || this.bundleName === ''
          ) {
              swal({
                  title: "Field Alert!",
                  text: "Hãy Điền Tên Tệp Để Phân Biệt Bạn Nhé!",
                  buttonsStyling: false,
                  confirmButtonClass: "btn btn-info"
              }).catch(swal.noop);
              return false;
          }


          let rhh = '';

          for(let rh of this.rushHour){
              if(rhh.length === 0){
                  rhh = rh;
              } else {
                  rhh = rhh + ';' + rh;
              }
          }

          const newReplyRate = '0='+(100-this.stickerCommentRate)+';1=0;2=' + this.stickerCommentRate + ';3=0';

          const newCommentBundle = {
              bundleName: this.bundleName,
              hashTag: this.hashTag,
              replyRate: newReplyRate,
              commentOrder: this.commentOrder,
              delay: this.delay,
              rushHour: rhh,
              commentContents: this.commentContent
          };
          console.log(this.commentContent);
          swal({
              title: 'Xác Nhận',
              text: 'Thêm Tệp Bình Luận ' + this.bundleName ,
              type: 'warning',
              showCancelButton: true,
              confirmButtonClass: 'btn btn-success',
              cancelButtonClass: 'btn btn-danger',
              confirmButtonText: 'Chắc Chắn',
              buttonsStyling: false
          }).then((result) => {
              if (result.value) {
                  this.authService.addCommentBundle(newCommentBundle).subscribe(data => {
                      if (data.success) {
                          swal(
                              {
                                  title: data.msg,
                                  type: 'success',
                                  confirmButtonClass: "btn btn-success",
                                  buttonsStyling: false
                              }
                          );
                          this.loadCommentBundleData();
                          this.addNewIntableClicked = false;
                      } else {
                          swal(
                              {
                                  title: data.msg,
                                  type: 'error',
                                  confirmButtonClass: "btn btn-success",
                                  buttonsStyling: false
                              }
                          )
                      }
                  });
              }
          });
  }

    updateCommentBundle(){
        if(
            this.bundleName === undefined || this.bundleName === ''
        ) {
            swal({
                title: "Field Alert!",
                text: "Hãy Điền Tên Cho Tệp Để Phân Biệt Bạn Nhé!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }


        console.log(this.rushHour);
        let rhh = '';
        for(let rh of this.rushHour){
            if(rhh.length === 0){
                rhh = rh;
            } else {
                rhh = rhh + ';' + rh;
            }
        }

        const newReplyRate = '0='+(100-this.stickerCommentRate)+';1=0;2=' + this.stickerCommentRate + ';3=0';

        const newCommentBundle = {
            id: this.commentBundleEditingId,
            bundleName: this.bundleName,
            hashTag: this.hashTag,
            replyRate: newReplyRate,
            commentOrder: this.commentOrder,
            delay: this.delay,
            rushHour: rhh,
            commentContents: this.commentContent
        };

        swal({
            title: 'Xác Nhận',
            text: 'Cập Nhật Tệp Bình Luận ' + this.bundleName ,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.updateCommentBundle(newCommentBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadCommentBundleData();
                        this.addNewIntableClicked = false;
                        this.commentBundleEditMode = false;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

  profileLinkTableEditButton(data){
      this.addNewProfileLinkMode = false;
      this.commentBundleEditMode = false;
      this.addNewIntableClicked = false;
      this.profileTableEdit = true;
      this.profileLink = data.profileLink;
      this.profileEditingId = data._id;
      this.bundleIdList = data.commentBundles;
      this.maxPerDay = data.maxPerDay;
      this.maxCommentNum = data.maxCommentNum;
      this.commentNum = data.commentNum;
      this.profileLink = 'https://www.facebook.com/'+data.profileId;
  }

    commentBundleTableEditButton(data){
        this.profileTableEdit = false;
        this.addNewProfileLinkMode = false;
        this.addNewIntableClicked = false;
        this.commentBundleEditMode = true;
        this.commentBundleEditingId = data._id;
           this.bundleName = data.bundleName;
            this.hashTag = data.hashTag;
            this.stickerCommentRate = data.stickerCommentRate;
            this.commentOrder = data.commentOrder;
            this.delay = data.delay;
            const segments = data.rushHour.split(';');
            this.rushHour = [];
            for(let x of segments){
                this.rushHour.push(x);
            }
            this.commentContent = data.commentContents;
    }

  cancelButton(){
      this.profileTableEdit = false;
      this.profileLink = undefined;
      this.addNewIntableClicked = false;
      this.addNewProfileLinkMode = false;
      this.bundleIdList = [];
      this.rushHour = [];
      this.maxPerDay = 10;
      this.commentNum = 0;
      this.commentBundleEditMode = false;
      this.isCommentBundleEmpty = false;

      this.buyMoreDurationMode = false;
      this.buyMorePostMode = false;
      this.buyMoreCommentMode = false;
  }

    profileLinkdoneButton() {
        if (this.profileLink === undefined || this.profileLink === null || this.profileLink === '') {
            swal({
                title: "Fill In ProfileLink Field!",
                text: "Hãy Nhập ProfileLink!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        const newDataBundle = {
            id: this.profileEditingId,
            profileLink: this.profileLink,
            commentBundles: this.bundleIdList,
            commentNum: this.commentNum
        };
        swal({
            title: 'Bạn Muốn Cập Nhật ProfileLink?',
            text: ''+this.profileLink,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.updateCommentProfileLink(newDataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadProfileData();
                        this.profileTableEdit = false;
                        this.profileLink = undefined;
                        this.commentNum = 0;
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

  registerNewProfileLink(){
      if (this.isCommentBundleEmpty) {
          swal({
              title: "Chưa Có Tệp Bình Luận!",
              text: "ChatBot Đang Được Thử Nghiệm, Hãy Tạo Tệp Bình Luận!",
              buttonsStyling: false,
              confirmButtonClass: "btn btn-info"
          }).catch(swal.noop);
          this.addNewIntableClicked = true;
          this.addNewProfileLinkMode = false;
          return false;
      }
      if (this.bundleIdList.length == 0) {
          swal({
              title: "Bạn Chưa Chọn Tệp Bình Luận!",
              text: "ChatBot Đang Được Thử Nghiệm, Hãy Chọn Tệp Bình Luận!",
              buttonsStyling: false,
              confirmButtonClass: "btn btn-info"
          }).catch(swal.noop);
          return false;
      }
      if (this.commentNum === undefined || this.commentNum === null || this.commentNum === 0) {
          swal({
              title: "Fill In Comment Field!",
              text: "Hãy Nhập Số Lượng Bình Luận!",
              buttonsStyling: false,
              confirmButtonClass: "btn btn-info"
          }).catch(swal.noop);
          return false;
      }
      if (this.profileLink === undefined || this.profileLink === null || this.profileLink === '') {
          swal({
              title: "Fill In ProfileLink Field!",
              text: "Hãy Nhập ProfileLink!",
              buttonsStyling: false,
              confirmButtonClass: "btn btn-info"
          }).catch(swal.noop);
          return false;
      }
      const dataBundle = {
          period: this.duration,
          commentNum:this.commentNum,
          profileLink:this.profileLink,
          commentBundles: this.bundleIdList,
          maxPerDay: this.maxPerDay
      };
      swal({
          title: 'Xác Nhận',
          text: 'Thanh Toán ' + this.finalPrice ,
          type: 'warning',
          showCancelButton: true,
          confirmButtonClass: 'btn btn-success',
          cancelButtonClass: 'btn btn-danger',
          confirmButtonText: 'Chắc Chắn',
          buttonsStyling: false
      }).then((result) => {
          if (result.value) {
              this.authService.addCommentService(dataBundle).subscribe(data => {
                  if (data.success) {
                      swal(
                          {
                              title: data.msg,
                              type: 'success',
                              confirmButtonClass: "btn btn-success",
                              buttonsStyling: false
                          }
                      );
                      this.loadProfileData();
                      this.profileLink = undefined;
                  } else {
                      swal(
                          {
                              title: data.msg,
                              type: 'error',
                              confirmButtonClass: "btn btn-success",
                              buttonsStyling: false
                          }
                      )
                  }
              });
          }
      });
  }

    profileLinkplayButton(tableData) {
        const reactionServBundle = {
            id: tableData._id
        };
        swal({
            title: 'Tiếp Tục Buff Bình Luận?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Tiếp Tục',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.unPauseCommentProfileServBundle(reactionServBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadProfileData();
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }
    profileLinkpauseButton(tableData) {
        const reactionServBundle = {
            id: tableData._id
        };
        swal({
            title: 'Tạm Dừng Buff Bình Luận?',
            text: 'Thời Hạn Sử Dụng Vẫn Được Bảo Toàn',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Tạm Dừng',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.pauseCommentProfileServBundle(reactionServBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadProfileData();
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }


    deleteCommentBundleButton(tableData) {
        const reactionServBundle = {
            id: tableData._id
        };
        swal({
            title: 'XÓA TỆP?',
            text:'Bạn có chắc chắn muốn xóa tệp VĨNH VIỄN',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.deleteCommentBundleServBundle(reactionServBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.loadCommentBundleData();
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }


  onSelectChange(){
      if(this.commentNum === undefined || this.commentNum === null){
          this.commentNum = 1;
      }
      if (this.duration === 'oneMonth') {
          this.period = 1;
          this.durationDiscountNum = this.oneMonthDiscount;
      } else if (this.duration === 'threeMonth') {
          this.period = 3;
          this.durationDiscountNum = this.threeMonthDiscount;
      } else if (this.duration === 'sixMonth') {
          this.period = 6;
          this.durationDiscountNum = this.sixMonthDiscount;
      }
      if(this.commentNum >= 20){
          this.commentDiscount = this.thirtyDiscount;
      } else if(this.commentNum >= 10){
          this.commentDiscount = this.twentyDiscount;
      } else if(this.commentNum >= 1){
          this.commentDiscount = this.tenDiscount;
      }
      this.totalPriceNum = this.period * this.commentNum * this.basePrice;
      this.totalDiscountNum = this.commentDiscount + this.durationDiscountNum;
      if(this.maxPerDay > 10){
          this.totalPriceNum = this.totalPriceNum*(this.maxPerDay/10);
      }
      this.finalPriceNum = this.totalPriceNum - this.totalDiscountNum * this.totalPriceNum;
      this.totalPrice = this.moneySeparate(this.totalPriceNum.toFixed(2));
      this.finalPrice = this.moneySeparate(this.finalPriceNum.toFixed(2));
  }

    moneySeparate(amount) {
        const parts = amount.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }

}
