import { Routes } from '@angular/router';

import { ExtendedFormsComponent } from './extendedforms/extendedforms.component';
import { RegularFormsComponent } from './regularforms/regularforms.component';
import { ValidationFormsComponent } from './validationforms/validationforms.component';
import { WizardComponent } from './wizard/wizard.component';

export const FormsRoutes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
    },
    {
      path: '',
      children: [ {
        path: 'autotym',
        component: RegularFormsComponent
    }]}, {
    path: '',
    children: [ {
      path: 'bufflike',
      component: ExtendedFormsComponent
    }]
    }
    , {
    path: '',
    children: [ {
      path: 'comments',
      component: ValidationFormsComponent
    }]
    }
    // , {
    //     path: '',
    //     children: [ {
    //         path: 'wizard',
    //         component: WizardComponent
    //     }]
    // }
];
