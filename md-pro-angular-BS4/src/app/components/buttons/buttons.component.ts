import {Component, OnInit, AfterViewInit} from '@angular/core';
import swal from "sweetalert2";
import {TableData} from '../../md/md-table/md-table.component';
import {Headers} from '@angular/http';
import { AuthService } from '../../services/auth.service';

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: string[][];
}

declare var $: any;

@Component({
    selector: 'app-buttons',
    templateUrl: './buttons.component.html'
})

export class ButtonsComponent implements OnInit, AfterViewInit {
    phone: String;
    username: String;
    password: Number;
    allFieldFilled: boolean;
    existUsername: boolean;
    tableData: TableData;
    logs: any;
    logsLength: number;
    newDataRow: any;
    dataTable: DataTable;
    constructor(private authService: AuthService) {}
  ngOnInit() {
    this.allFieldFilled = false;
    this.existUsername = true;
      this.dataTable = {
          headerRow: [ 'Time', 'Created Account', 'Responsible' ],
          footerRow: [ 'Time', 'Created Account', 'Responsible' ],
          dataRows: [
          ]
      };
  }
  onRegisterSubmit() {
    const user = {
        username: this.username,
        password: this.password
    };
    if (!this.validateRegister(user)) {
        this.allFieldFilled = false;
        this.showSwal();
        return false;
    } else {
        this.allFieldFilled = true;
    }
    this.authService.registerUser(user).subscribe(data => {
          if (data.success) {
              this.existUsername = false;
              this.showSwal();
          } else {
              this.existUsername = true;
              this.showSwal();
          }
      });
  }
    // emailValidationRegister(e) {
    //     const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //     if (re.test(String(e).toLowerCase())) {
    //         this.validEmailRegister = true;
    //     } else {
    //         this.validEmailRegister = false;
    //     }
    // }
    showSwal() {
        if (!this.allFieldFilled) {
            swal({
                title: "Fill In All Field!",
                text: "Điền Đầy Đủ Cả Tài Khoản Và Mật Khẩu!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
        } else if (this.existUsername) {
            swal({
                title: "Username Existed",
                text: "Tài Khoản Đã Tồn Tại!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
        } else if (this.allFieldFilled && !this.existUsername) {
            swal({
                title: "Created Account",
                text: "Tạo Tài Khoản Thành Công!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
        }
    }
    validateRegister(user) {
        if (user.username === undefined || user.password === undefined) {
            return false;
        } else {
            return true;
        }
    }

    ngAfterViewInit() {
        const table = $('#datatables').DataTable({
            order: [[ 0, "desc" ]]
        } );
        this.authService.getAccountLogs().subscribe( logs => {
            this.logs = logs;
            this.logsLength = this.logs.length;
            for ( const log of logs) {
                const ISOTime = new Date(log.time).toISOString();
                this.newDataRow = [];
                this.newDataRow.push(ISOTime.replace(/T/, ' ').replace(/\..+/, ''));
                this.newDataRow.push(log.createdAccount);
                this.newDataRow.push(log.responsibler);
                table.row.add(this.newDataRow).draw( false );
            }
        });
    }
}
