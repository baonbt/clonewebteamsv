import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TableData} from '../../md/md-table/md-table.component';
import swal from "sweetalert2";
import { AuthService } from '../../services/auth.service';

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: string[][];
}

declare var $: any;

@Component({
    selector: 'app-grid-cmp',
    templateUrl: 'grid.component.html'
})

export class GridSystemComponent implements OnInit, AfterViewInit {
    phone: String;
    username: String;
    accountInfoUsername: String;
    email: String;
    name: String;
    moneyAmount: Number;
    allFieldFilled: boolean;
    floatMoney: boolean;
    responsibler: any;
    newDataRow: string[];
    public dataTable: DataTable;
    constructor(private authService: AuthService) {}
    ngOnInit() {
        this.dataTable = {
        headerRow: [ 'Time', 'Responsible', 'Username', 'Amount' ],
        footerRow: [ 'Time', 'Responsible', 'Username', 'Amount' ],
        dataRows: [
        ]
        };
        this.allFieldFilled = false;
        this.floatMoney = true;
    }
    onDepositeSubmit() {
        const depositeForm = {
            username: this.username,
            money: this.moneyAmount
        };
        if (!this.validateDeposite(depositeForm)) {
            this.allFieldFilled = false;
            this.showSwal();
            return false;
        } else {
            this.allFieldFilled = true;
        }
        if ( this.moneyAmount === null || this.moneyAmount.toString().indexOf('.') !== -1) {
            this.floatMoney = true;
            this.showSwal();
            return false;
        } else {
            this.floatMoney = false;
        }
        this.confirmDeposite(depositeForm);
    }

    confirmDeposite(depositeForm) {
            swal({
                title: 'Xác Nhận',
                text: 'Nạp ' + depositeForm.money + ' Vào Tài Khoản ' + depositeForm.username,
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: 'Chắc Chắn',
                buttonsStyling: false
            }).then((result) => {
                if (result.value) {
                    this.authService.depositMoney(depositeForm).subscribe(data => {
                        if (data.success) {
                            swal(
                                {
                                    title: data.msg,
                                    type: 'success',
                                    confirmButtonClass: "btn btn-success",
                                    buttonsStyling: false
                                }
                            )
                        } else {
                            swal(
                                {
                                    title: data.msg,
                                    type: 'error',
                                    confirmButtonClass: "btn btn-success",
                                    buttonsStyling: false
                                }
                            )
                        }
                    });
                }
            })
    }
    onAccountChange() {
        const user = {
            username: this.username
        };
        this.authService.getUserInfo(user).subscribe( userInfo => {
            if (userInfo) {
                if (userInfo.success) {
                    this.phone = userInfo.data.phone;
                    this.email = userInfo.data.email;
                    this.name = userInfo.data.name;
                    this.accountInfoUsername = userInfo.data.username;
                    this.responsibler = userInfo.responsibler;
                } else {
                    this.accountInfoUsername = 'User Not Found';
                }
            } else {
                this.accountInfoUsername = 'User Not Found';
            }
        });
    }
    showSwal() {
        if (!this.allFieldFilled) {
            swal({
                title: "Fill In All Field!",
                text: "Hãy Điền Đầy Đủ!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
        } else if (this.floatMoney) {
            swal({
                title: "Money Amount Is Not Allowed",
                text: "Vui Lòng Nhập Số Tiền Hợp Lệ!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
        }
    }
    validateDeposite(depositeForm) {
        if (depositeForm.username === undefined || depositeForm.money === undefined) {
            return false;
        } else {
            return true;
        }
    }
    usernameDefine() {
        if (this.username === undefined) {
            return false;
        }
        return true;
    }
    moneySeparate(amount) {
        const parts = amount.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
    ngAfterViewInit() {
        const table = $('#datatables').DataTable({
            order: [[ 0, "desc" ]],
            columnDefs: [
                {
                    targets: -1,
                    className: 'text-right'
                }
            ]
        } );
        this.authService.getMoneyLogs().subscribe( logs => {
            for ( const log of logs) {
                const ISOTime = new Date(log.time).toISOString();
                this.newDataRow = [];
                this.newDataRow.push(ISOTime.replace(/T/, ' ').replace(/\..+/, ''));
                // this.newDataRow.push(log.responsibler);
                this.newDataRow.push(log.account);
                this.newDataRow.push(log.description);
                this.newDataRow.push(this.moneySeparate(log.amount.toFixed(2)));
                table.row.add(this.newDataRow).draw( false );
            }
        });
    }
}
