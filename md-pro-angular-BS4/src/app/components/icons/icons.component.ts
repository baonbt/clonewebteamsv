import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth.service";

@Component({
    selector: 'app-icons-cmp',
    templateUrl: 'icons.component.html'
})

export class IconsComponent implements OnInit, AfterViewInit{
    errLogsTable: any;
    newLogRow: any;
    botLogsTable: any;
    newBotLogRow: any;

    constructor(private authService: AuthService) {}
    ngOnInit(): void {
        this.errLogsTable = [];
        this.botLogsTable = [];

    }

    ngAfterViewInit() {
        const logTable = $('#errlogtable').DataTable({
            order: [[ 0, "desc" ]],
            columnDefs: [
                {
                    targets: -1,
                    className: 'text-right'
                },
                {
                    width: "20%",
                    targets: 0
                },
                {
                    width: "70%",
                    targets: 1
                }
            ]
        } );
        this.authService.getErrLogs().subscribe(data=>{
            if(data.success){
                for ( const log of data.user) {
                    const passedTime = new Date().getTime()-log.time;
                    if( passedTime>86400000 && log.fixStatus){

                    } else {
                        const ISOTime = new Date(log.time).toISOString();
                        this.newLogRow = [];
                        this.newLogRow.push(ISOTime.replace(/T/, ' ').replace(/\..+/, ''));
                        // this.newDataRow.push(log.responsibler);
                        this.newLogRow.push(log.infoDes);
                        if(log.fixStatus){
                            this.newLogRow.push("Đã Sửa");
                        } else {
                            this.newLogRow.push("Chưa Sửa");
                        }
                        logTable.row.add(this.newLogRow).draw( false );
                    }
                }

            }
        });

        const botLogTable = $('#botlogtable').DataTable({
            order: [[ 0, "desc" ]],
            columnDefs: [
                {
                    targets: -1,
                    className: 'text-right'
                },
                {
                    width: "20%",
                    targets: 0
                },
                {
                    width: "70%",
                    targets: 1
                }
            ]
        } );
    }
}
