import { Routes } from '@angular/router';

import { ButtonsComponent } from './buttons/buttons.component';
import { GridSystemComponent } from './grid/grid.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { PanelsComponent } from './panels/panels.component';
import { SweetAlertComponent } from './sweetalert/sweetalert.component';
import { TypographyComponent } from './typography/typography.component';
import { ManagerGuard } from '../services/manager.guard';
import { SuperadminGuard } from '../services/superadmin.guard';
import { WidgetsComponent } from '../widgets/widgets.component';


export const ComponentsRoutes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
    },
    {
      path: '',
        canActivate: [ManagerGuard],
      children: [ {
        path: 'createaccount',
        component: ButtonsComponent
    }]}, {
    path: '',
        canActivate: [ManagerGuard],
    children: [ {
      path: 'deposit',
      component: GridSystemComponent
    }]
    }, {
        path: '',
        canActivate: [SuperadminGuard],
        children: [ {
            path: 'superadmin',
            component: WidgetsComponent
        }]
    }, {
        path: '',
        canActivate: [SuperadminGuard],
        children: [ {
            path: 'logs',
            component: IconsComponent
        }]
    }, {
        path: '',
        canActivate: [ManagerGuard],
        children: [ {
            path: 'checkpoints',
            component: NotificationsComponent
        }]
    }
];
