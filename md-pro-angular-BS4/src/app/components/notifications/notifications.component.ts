// IMPORTANT: this is a plugin which requires jQuery for initialisation and data manipulation

import {AfterViewInit, Component, OnInit} from '@angular/core';
import {provideAuth} from "angular2-jwt";
import {AuthService} from "../../services/auth.service";

declare const $: any;

@Component({
    selector: 'app-notifications-cmp',
    templateUrl: 'notifications.component.html'
})
export class NotificationsComponent implements OnInit, AfterViewInit {

    // localhost: String = 'http://localhost:9999/';
    localhost: String = '';

    constructor(private authService: AuthService) {}

    ngOnInit(){
        // this.authService.getCheckPoints().subscribe( logs => {
        //     console.log(logs);
        // });
    }

    ngAfterViewInit() {
        $('#datatables').DataTable({
            ajax: {
                url: this.localhost + 'logs/checkpoints',
                type: 'GET',
                beforeSend: function (request) {
                    const token = window.localStorage.getItem('id_token');
                    request.setRequestHeader("Authorization", token);
                }
            },
            pagingType: "full_numbers",
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            },
            columnDefs:[
                {
                    "width": "10%",
                    "targets": 0,
                }
            ]
        });
    }
}
