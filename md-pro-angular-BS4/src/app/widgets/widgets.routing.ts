import { Routes } from '@angular/router';
import { SuperadminGuard } from '../services/superadmin.guard';

import { WidgetsComponent } from './widgets.component';

export const WidgetsRoutes: Routes = [
    {
      path: '',
      children: [ {
        path: 'tools',
        component: WidgetsComponent
    }]
}
];
