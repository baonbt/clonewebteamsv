import { Component, OnInit } from '@angular/core';
import { TableData } from '../md/md-table/md-table.component';
import swal from "sweetalert2";
import { AuthService } from '../services/auth.service';

declare var $: any;
declare interface Task {
  title: string;
  checked: boolean;
}
@Component({
    selector: 'app-widgets-cmp',
    templateUrl: 'widgets.component.html'
})

export class WidgetsComponent implements OnInit {
    usernameToReset: String;
    autoTymPriceBasePrice: Number;
    autoTymPriceOneMonthDiscount: Number;
    autoTymPriceThreeMonthDiscount: Number;
    autoTymPriceSixMonthDiscount: Number;

    buffLikePriceBasePrice: Number;
    buffLikePriceOneMonthDiscount: Number;
    buffLikePriceThreeMonthDiscount: Number;
    buffLikePriceSixMonthDiscount: Number;
    buffLikePrice50LikeDiscount: Number;
    buffLikePrice100LikeDiscount: Number;
    buffLikePrice200LikeDiscount: Number;
    buffLikePrice300LikeDiscount: Number;
    buffLikePrice600LikeDiscount: Number;
    buffLikePrice1200LikeDiscount: Number;


    commentbasePrice : Number;
    commentoneMonthDiscount : Number;
    commentsixMonthDiscount : Number;
    commenttenCmtDiscount : Number;
    commentthirtyCmtDiscount : Number;
    commentthreeMonthDiscount : Number;
    commenttwentyCmtDiscount : Number;

    testConnectionResult: any;

    constructor(
        private authService: AuthService
    ) {}
    ngOnInit() {
        this.authService.getAutoTymPrice().subscribe(data => {
            if (data.success) {
                this.autoTymPriceBasePrice = data.price.basePrice;
                this.autoTymPriceOneMonthDiscount = data.price.oneMonthDiscount;
                this.autoTymPriceSixMonthDiscount = data.price.sixMonthDiscount;
                this.autoTymPriceThreeMonthDiscount = data.price.threeMonthDiscount;
            }
        });
        this.authService.getBuffLikePrice().subscribe(data => {
            if (data.success) {
                this.buffLikePriceBasePrice = data.price.basePrice;
                this.buffLikePriceOneMonthDiscount = data.price.oneMonthDiscount;
                this.buffLikePriceThreeMonthDiscount = data.price.threeMonthDiscount;
                this.buffLikePriceSixMonthDiscount = data.price.sixMonthDiscount;
                this.buffLikePrice50LikeDiscount = data.price.fiftyLikeDiscount;
                this.buffLikePrice100LikeDiscount = data.price.oneHundredLikeDiscount;
                this.buffLikePrice200LikeDiscount = data.price.twoHundredLikeDiscount;
                this.buffLikePrice300LikeDiscount = data.price.threeHundredLikeDiscount;
                this.buffLikePrice600LikeDiscount = data.price.sixHundredLikeDiscount;
                this.buffLikePrice1200LikeDiscount = data.price.oneThousandTwoHundredLikeDiscount;
            }
        });
        this.authService.getCommentPrice().subscribe(data=>{
            if(data.success){
                this.commentbasePrice = data.price.basePrice;
                this.commentoneMonthDiscount = data.price.oneMonthDiscount;
                this.commentsixMonthDiscount = data.price.sixMonthDiscount;
                this.commenttenCmtDiscount = data.price.tenCmtDiscount;
                this.commentthirtyCmtDiscount = data.price.thirtyCmtDiscount;
                this.commentthreeMonthDiscount = data.price.threeMonthDiscount;
                this.commenttwentyCmtDiscount = data.price.twentyCmtDiscount;
            }
        });
    }
    onResetUserPasswordClick() {
        if (this.usernameToReset === undefined) {
            swal({
                title: "Fill In All Field!",
                text: "Hãy Điền Đầy Đủ!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        const changePassBundle = {
            username: this.usernameToReset
        };
        swal({
            title: 'Bạn Có Chắc Chắn',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.resetUserPassword(changePassBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.usernameToReset = '';
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

    ontestConnection(){
        this.authService.testConnection().subscribe(data => {
            console.log(data);
            if (data.success) {
                this.testConnectionResult = 'Success ' + data.body;
            } else {
                this.testConnectionResult = 'ERR';
            }
        });
    }

    updateAutoTymPrice() {
        if (this.autoTymPriceBasePrice === undefined || this.autoTymPriceOneMonthDiscount === undefined || this.autoTymPriceThreeMonthDiscount === undefined || this.autoTymPriceSixMonthDiscount === undefined ) {
            swal({
                title: "Fill In All Field!",
                text: "Hãy Điền Đầy Đủ!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        const dataBundle = {
            basePrice: this.autoTymPriceBasePrice,
            oneMonthDiscount: this.autoTymPriceOneMonthDiscount,
            threeMonthDiscount: this.autoTymPriceThreeMonthDiscount,
            sixMonthDiscount: this.autoTymPriceSixMonthDiscount
        };
        swal({
            title: 'Bạn Có Chắc Chắn',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.updateAutoTymPrice(dataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.usernameToReset = '';
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }
    updateBuffLikePrice() {
        if (this.buffLikePriceBasePrice === undefined ||
            this.buffLikePriceOneMonthDiscount === undefined ||
            this.buffLikePriceThreeMonthDiscount === undefined ||
            this.buffLikePriceSixMonthDiscount === undefined ||
            this.buffLikePrice50LikeDiscount === undefined ||
            this.buffLikePrice100LikeDiscount === undefined ||
            this.buffLikePrice200LikeDiscount === undefined ||
            this.buffLikePrice300LikeDiscount === undefined ||
            this.buffLikePrice600LikeDiscount === undefined ||
            this.buffLikePrice1200LikeDiscount === undefined) {
            swal({
                title: "Fill In All Field!",
                text: "Hãy Điền Đầy Đủ!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        const dataBundle = {
            basePrice: this.buffLikePriceBasePrice,
            oneMonthDiscount: this.buffLikePriceOneMonthDiscount,
            threeMonthDiscount: this.buffLikePriceThreeMonthDiscount,
            sixMonthDiscount: this.buffLikePriceSixMonthDiscount,
            fiftyLikeDiscount: this.buffLikePrice50LikeDiscount,
            oneHundredLikeDiscount: this.buffLikePrice100LikeDiscount,
            twoHundredLikeDiscount: this.buffLikePrice200LikeDiscount,
            threeHundredLikeDiscount: this.buffLikePrice300LikeDiscount,
            sixHundredLikeDiscount: this.buffLikePrice600LikeDiscount,
            oneThousandTwoHundredLikeDiscount: this.buffLikePrice1200LikeDiscount,
        };
        swal({
            title: 'Bạn Có Chắc Chắn',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.updateBuffLikePrice(dataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.usernameToReset = '';
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }
    updateCommentServPrice() {
        if (this.autoTymPriceBasePrice === undefined || this.autoTymPriceOneMonthDiscount === undefined || this.autoTymPriceThreeMonthDiscount === undefined || this.autoTymPriceSixMonthDiscount === undefined ) {
            swal({
                title: "Fill In All Field!",
                text: "Hãy Điền Đầy Đủ!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        const dataBundle = {
            basePrice: this.commentbasePrice,
            oneMonthDiscount: this.commentoneMonthDiscount,
            threeMonthDiscount: this.commentthreeMonthDiscount,
            sixMonthDiscount: this.commentsixMonthDiscount,
            tenCmtDiscount: this.commenttenCmtDiscount,
            twentyCmtDiscount: this.commenttwentyCmtDiscount,
            thirtyCmtDiscount: this.commentthirtyCmtDiscount
        };
        swal({
            title: 'Bạn Có Chắc Chắn',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.updateCommentServPrice(dataBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        );
                        this.usernameToReset = '';
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        });
    }

}
