import { Component } from '@angular/core';
import swal from "sweetalert2";
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-user-cmp',
    templateUrl: 'user.component.html'
})

export class UserComponent {
    oldPassword: String;
    newPassword: String;
    reNewPassword: String;

    constructor(
        private authService: AuthService
    ) {}

    confirmChangePassword() {
        if (this.oldPassword === undefined || this.newPassword === undefined || this.reNewPassword === undefined) {
            swal({
                title: "Fill In All Field!",
                text: "Hãy Điền Đầy Đủ!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        if (this.newPassword !== this.reNewPassword){
            swal({
                title: "Mật Khẩu Mới Không Trùng Khớp",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        const changePassBundle = {
            oldPassword: this.oldPassword,
            newPassword: this.newPassword
        };
        swal({
            title: 'Bạn Có Chắc Chắn',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.changePassword(changePassBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        })
    }
}
