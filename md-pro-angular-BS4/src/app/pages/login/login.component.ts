import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import swal from 'sweetalert2';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { tokenNotExpired } from 'angular2-jwt';
import { Router } from '@angular/router';
import { AuthService } from "../../services/auth.service";


declare var $: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {


    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;
    username: String;
    password: String;
    authToken: any;
    user: any;
    constructor(private element: ElementRef,
                private http: Http,
                private router: Router,
                private authService: AuthService) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
    }

    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);
    }
    onLetsGoClick() {
        if (!this.showSwal()) {
            return false;
        }
        const user = {
            username: this.username,
            password: this.password
        };
        this.authService.authenticateUser(user).subscribe(data => {
            if (data.success) {
                this.storeUserData(data.token, data.user);
                this.showAuthSwal(data.success, data.msg);
                this.router.navigate(['dashboard']);
            } else {
                this.showAuthSwal(data.success, data.msg);
            }
        });
    }
    ngOnDestroy() {
      const body = document.getElementsByTagName('body')[0];
      body.classList.remove('login-page');
      body.classList.remove('off-canvas-sidebar');
    }
    showSwal() {
        if (this.username === undefined || this.password === undefined) {
            swal({
                title: "Fill In All Field!",
                text: "Vui Lòng Điền Đầy Đủ Tài Khoản Và Mật Khẩu!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        return true;
    }
    showAuthSwal(dataSuccess, dataMsg) {
        console.log(dataMsg);
        if (dataSuccess) {
            swal({
                title: "Log In Success",
                text: dataMsg,
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop);
            return true;
        } else {
            swal({
                title: "Log In Failed",
                text: dataMsg,
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
    }

    loggedIn() {
        const token1 = localStorage.getItem('id_token');
        if (token1 == null) {
            return false;
        } else {
            return tokenNotExpired('id_token');
        }
    }
    loadToken() {
        const token = window.localStorage.getItem('id_token');
        this.authToken = token;
    }
    storeUserData(token, user){
        window.localStorage.setItem('id_token', token);
        window.localStorage.setItem('user', JSON.stringify(user));
        this.authToken = token;
        this.user = user;
    }
}
