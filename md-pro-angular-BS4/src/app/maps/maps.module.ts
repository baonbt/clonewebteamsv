import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { MapsRoutes } from './maps.routing';

import { FullScreenMapsComponent } from './fullscreenmap/fullscreenmap.component';
import { GoogleMapsComponent } from './googlemaps/googlemaps.component';
import { VectorMapsComponent } from './vectormaps/vectormaps.component';
import {NouisliderModule} from "ng2-nouislider";
import {TagInputModule} from "ngx-chips";
import {MaterialModule} from "../app.module";
import {HttpClientModule} from "../../../node_modules/@angular/common/http";
import {MatTableModule} from "@angular/material";
import {ExtendedFormsComponent} from "../forms/extendedforms/extendedforms.component";
import {RegularFormsComponent} from "../forms/regularforms/regularforms.component";
import {ValidationFormsComponent} from "../forms/validationforms/validationforms.component";
import {WizardComponent} from "../forms/wizard/wizard.component";
import {FieldErrorDisplayComponent} from "../forms/validationforms/field-error-display/field-error-display.component";


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MapsRoutes),
    FormsModule,
      ReactiveFormsModule,
      NouisliderModule,
      TagInputModule,
      MaterialModule,
      HttpClientModule,
      MatTableModule
  ],
  declarations: [
      FullScreenMapsComponent,
      GoogleMapsComponent,
      VectorMapsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class MapsModule {}
