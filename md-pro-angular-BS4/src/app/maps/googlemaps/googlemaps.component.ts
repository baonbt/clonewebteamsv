import {AfterViewInit, Component, OnInit} from '@angular/core';
import swal from "sweetalert2";

declare const $: any;
@Component({
    selector: 'app-vector-maps-cmp',
    templateUrl: './googlemaps.component.html'
})

export class GoogleMapsComponent implements OnInit, AfterViewInit {
    remainingDays: any;
    oneMonthPrice: any;
    sharedPostTable: any;
    groupBundleTable: any;
    sharedLinkTable: any;
    groupValuationTable: any;
    remainingShareNum: any;
    groupBundleSelection: any;
    currentBundle: any;
    captionContent: any;
    createNewShare: boolean = false;
    createNewGroupBundle: boolean = false;
    editGroupBundle: boolean = false;
    currentGroupIdList: any;
    moreShareNum: any;
    shareNumPrice: any;
    ngOnInit() {
        this.shareNumPrice = 100;
        this.groupBundleSelection = [
            {_id: 'bundleId1', bundleName: 'bundle1'},
            {_id: 'bundleId2', bundleName: 'bundle2'},
            {_id: 'bundleId3', bundleName: 'bundle3'},
            {_id: 'bundleId4', bundleName: 'bundle4'},
            {_id: 'botWall', bundleName: 'Share Lên Tường Cá Nhân'}
        ];
        this.remainingShareNum = 200;
        this.remainingDays = 30;
        this.oneMonthPrice = 200000;
        this.sharedPostTable = [
            {time: '2018-08-19 11:24:11', postLink:'https://www.facebook.com/groups/670659939761616/permalink/1116166251877647/', status:'CHECKING', shareNum:'5/10', totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {time: '2018-08-19 11:20:11',postLink:'plink2', status:'CHECKING', shareNum:'10/10', totalLikes:250, totalComments:50, averageLikes: 25, averageComments: 5}
        ];
        this.sharedLinkTable = [
            {time: '2018-08-19 11:24:11', link: 'https://www.facebook.com/groups/670659939761616//', originLink:'https://www.facebook.com/670659939761616/permalink/1116166251877647/', groupBundleName:20, likes:250,comments:50},
            {time: '2018-08-19 11:24:11', link: 'https://www.facebook.com/groups/670659939761616///', originLink:'https://www.facebook.com/670659939761616/permalink/1116166251877647/', groupBundleName:20, likes:250,comments:50},
            {time: '2018-08-19 11:24:11', link: 'https://www.facebook.com/groups/670659939761616///', originLink:'https://www.facebook.com/670659939761616/permalink/1116166251877647/', groupBundleName:20, likes:250,comments:50},
            {time: '2018-08-19 11:24:11', link: 'https://www.facebook.com/groups/670659939761616///', originLink:'https://www.facebook.com/670659939761616/permalink/1116166251877647/', groupBundleName:20, likes:250,comments:50},
            {time: '2018-08-19 11:24:11', link: 'https://www.facebook.com/groups/670659939761616///', originLink:'https://www.facebook.com/670659939761616/permalink/1116166251877647/', groupBundleName:20, likes:250,comments:50}
        ];
        this.groupBundleTable = [
            {bundleName: 'name1', joinStatus:'10/128', sharedNum:20, totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {bundleName: 'name2', joinStatus:'20/128', sharedNum:20, totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {bundleName: 'name3', joinStatus:'30/128', sharedNum:20, totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {bundleName: 'name4', joinStatus:'40/128', sharedNum:20, totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {bundleName: 'name5', joinStatus:'50/128', sharedNum:20, totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10}
        ];
        this.groupValuationTable = [
            {groupId: 'ID1', groupName:'name1', groupType:'sell-pulic', groupBundleName:'bundle1', sharedTime:20,totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {groupId: 'ID2', groupName:'name2',groupType:'sell-pulic', groupBundleName:'bundle1', sharedTime:20,totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {groupId: 'ID3', groupName:'name3', groupType:'sell-pulic', groupBundleName:'bundle1', sharedTime:20,totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {groupId: 'ID4', groupName:'name4', groupType:'sell-pulic', groupBundleName:'bundle1', sharedTime:20,totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {groupId: 'ID5', groupName:'name5', groupType:'sell-pulic', groupBundleName:'bundle1', sharedTime:20,totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
            {groupId: 'ID6', groupName:'name6', groupType:'sell-pulic', groupBundleName:'bundle1', sharedTime:20,totalLikes:250, totalComments:50, averageLikes: 50, averageComments: 10},
        ];
    }


    shareNumPriceCal(shareNum){
        if(shareNum>200){
            return this.shareNumPrice*shareNum - this.shareNumPrice*shareNum*0.1;
        } else if(shareNum>100){
            return this.shareNumPrice*shareNum - this.shareNumPrice*shareNum*0.05;
        }
        return this.shareNumPrice*shareNum;
    }

    buyMoreShareNum(){
        swal({
            title: 'Bạn Muốn Thêm Lượt Chia Sẻ?',
            text: this.shareNumPrice + 'đ/1 lượt, giảm ngay 5% khi mua trên 100 Lượt và 10% khi mua trên 200 Lượt',
            input: 'number',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then((result) => {
            if(result.value){
                swal({
                    title: 'Bạn Có Chắc Chắn?',
                    text: "Thanh toán " + this.shareNumPriceCal(result.value) + 'đ cho ' + result.value + ' lượt Chia Sẻ',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonClass: 'btn btn-success',
                    cancelButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Xác Nhận',
                    buttonsStyling: false
                }).then((_result) => {
                    if (_result.value) {
                        swal(
                            {
                                title: 'Thanh Toán Thành Công!',
                                text: 'Your file has been deleted.',
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                })
            }
            return false;

        }).catch(swal.noop)
    }

    createNewGroupBundleClicked(){
        this.createNewGroupBundle = true;
    }

    createNewShareClicked(){
        this.createNewShare = true;
    }

    cancelCreateNewShareClicked(){
        this.createNewShare = false;
        this.createNewGroupBundle = false;
        this.editGroupBundle = false;
    }

    ngAfterViewInit(){
        $('a[data-toggle="tab"]').on( 'shown.bs.tab', function (e) {
            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        } );

        $('#sharedPostTable').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            "columnDefs": [
                { "width": "40%", "targets": 1 }
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
        $('#groupValuationTable').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
        $('#sharedLinkValuationTable').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            columnDefs: [
                { "width": "10%", "targets": 0 },
                { "width": "40%", "targets": 1 },
                { "width": "40%", "targets": 2 }
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
        $('#groupBundleTable').DataTable({
            pagingType: "full_numbers",
            lengthMenu: [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });
    }
}
