import { Component, OnInit, AfterViewInit } from '@angular/core';
import { TableData } from '../md/md-table/md-table.component';
import { LegendItem, ChartType } from '../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {AuthService} from '../services/auth.service';
import swal from "sweetalert2";

declare interface DataTable {
    headerRow: string[];
    footerRow: string[];
    dataRows: string[];
}

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit, AfterViewInit {
  public dataTable: DataTable;
    newDataRow: string[];
    totalPayment: Number;
    last30DaysPayment: Number;
    totalPaymentString: String;
    last30DaysPaymentString: String;
    changePassword: boolean;
    oldPassword: String;
    newPassword: String;
    reNewPassword: String;
    constructor(private authService: AuthService) {}
  public ngOnInit() {
        this.changePassword = false;
        this.totalPayment = 0;
        this.last30DaysPayment = 0;
      this.dataTable = {
          headerRow: [ 'Time', 'Responsible', 'Username', 'Amount' ],
          footerRow: [ 'Time', 'Responsible', 'Username', 'Amount' ],
          dataRows: [
          ]
      };
}
    ngAfterViewInit() {
        const table = $('#datatables').DataTable({
            order: [[ 0, "desc" ]],
            columnDefs: [
                {
                    targets: -1,
                    className: 'text-right'
                }
            ]
        } );
        this.authService.getMoneyLogs().subscribe( logs => {
            for ( const log of logs) {
                const ISOTime = new Date(log.time).toISOString();
                this.newDataRow = [];
                this.newDataRow.push(ISOTime.replace(/T/, ' ').replace(/\..+/, ''));
                // this.newDataRow.push(log.responsibler);
                this.newDataRow.push(log.account);
                this.newDataRow.push(log.description);
                this.newDataRow.push(this.moneySeparate(log.amount.toFixed(2)));
                table.row.add(this.newDataRow).draw( false );
                const thirdtyDays = new Date().getTime() - 30 * 24 * 60 * 60 * 1000;
                if (log.amount < 0) {
                    if (log.amount < 0) {
                        this.totalPayment = this.totalPayment + log.amount;
                    }
                    if (log.time > thirdtyDays) {
                        if (log.amount < 0) {
                            this.last30DaysPayment = this.last30DaysPayment + log.amount;
                        }
                    }
                }
            }
            this.last30DaysPaymentString = this.moneySeparate(this.last30DaysPayment.toFixed(2));
            this.totalPaymentString = this.moneySeparate(this.totalPayment.toFixed(2));
        });
    }

    changePasswordClick(){
        this.changePassword = true;
    }

    confirmChangePassword() {
        if (this.oldPassword === undefined || this.newPassword === undefined || this.reNewPassword === undefined) {
            swal({
                title: "Fill In All Field!",
                text: "Hãy Điền Đầy Đủ!",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        if (this.newPassword !== this.reNewPassword){
            swal({
                title: "Mật Khẩu Mới Không Trùng Khớp",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-info"
            }).catch(swal.noop)
            return false;
        }
        const changePassBundle = {
            oldPassword: this.oldPassword,
            newPassword: this.newPassword
        };
        swal({
            title: 'Bạn Muốn Thay Đổi Mật Khẩu',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Chắc Chắn',
            buttonsStyling: false
        }).then((result) => {
            if (result.value) {
                this.authService.changePassword(changePassBundle).subscribe(data => {
                    if (data.success) {
                        swal(
                            {
                                title: data.msg,
                                type: 'success',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    } else {
                        swal(
                            {
                                title: data.msg,
                                type: 'error',
                                confirmButtonClass: "btn btn-success",
                                buttonsStyling: false
                            }
                        )
                    }
                });
            }
        })
    }

    moneySeparate(amount) {
        const parts = amount.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }
}
