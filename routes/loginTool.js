const express = require('express');
const router = express.Router();
const passport = require('passport');
var AUTH = passport.authenticate('jwt', {session:false});
const request = require('request');
const errLog = require('../models/errLog');
const roles = require('../guards/levels');

const _botsvAddress = 'http://localhost:8080';

const botsvAddress = _botsvAddress+'/angularwebselling';

const getProxyForNewCloneAddress = _botsvAddress+'/account/getinfo/v3';

const pushNewCloneAddress = _botsvAddress+'/account/save';
const getCloneInfoAddress = _botsvAddress+'/account/search/info';
const disableCloneAddress = _botsvAddress+'/account//disable';
const changeCloneProxyAddress = _botsvAddress+'/account/proxy/change';


router.post('/loginnewclone', AUTH, roles.Auth(6), function (req, res, next) {
    const toBotSvBundle = {
        workingUID: req.body.uid
    };
    request.post({
        url: getProxyForNewCloneAddress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/loginnewclone', err.stack, 'na');
        }
        // let newjs = JSON.parse(body);
            return res.json(body);
    });
});

router.post('/loggednewclone', AUTH, roles.Auth(6), function (req, res, next) {
    const toBotSvBundle = {
        loggedAccount: req.body.uid,
        cookies: req.body.cookies,
        proxyId: req.body.proxyId,
        password: req.body.password
    };
    request.post({
        url: pushNewCloneAddress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/loggednewclone', err.stack, 'na');
            return res.json({success: false, msg: "đã xảy ra lỗi"});
        }
        return res.json({success: true, msg: "push thông tin thành công"});
    });
});

router.post('/getcloneinfo', AUTH, roles.Auth(6), function (req, res, next) {
    request.post({
        url: getCloneInfoAddress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: req.body.uid
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(req.body.uid, '/loginnewclone', err.stack, 'na');
        }
        // let newjs = JSON.parse(body);
        return res.json(body);
    });
});

router.post('/updatecloneinfo', AUTH, roles.Auth(6), function (req, res, next) {
    const toBotSvBundle = {
        loggedAccount: req.body.uid,
        cookies: req.body.cookies,
        proxyId: req.body.proxyId,
        password: req.body.password,
        email: req.body.email
    };
    request.post({
        url: pushNewCloneAddress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/updateCloneInfo', err.stack, 'na');
            return res.json({success: false, msg: "đã xảy ra lỗi"});
        }
        return res.json({success: true, msg: "push thông tin thành công"});
    });
});

router.post('/getnewproxyforclone', AUTH, roles.Auth(6), function (req, res, next) {
    request.post({
        url: changeCloneProxyAddress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: req.body.uid
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(req.body.uid, '/loginnewclone', err.stack, 'na');
        }
        // let newjs = JSON.parse(body);
        return res.json(body);
    });
});

router.post('/disableclone', AUTH, roles.Auth(6), function (req, res, next) {
    console.log(req.body.uid);
    request.post({
        url: disableCloneAddress,
        method: "POST",
        body: req.body.uid
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(req.body.uid, '/disableclone', err.stack, 'na');
            return res.json({success: false, msg: "đã xảy ra lỗi"});
        }
        return res.json({success: true, msg: "push thông tin thành công"});
    });
});
module.exports = router;