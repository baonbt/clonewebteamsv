const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const passport = require('passport');
var AUTH = passport.authenticate('jwt', {session: false});
const roles = require('../../guards/levels');
const moneyLog = require('../../models/moneyLog');
const buffLikeServicePrice = require('../../models/buffLikeServicePrice');
const buffLikeService = require('../../models/buffLikeService');
const request = require('request');
const errLog = require('../../models/errLog');
const reqToBotSv = require('../botSv');
const URI = require('urijs');


module.exports.getUidFromProfileLink = function (profileLink) {

    if (profileLink.indexOf("profile.php") === -1) {
        const segments = URI.parse(profileLink).path.split('/');

        return segments[1];
    } else {
        const params = URI(profileLink).query(true).id;

        return params;
    }
};


router.post('/updateprice', AUTH, roles.Auth(99), (req, res, next) => {
    let newPrice = {
        serviceName: 'buffLike',
        basePrice: req.body.basePrice,
        oneMonthDiscount: req.body.oneMonthDiscount,
        threeMonthDiscount: req.body.threeMonthDiscount,
        sixMonthDiscount: req.body.sixMonthDiscount,
        fiftyLikeDiscount: req.body.fiftyLikeDiscount,
        oneHundredLikeDiscount: req.body.oneHundredLikeDiscount,
        twoHundredLikeDiscount: req.body.twoHundredLikeDiscount,
        threeHundredLikeDiscount: req.body.threeHundredLikeDiscount,
        sixHundredLikeDiscount: req.body.sixHundredLikeDiscount,
        oneThousandTwoHundredLikeDiscount: req.body.oneThousandTwoHundredLikeDiscount
    };
    buffLikeServicePrice.updateBuffLikeServicePrice(newPrice, (err) => {
        if (err) {
            errLog.addErrLog(req.user, 'cập nhật giá buff like', err.stack, 'na');
            return res.json({success: false, msg: 'Update BuffLike Service Price Failed'});
        } else {
            return res.json({success: true, msg: 'Updated BuffLike Service Price'});
        }
    })
});

router.post('/renewservice', AUTH, (req, res, next)=>{
    buffLikeService.getBuffLikeServiceById(req.body.id, (err, respondData)=>{
        if (err) {
            errLog.addErrLog(req.user, '/renewservice-getBuffLikeServiceById', err.stack, 'na');

            return res.json({success: false, msg: 'Mua Thêm Dịch Vụ buff like Không Thành Công'});
        }
        if(respondData){
            buffLikeServicePrice.getBuffLikeServicePrice((err, price)=>{
                if (err) {
                    errLog.addErrLog(req.user, '/renewservice-getBuffLikeServiceById-getBuffLikeServicePrice', err.stack, 'na');
                    return res.json({success: false, msg: 'Mua Thêm Dịch Vụ buff like Không Thành Công'});
                }
                if(price){
                    let period = 0;
                    if (req.body.buyMoreDuration === 'oneMonth') {
                        period = 1;
                    } else if (req.body.buyMoreDuration === 'threeMonth') {
                        period = 3;
                    } else if (req.body.buyMoreDuration === 'sixMonth') {
                        period = 6;
                    }
                    let exTime = (respondData.expiredTime - (new Date().getTime())) / (1000 * 60 * 60 * 24);
                    if(exTime < 0){
                        exTime = 0;
                    }
                    let durationDiscountNum = 0;
                    const remainingPeriod = exTime/30;
                    if (period + remainingPeriod >= 6) {
                        durationDiscountNum = price.sixMonthDiscount;
                    } else if (this.period + remainingPeriod >= 3) {
                        durationDiscountNum = price.threeMonthDiscount;
                    } else if (this.period + remainingPeriod >= 1) {
                        durationDiscountNum = price.oneMonthDiscount;
                    }

                    let likeDiscountNum = 0;
                    if (respondData.likeNum + req.body.buyMoreLike >= 1200) {
                        likeDiscountNum = price.oneThousandTwoHundredLikeDiscount;
                    } else if (respondData.likeNum + req.body.buyMoreLike >= 600) {
                        likeDiscountNum = price.sixHundredLikeDiscount;
                    } else if (respondData.likeNum + req.body.buyMoreLike >= 300) {
                        likeDiscountNum = price.threeHundredLikeDiscount;
                    } else if (respondData.likeNum + req.body.buyMoreLike >= 200) {
                        likeDiscountNum = price.twoHundredLikeDiscount;
                    } else if (respondData.likeNum + req.body.buyMoreLike >= 100) {
                        likeDiscountNum = price.oneHundredLikeDiscount;
                    } else if (respondData.likeNum + req.body.buyMoreLike >= 50) {
                        likeDiscountNum = price.fiftyLikeDiscount;
                    }

                    let totalPriceNum = 0;
                    let finalPriceNum = 0;

                    if(period === 0 && req.body.buyMoreLike === 0 && req.body.buyMorePost > 0){
                        totalPriceNum = remainingPeriod * respondData.likeNum * price.basePrice*(req.body.buyMorePost/10);
                    }
                    if(period === 0 && req.body.buyMoreLike > 0 && req.body.buyMorePost === 0){
                        totalPriceNum = remainingPeriod * req.body.buyMoreLike * price.basePrice*(respondData.maxMaxPerDay/10);
                    }
                    if(period === 0 && req.body.buyMoreLike > 0 && req.body.buyMorePost > 0){
                        totalPriceNum = remainingPeriod * respondData.likeNum * price.basePrice*(req.body.buyMorePost/10);
                        totalPriceNum = totalPriceNum + remainingPeriod * req.body.buyMoreLike * price.basePrice*(req.body.buyMorePost/10 + respondData.maxMaxPerDay/10);
                    }
                    if(period > 0 && req.body.buyMoreLike === 0 && req.body.buyMorePost === 0){
                        totalPriceNum = period*respondData.likeNum*price.basePrice*(respondData.maxMaxPerDay/10);
                    }
                    if(period > 0 && req.body.buyMoreLike === 0 && req.body.buyMorePost > 0){
                        totalPriceNum = remainingPeriod * respondData.likeNum * price.basePrice*(req.body.buyMorePost/10);
                        totalPriceNum = totalPriceNum + period * respondData.likeNum * price.basePrice*(req.body.buyMorePost/10+respondData.maxMaxPerDay/10);
                    }
                    if(period > 0 && req.body.buyMoreLike > 0 && req.body.buyMorePost === 0){
                        totalPriceNum = remainingPeriod * req.body.buyMoreLike * price.basePrice*(respondData.maxMaxPerDay/10);
                        totalPriceNum = totalPriceNum + period * (respondData.likeNum +req.body.buyMoreLike)* price.basePrice*(respondData.maxMaxPerDay/10);
                    }
                    if(period > 0 && req.body.buyMoreLike > 0 && req.body.buyMorePost > 0){
                        totalPriceNum = remainingPeriod * respondData.likeNum * price.basePrice*(req.body.buyMorePost/10);
                        totalPriceNum = totalPriceNum + (period+remainingPeriod) * (respondData.likeNum +req.body.buyMoreLike)* price.basePrice*(req.body.buyMorePost/10+respondData.maxMaxPerDay/10);
                    }

                    const totalDiscountNum = likeDiscountNum + durationDiscountNum;
                    finalPriceNum = totalPriceNum - totalDiscountNum * totalPriceNum;
                    if(period === 0 && req.body.buyMoreLike === 0 && req.body.buyMorePost === 0){
                        return res.json({success: false, msg: 'Bạn Có Gia Hạn Gì Đâu :('});
                    }
                    if (finalPriceNum > req.user.money) {
                        return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
                    } else {
                        User.truTien(req.user, finalPriceNum, (err) => {
                            if (err) {
                                errLog.addErrLog(req.user, '/renewservice-getBuffLikeServiceById-getBuffLikeServicePrice-truTien-' + finalPriceNum, err.stack, 'na');
                                return res.json({success: false, msg: 'Lỗi Trừ Tiền, Vui Lòng Thực Hiện Lại'});
                            } else {
                                const descrMoneyLog = req.user.username + ' Thanh Toán ' + finalPriceNum + ' Mua Thêm DV Like';
                                moneyLog.addMoneyLog(req.user.username, req.user.username, -finalPriceNum, descrMoneyLog);
                                let newExpTime = (period + remainingPeriod)*(30*1000 * 60 * 60 * 24) + (new Date().getTime());

                                buffLikeService.renewalRecord(req.body.id, req.body.buyMoreLike + respondData.likeNum, req.body.buyMorePost+respondData.maxMaxPerDay, newExpTime, (err, data) => {
                                    if (err) {
                                        errLog.addErrLog(req.user, '/renewservice-getBuffLikeServiceById-getBuffLikeServicePrice-truTien-renewalRecord-' + finalPriceNum, err.stack, 'na');
                                        return res.json({
                                            success: false,
                                            msg: 'Mua Thêm Dịch Vụ Buff Like Không Thành Công'
                                        });
                                    } else {
                                        const dayNum = (period + remainingPeriod)*30;
                                        reqToBotSv.buyBuffLike(data._id, data.profileId, data.reactionRate, dayNum, data.likeNum, data.rushHour, data.maxPerDay, data.delay);
                                        return res.json({success: true, msg: 'Mua Thêm Dịch Vụ Buff Like Thành Công'});
                                    }
                                });
                            }
                        });
                    }
                }
            });
        } else {
            return res.json({success: false, msg: 'Mua Thêm Dịch Vụ buff like Không Thành Công'});
        }
    });
});

router.post('/bufflike', AUTH, (req, res, next) => {
    const profileId = this.getUidFromProfileLink(req.body.profileLink);
    if (profileId.length < 1) {
        return res.json({success: false, msg: 'Profile Link Không Chính Xác'});
    }
    buffLikeService.getBuffLikeServiceByProfiledId(profileId, (err, profileIdRes) => {
        if (err) {
            errLog.addErrLog(req.user, '/bufflike-getBuffLikeServiceByProfiledId', err.stack, 'na');
            return res.json({success: false, msg: 'Đăng Ký Dịch Vụ buff like Không Thành Công'});
        }
        console.log(profileIdRes);
        if(profileIdRes){
            return res.json({success: false, msg: 'ProfileLink Đang Được Chạy Trên Hệ Thống, Vui Lòng Cập Nhật Bản Ghi Cũ'});
        }
        buffLikeServicePrice.getBuffLikeServicePrice((err, price) => {
            if (err) {
                errLog.addErrLog(req.user, '/bufflike-getBuffLikeServicePrice', err.stack, 'na');
                return res.json({success: false, msg: 'Đăng Ký Dịch Vụ buff like Không Thành Công'});
            }
            if (price) {
                let requiredPrice = req.body.likeNum * price.basePrice;
                let newExpiredTime = 30 * 24 * 60 * 60 * 1000;
                let discount = 0;

                if (req.body.likeNum >= 1200) {
                    discount = price.oneThousandTwoHundredLikeDiscount;
                } else if (req.body.likeNum >= 600) {
                    discount = price.sixHundredLikeDiscount;
                } else if (req.body.likeNum >= 300) {
                    discount = price.threeHundredLikeDiscount;
                } else if (req.body.likeNum >= 200) {
                    discount = price.twoHundredLikeDiscount;
                } else if (req.body.likeNum >= 100) {
                    discount = price.oneHundredLikeDiscount;
                } else if (req.body.likeNum >= 50) {
                    discount = price.fiftyLikeDiscount;
                }

                if (req.body.period === 'oneMonth') {
                    discount = discount + price.oneMonthDiscount;
                    newExpiredTime = newExpiredTime * 1;
                    requiredPrice = 1 * requiredPrice - 1 * requiredPrice * discount;
                } else if (req.body.period === 'threeMonth') {
                    discount = discount + price.threeMonthDiscount;
                    newExpiredTime = newExpiredTime * 3;
                    requiredPrice = 3 * requiredPrice - 3 * requiredPrice * discount;
                } else if (req.body.period === 'sixMonth') {
                    discount = discount + price.sixMonthDiscount;
                    newExpiredTime = newExpiredTime * 6;
                    requiredPrice = 6 * requiredPrice - 6 * requiredPrice * discount;
                } else {
                    return res.json({success: false, msg: 'Đăng Ký Dịch Vụ Buff Like Không Thành Công'});
                }
                let maxMaxPerDay = 10;
                if (req.body.maxPerDay > maxMaxPerDay) {
                    maxMaxPerDay = req.body.maxPerDay;
                    requiredPrice = requiredPrice * (req.body.maxPerDay / 10);
                }
                // if (profileIdRes) {
                //     if (req.body.maxPerDay > profileIdRes.maxMaxPerDay) {
                //         maxMaxPerDay = req.body.maxPerDay;
                //         requiredPrice = requiredPrice * (1 + (req.body.maxPerDay / 10 - profileIdRes.maxMaxPerDay / 10));
                //     }
                // }

                if (requiredPrice > req.user.money) {
                    return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
                } else {
                    User.truTien(req.user, requiredPrice, (err) => {
                        if (err) {
                            errLog.addErrLog(req.user, '/bufflike-getBuffLikeServicePrice-truTien-' + requiredPrice, err.stack, 'na');
                            return res.json({success: false, msg: 'Lỗi Trừ Tiền, Vui Lòng Thực Hiện Lại'});
                        } else {
                            const descrMoneyLog = req.user.username + ' Thanh Toán ' + requiredPrice + ' Buff Like';
                            moneyLog.addMoneyLog(req.user.username, req.user.username, -requiredPrice, descrMoneyLog);
                            let newData = {
                                responsibler: req.user.username,
                                profileId: profileId,
                                likeNum: req.body.likeNum,
                                maxLikeNum: req.body.likeNum,
                                maxPerDay: req.body.maxPerDay,
                                maxMaxPerDay: maxMaxPerDay,
                                rushHour: req.body.rushHour,
                                reactionRate: req.body.reactionRate,
                                delay: req.body.delay,
                                expiredTime: newExpiredTime
                            };
                            buffLikeService.addBuffLikeService(newData, (err, data) => {
                                if (err) {
                                    errLog.addErrLog(req.user, '/bufflike-getBuffLikeServicePrice-truTien-addBuffLikeService-' + requiredPrice, err.stack, 'na');
                                    return res.json({
                                        success: false,
                                        msg: 'Đăng Ký Dịch Vụ Buff Like Không Thành Công'
                                    });
                                } else {
                                    const dayNum = (newExpiredTime) / (1000 * 60 * 60 * 24);
                                    reqToBotSv.buyBuffLike(data._id, profileId, req.body.reactionRate, dayNum, req.body.likeNum, req.body.rushHour, req.body.maxPerDay, req.body.delay);
                                    return res.json({success: true, msg: 'Đăng Ký Dịch Vụ Buff Like Thành Công'});
                                }
                            });
                        }
                    });
                }
            }
        });
    });
});

router.post('/update', AUTH, (req, res, next) => {
    buffLikeService.getBuffLikeServiceById(req.body.id, (err, buffLikeServBundle) => {
        let newLikeNum = req.body.likeNum;
        if(req.body.likeNum > buffLikeServBundle.maxLikeNum){
            newLikeNum = buffLikeServBundle.maxLikeNum;
        }
        if (err) {
            errLog.addErrLog(req.user, '/update-getBuffLikeServiceById', err.stack, req.body.id);
        }
        if (buffLikeServBundle) {

            buffLikeService.updateProfileLinkAndReactionRate(buffLikeServBundle._id, buffLikeServBundle.profileId, req.body.reactionRate, req.body.maxPerDay, buffLikeServBundle.maxMaxPerDay, req.body.delay, req.body.rushHour, newLikeNum, (err) => {

                if (err) {
                    errLog.addErrLog(req.user, '/update-getBuffLikeServiceById-updateProfileLinkAndReactionRate', err.stack, req.body.id);
                    return res.json({success: false, msg: 'Update BuffLike Service Failed'});
                } else {
                    reqToBotSv.updateBuffLike(req.body.id, buffLikeServBundle.profileId, req.body.reactionRate, req.body.rushHour, req.body.maxPerDay, newLikeNum, req.body.delay);
                    return res.json({success: true, msg: 'Updated BuffLike Service'});
                }
            });
        } else {
            return res.json({success: false, msg: 'Update BuffLike Failed'});
        }
    });
});

router.post('/pause', AUTH, (req, res, next) => {
    buffLikeService.pauseBuffLikeService(req.body.id, (err) => {
        if (err) {
            errLog.addErrLog(req.user, '/pause-pauseBuffLikeService', err.stack, req.body.id);
            return res.json({success: false, msg: 'Pause BuffLike Service Failed'});
        } else {
            reqToBotSv.pauseBuffLike(req.body.id);
            return res.json({success: true, msg: 'Paused BuffLike Service'});
        }
    });
});

router.post('/unpause', AUTH, (req, res, next) => {
    buffLikeService.getBuffLikeServiceById(req.body.id, (err, buffLikeServBundle) => {
        if (err) {
            errLog.addErrLog(req.user, '/unpause-getBuffLikeServiceById', err.stack, req.body.id);
        }
        if (buffLikeServBundle) {
            const timeNow = new Date().getTime();
            let newExpTime = buffLikeServBundle.expiredTime + timeNow - buffLikeServBundle.lastPauseTime;
            buffLikeService.unPauseBuffLikeService(req.body.id, newExpTime, (err) => {
                if (err) {
                    errLog.addErrLog(req.user, '/unpause-getBuffLikeServiceById-unPauseBuffLikeService', err.stack, req.body.id);
                    return res.json({success: false, msg: 'unPause BuffLike Service Failed'});
                } else {
                    const dayNum = (newExpTime - new Date().getTime()) / (1000 * 60 * 60 * 24);
                    reqToBotSv.unPauseBuffLike(req.body.id, dayNum);
                    return res.json({success: true, msg: 'unPaused BuffLike Service'});
                }
            });
        } else {
            return res.json({success: false, msg: 'unPause BuffLike Service Failed'});
        }
    });
});

router.get('/getprice', AUTH, (req, res) => {
    buffLikeServicePrice.getBuffLikeServicePrice((err, price) => {
        if (err) {
            errLog.addErrLog(req.user, '/getprice-getBuffLikeServicePrice', err.stack, 'na');
        }
        if (price) {
            return res.json({success: true, price});
        } else {
            return res.json({success: false});
        }
    })
});

router.get('/getlist', AUTH, (req, res) => {
    buffLikeService.getBuffLikeServiceByResponsibler(req.user.username, (err, bundle) => {
        if (err) {
            errLog.addErrLog(req.user, '/getlist-getBuffLikeServiceByResponsibler', err.stack, 'na');
        }
        if (bundle) {
            for (let subBundle of bundle) {
                let remainingDays = (subBundle.expiredTime - (new Date().getTime())) / (1000 * 60 * 60 * 24);
                if(remainingDays < 0){
                    remainingDays = 0;
                }
                subBundle.expiredTime = parseFloat(remainingDays).toFixed(2);
            }
            return res.json({success: true, bundle});
        } else {
            return res.json({success: false});
        }
    })
});

module.exports = router;