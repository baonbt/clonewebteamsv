const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const passport = require('passport');
var AUTH = passport.authenticate('jwt', {session: false});
const roles = require('../../guards/levels');
const moneyLog = require('../../models/moneyLog');
const request = require('request');
const errLog = require('../../models/errLog');
const commentPrice = require('../../models/commentPrice');
const commentBundle = require('../../models/commentBundle');
const commentCustomer = require('../../models/commentCustomer');
const reqToBotSv = require('../botSv');
const URI = require('urijs');


module.exports.getUidFromProfileLink = function (profileLink) {

    if (profileLink.indexOf("profile.php") === -1) {
        const segments = URI.parse(profileLink).path.split('/');

        return segments[1];
    } else {
        const params = URI(profileLink).query(true).id;

        return params;
    }
};
// MUA DV CMT
router.post('/buynew', AUTH, (req, res, next) => {
    const profileId = this.getUidFromProfileLink(req.body.profileLink);
    if (profileId === undefined || profileId === '') {
        return res.json({success: false, msg: 'ProfileLink Không Chính Xác'});
    }
    commentCustomer.getCommentCustomerByProfileId(profileId, (err, profileRes) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/buynew-getCommentCustomerByProfileId', err.stack, 'na');
            return res.json({success: false, msg: 'Đăng Ký Dịch Vụ Tự Động TƯơng Tác Không Thành Công'});
        }
        if (profileRes.length > 0) {
            return res.json({success: false, msg: 'Profile Đã Chạy, Vui Lòng Bổ Sung Hoặc Cập Nhật.'});
        } else {
            commentPrice.getCommentServicePrice((err, price) => {
                if (err) {
                    errLog.addErrLog(req.user, 'cmt/buynew-getCommentServicePrice', err.stack, 'na');
                    return res.json({success: false, msg: 'Đăng Ký Dịch Vụ Tự Động TƯơng Tác Không Thành Công'});
                } else {
                    let requiredPrice = price.basePrice;
                    let newExpiredTime = 30 * 24 * 60 * 60 * 1000;
                    let discount = 0;
                    if (req.body.period === 'oneMonth') {
                        discount = price.oneMonthDiscount;
                    } else if (req.body.period === 'threeMonth') {
                        newExpiredTime = 3 * 30 * 24 * 60 * 60 * 1000;
                        requiredPrice = requiredPrice * 3;
                        discount = price.oneMonthDiscount;

                    } else if (req.body.period === 'sixMonth') {
                        newExpiredTime = 6 * 30 * 24 * 60 * 60 * 1000;
                        requiredPrice = requiredPrice * 6;
                        discount = price.oneMonthDiscount;
                    } else {
                        return res.json({success: false, msg: 'Đăng Ký Dịch Vụ Tự Động Thả Tim Không Thành Công'});
                    }
                    if (req.body.commentNum > 0) {
                        requiredPrice = requiredPrice * req.body.commentNum;
                        if (req.body.commentNum >= 30) {
                            discount = discount + price.thirtyCmtDiscount;
                        } else if (req.body.commentNum >= 20) {
                            discount = discount + price.twentyCmtDiscount;
                        } else if (req.body.commentNum >= 10) {
                            discount = discount + price.tenCmtDiscount;
                        }
                    }
                    if (req.body.maxPerDay > 10) {
                        requiredPrice = requiredPrice * (req.body.maxPerDay / 10);
                    }
                    requiredPrice = requiredPrice - discount * requiredPrice;
                    newExpiredTime = newExpiredTime + new Date().getTime();
                    if (requiredPrice > req.user.money) {
                        return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
                    } else {
                        User.truTien(req.user, requiredPrice, (err) => {
                            if (err) {
                                errLog.addErrLog(req.user, 'cmt/buynew-getCommentServicePrice-truTien-' + requiredPrice, err.stack, 'na');
                                return res.json({success: false, msg: 'Lỗi Trừ Tiền, Vui Lòng Thực Hiện Lại'});
                            } else {
                                const descrMoneyLog = req.user.username + ' Thanh Toán ' + requiredPrice + ' Mua Bình Luận';
                                moneyLog.addMoneyLog(req.user.username, req.user.username, -requiredPrice, descrMoneyLog);
                                commentCustomer.addCommentCustomer(req.user.username, profileId, newExpiredTime, req.body.commentNum, req.body.commentBundles, req.body.maxPerDay, (err, addRes) => {
                                    if (err) {
                                        errLog.addErrLog(req.user, 'cmt/buynew-getCommentServicePrice-truTien-addCommentCustomer-' + requiredPrice, err.stack, 'na');
                                        return res.json({
                                            success: false,
                                            msg: 'Mua Dịch Vụ Không Thành Công Vui Lòng Báo Với Quản Lý'
                                        });
                                    }
                                    if (addRes) {

                                        const dayNum = (newExpiredTime - new Date().getTime()) / (24 * 60 * 60 * 1000);
                                        sendCommentBundleToBotSv(addRes._id, profileId, dayNum, req.body.commentNum, req.body.maxPerDay, req.body.commentBundles);
                                        return res.json({success: true, msg: 'Mua Bình Luận Thành Công'});
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    });

});

function sendCommentBundleToBotSv(id, profileId, dayNum, commentNum, maxPerDay, commentBundles) {
    let cmtBundleIds = [];
    for (let id of commentBundles) {
        cmtBundleIds.push(id);
    }
    commentBundle.getListCommentBundleById(cmtBundleIds, (err, respondData) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/buynew-getCommentServicePrice-truTien-addCommentCustomer-getListCommentBundleById-' + requiredPrice, err.stack, 'na');
        }
        if (respondData) {
            let cmtBundles = [];
            for (let x of respondData) {
                let cmts = {
                    id: x._id,
                    fbUid: profileId,
                    replyRate: x.replyRate,
                    rushHour: x.rushHour,
                    hashtag: x.hashTag,
                    duration: x.delay,
                    comment: x.commentContents
                };
                cmtBundles.push(cmts);
            }
            reqToBotSv.commentUpdate(id, profileId, dayNum, commentNum, maxPerDay, cmtBundles);
        }
    });
}

router.post('/muathempost', AUTH, (req, res, next) => {
    commentCustomer.getCommentCustomerById(req.body.id, (err, resData) => {
        if (err) {
            errLog.addErrLog(req.user, '/muathempost-getCommentCustomerById', err.stack, 'na');
            return res.json({success: false, msg: 'Mua Thêm Không Thành Công'});
        }
        if (resData) {
            commentPrice.getCommentServicePrice((err, price) => {
                if (err) {
                    errLog.addErrLog(req.user, '/muathempost-getCommentCustomerById-getCommentServicePrice', err.stack, 'na');
                    return res.json({success: false, msg: 'Mua Thêm Không Thành Công'});
                }
                let period = (resData.expiredTime - (new Date().getTime())) / (30 * 24 * 60 * 60 * 1000);
                let durationDiscountNum = 0;
                if (period >= 6) {
                    durationDiscountNum = price.sixMonthDiscount;
                } else if (this.period >= 3) {
                    durationDiscountNum = price.threeMonthDiscount;
                } else if (this.period >= 1) {
                    durationDiscountNum = price.oneMonthDiscount;
                }
                let commentDiscount = 0;
                if (resData.maxCommentNum + req.body.commentNum >= 20) {
                    commentDiscount = price.thirtyCmtDiscount;
                } else if (resData.maxCommentNum + req.body.commentNum >= 10) {
                    commentDiscount = price.twentyCmtDiscount;
                } else if (resData.maxCommentNum + req.body.commentNum >= 1) {
                    commentDiscount = price.tenCmtDiscount;
                }
                const totalPriceNum = period * resData.maxCommentNum * price.basePrice * (req.body.maxPerDay / 10);
                const totalDiscountNum = commentDiscount + durationDiscountNum;
                const finalPriceNum = totalPriceNum - totalDiscountNum * totalPriceNum;
                if (finalPriceNum > req.user.money) {
                    return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
                } else {
                    User.truTien(req.user, finalPriceNum, (err) => {
                        if (err) {
                            errLog.addErrLog(req.user, '/muathempost-getCommentCustomerById-getCommentServicePrice-truTien-' + finalPriceNum, err.stack, 'na');
                            return res.json({success: false, msg: 'Mua Thêm Không Thành Công'});
                        }
                        const descrMoneyLog = req.user.username + ' Thanh Toán ' + finalPriceNum + ' Mua Thêm Post';
                        moneyLog.addMoneyLog(req.user.username, req.user.username, -finalPriceNum, descrMoneyLog);
                        const newMaxMaxPerDay = req.body.maxPerDay + resData.maxMaxPerDay;
                        commentCustomer.muaThemPost(req.body.id, newMaxMaxPerDay, (err, fRes) => {
                            if (err) {
                                errLog.addErrLog(req.user, '/muathempost-getCommentCustomerById-getCommentServicePrice-truTien-muaThemComment-' + finalPriceNum, err.stack, 'na');
                                return res.json({
                                    success: false,
                                    msg: 'Mua Thêm Không Thành Công, Vui Lòng Liên Hệ Quản Lý'
                                });
                            }
                            let dayNum = (fRes.expiredTime - (new Date().getTime())) / (24 * 60 * 60 * 1000);
                            if (dayNum < 0) {
                                dayNum = 0;
                            }
                            sendCommentBundleToBotSv(fRes._id, fRes.profileId, dayNum, fRes.commentNum, fRes.maxPerDay, fRes.commentBundles);
                            return res.json({success: true, msg: 'Mua Thêm Thành Công'});

                        });
                    })
                }
            });
        } else {
            return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Không Thành Công'});
        }
    });
});

router.post('/muathemcomment', AUTH, (req, res, next) => {
    commentCustomer.getCommentCustomerById(req.body.id, (err, resData) => {
        if (err) {
            errLog.addErrLog(req.user, '/muathemcomment-getCommentCustomerById', err.stack, 'na');
            return res.json({success: false, msg: 'Mua Thêm Không Thành Công'});
        }
        if (resData) {
            commentPrice.getCommentServicePrice((err, price) => {
                if (err) {
                    errLog.addErrLog(req.user, '/muathemcomment-getCommentCustomerById-getCommentServicePrice', err.stack, 'na');
                    return res.json({success: false, msg: 'Mua Thêm Dịch Vụ Không Thành Công'});
                }
                let period = (resData.expiredTime - (new Date().getTime())) / (30 * 24 * 60 * 60 * 1000);
                let durationDiscountNum = 0;
                if (period >= 6) {
                    durationDiscountNum = price.sixMonthDiscount;
                } else if (this.period >= 3) {
                    durationDiscountNum = price.threeMonthDiscount;
                } else if (this.period >= 1) {
                    durationDiscountNum = price.oneMonthDiscount;
                }
                let commentDiscount = 0;
                if (resData.maxCommentNum + req.body.commentNum >= 20) {
                    commentDiscount = price.thirtyCmtDiscount;
                } else if (resData.maxCommentNum + req.body.commentNum >= 10) {
                    commentDiscount = price.twentyCmtDiscount;
                } else if (resData.maxCommentNum + req.body.commentNum >= 1) {
                    commentDiscount = price.tenCmtDiscount;
                }
                const totalPriceNum = period * req.body.commentNum * price.basePrice * (resData.maxMaxPerDay / 10);
                const totalDiscountNum = commentDiscount + durationDiscountNum;
                const finalPriceNum = totalPriceNum - totalDiscountNum * totalPriceNum;
                if (finalPriceNum > req.user.money) {
                    return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
                } else {
                    User.truTien(req.user, finalPriceNum, (err) => {
                        if (err) {
                            errLog.addErrLog(req.user, '/muathemcomment-getCommentCustomerById-getCommentServicePrice-truTien-' + finalPriceNum, err.stack, 'na');
                            return res.json({success: false, msg: 'Mua Thêm Không Thành Công'});
                        }
                        const descrMoneyLog = req.user.username + ' Thanh Toán ' + finalPriceNum + ' Mua Thêm Bình Luận';
                        moneyLog.addMoneyLog(req.user.username, req.user.username, -finalPriceNum, descrMoneyLog);
                        const newMaxCommentNum = req.body.commentNum + resData.maxCommentNum;
                        commentCustomer.muaThemComment(req.body.id, newMaxCommentNum, (err, fRes) => {
                            if (err) {
                                errLog.addErrLog(req.user, '/giahan-getCommentCustomerById-getCommentServicePrice-truTien-muaThemComment-' + finalPriceNum, err.stack, 'na');
                                return res.json({
                                    success: false,
                                    msg: 'Mua Thêm Không Thành Công, Vui Lòng Liên Hệ Quản Lý'
                                });
                            }
                            let dayNum = (fRes.expiredTime - (new Date().getTime())) / (24 * 60 * 60 * 1000);
                            if (dayNum < 0) {
                                dayNum = 0;
                            }
                            sendCommentBundleToBotSv(fRes._id, fRes.profileId, dayNum, fRes.commentNum, fRes.maxPerDay, fRes.commentBundles);
                            return res.json({success: true, msg: 'Mua Thêm Thành Công'});

                        });
                    })
                }
            });
        } else {
            return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Không Thành Công'});
        }
    });
});

router.post('/giahan', AUTH, (req, res, next) => {
    commentCustomer.getCommentCustomerById(req.body.id, (err, resData) => {
        if (err) {
            errLog.addErrLog(req.user, '/giahan-getCommentCustomerById', err.stack, 'na');
            return res.json({success: false, msg: 'Gia Hạn Dịch Vụ Không Thành Công'});
        }
        if (resData) {
            commentPrice.getCommentServicePrice((err, price) => {
                if (err) {
                    errLog.addErrLog(req.user, '/giahan-getCommentCustomerById-getCommentServicePrice', err.stack, 'na');
                    return res.json({success: false, msg: 'Gia Hạn Dịch Vụ Không Thành Công'});
                }
                let period = 0;
                let durationDiscountNum = 0;
                if (req.body.duration === 'oneMonth') {
                    period = 1;
                    durationDiscountNum = price.oneMonthDiscount;
                } else if (req.body.duration === 'threeMonth') {
                    period = 3;
                    durationDiscountNum = price.threeMonthDiscount;
                } else if (req.body.duration === 'sixMonth') {
                    period = 6;
                    durationDiscountNum = price.sixMonthDiscount;
                }
                const totalPriceNum = period * resData.maxCommentNum * price.basePrice * (resData.maxMaxPerDay / 10);
                const totalDiscountNum = durationDiscountNum;
                const finalPriceNum = totalPriceNum - totalDiscountNum * totalPriceNum;
                if (finalPriceNum > req.user.money) {
                    return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
                } else {
                    User.truTien(req.user, finalPriceNum, (err) => {
                        if (err) {
                            errLog.addErrLog(req.user, '/giahan-getCommentCustomerById-getCommentServicePrice-truTien-' + finalPriceNum, err.stack, 'na');
                            return res.json({success: false, msg: 'Gia Hạn Dịch Vụ Không Thành Công'});
                        }
                        const descrMoneyLog = req.user.username + ' Thanh Toán ' + finalPriceNum + ' Gia Hạn Bình Luận';
                        moneyLog.addMoneyLog(req.user.username, req.user.username, -finalPriceNum, descrMoneyLog);
                        let remainingDays = resData.expiredTime - (new Date().getTime());
                        if (remainingDays < 0) {
                            remainingDays = 0;
                        }
                        const newExpiredTime = (new Date().getTime()) + remainingDays + period * (30 * 24 * 60 * 60 * 1000);
                        commentCustomer.giaHan(req.body.id, newExpiredTime, (err, fRes) => {
                            if (err) {
                                errLog.addErrLog(req.user, '/giahan-getCommentCustomerById-getCommentServicePrice-truTien-giaHan-' + finalPriceNum, err.stack, 'na');
                                return res.json({
                                    success: false,
                                    msg: 'Gia Hạn Dịch Vụ Không Thành Công, Vui Lòng Liên Hệ Quản Lý'
                                });
                            }
                            let dayNum = (fRes.expiredTime - (new Date().getTime())) / (24 * 60 * 60 * 1000);
                            if (dayNum < 0) {
                                dayNum = 0;
                            }
                            sendCommentBundleToBotSv(fRes._id, fRes.profileId, dayNum, fRes.commentNum, fRes.maxPerDay, fRes.commentBundles);
                            return res.json({success: true, msg: 'Gia Hạn Dịch Vụ Thành Công'});

                        });
                    })
                }
            });
        } else {
            return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Không Thành Công'});
        }
    });
});

// CẬP NHẬT PROFILE LINK
router.post('/updateprofilelink', AUTH, (req, res, next) => {
    const profileId = this.getUidFromProfileLink(req.body.profileLink);
    commentCustomer.getCommentCustomerById(req.body.id, (err, dataRes) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/updateprofilelink-getCommentCustomerById', err.stack, 'na');
            return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Không Thành Công'});
        }
        if (dataRes) {
            if (req.user.username === dataRes.username) {
                let newCommentNum = req.body.commentNum;
                if (newCommentNum > dataRes.maxCommentNum) {
                    newCommentNum = 0;
                }
                commentCustomer.updateProfileId(req.body.id, profileId, req.body.commentBundles, newCommentNum, (err, fRes) => {
                    if (err) {
                        errLog.addErrLog(req.user, 'cmt/buynew-getCommentCustomerById', err.stack, 'na');
                        return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Không Thành Công'});
                    }

                    let dayNum = (fRes.expiredTime - (new Date().getTime())) / (24 * 60 * 60 * 1000);
                    console.log(fRes.expiredTime);
                    console.log(dayNum);
                    if (dayNum < 0) {
                        dayNum = 0;
                    }
                    console.log(dayNum);
                    sendCommentBundleToBotSv(fRes._id, fRes.profileId, dayNum, fRes.commentNum, fRes.maxPerDay, fRes.commentBundles);
                    return res.json({success: true, msg: 'Cập Nhật Dịch Vụ Thành Công'});
                });
            } else {
                return res.json({success: false, msg: 'Bạn Không Có Quyền Cập Nhật'});
            }
        } else {
            return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Không Thành Công, ID Không Tồn Tại'});
        }
    });
});

// CẬP NHẬT PROFILE LINK STATUS
router.post('/updateprofilelinkstatus', (req, res, next) => {
    commentCustomer.updateProfileIdStatus(req.body.id, req.body.profileIdStatus, (err) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/updateprofilelinkstatus-updateProfileIdStatus', err.stack, 'na');
            return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Không Thành Công'});
        }
        return res.json({success: true, msg: 'Cập Nhật Dịch Vụ Thành Công'});
    });
});

// THÊM TỆP BÌNH LUẬN MỚI
router.post('/addnewcommentbundle', AUTH, (req, res, next) => {
    console.log(req.body);
    commentCustomer.getCommentCustomerByUsername(req.user.username, (err, dataRes) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/addnewcommentbundle-getCommentCustomerByUsername', err.stack, 'na');
            return res.json({success: false, msg: 'Thêm Tệp Bình Luận Không Thành Công'});
        }
        if (dataRes) {
            let totalCommentNum = 0;
            for (let data of dataRes) {
                totalCommentNum = totalCommentNum + data.commentNum;
            }
            totalCommentNum = totalCommentNum / 3;
            commentBundle.getCommentBundleByUsername(req.user.username, (err, dataRes_1) => {
                if (err) {
                    errLog.addErrLog(req.user, 'cmt/addnewcommentbundle-getCommentCustomerByUsername-getCommentBundleByUsername', err.stack, 'na');
                    return res.json({success: false, msg: 'Thêm Tệp Bình Luận Không Thành Công'});
                }
                if (dataRes_1.length < totalCommentNum + 9) {
                    commentBundle.addCommentBundle(req.user.username, req.body.bundleName, req.body.hashTag, req.body.replyRate, req.body.commentOrder, req.body.delay, req.body.rushHour, req.body.commentContents, (err, dataRes_2) => {
                        if (err) {
                            errLog.addErrLog(req.user, 'cmt/addnewcommentbundle-getCommentCustomerByUsername-getCommentBundleByUsername-addCommentBundle', err.stack, 'na');
                            return res.json({success: false, msg: 'Thêm Tệp Bình Luận Không Thành Công'});
                        }
                        if (dataRes_2) {
                            return res.json({success: true, msg: 'Thêm Tệp Bình Luận Thành Công'});
                        }
                    })
                } else {
                    return res.json({success: false, msg: 'Vượt Quá Số Tệp Cho Phép'});
                }
            });
        } else {
            return res.json({success: false, msg: 'Thêm Tệp Bình Luận Không Thành Công'});
        }
    });
});

// CẬP NHẬT TỆP BÌNH LUẬN
router.post('/updatecommentbundle', AUTH, (req, res, next) => {
    commentBundle.getCommentBundleById(req.body.id, (err, dataRes) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/updatecommentbundle-getCommentBundleById', err.stack, 'na');
            return res.json({success: false, msg: 'Cập Nhật Tệp Bình Luận Không Thành Công'});
        }
        if (dataRes) {
            if (dataRes.username === req.user.username) {
                commentBundle.updateCommentBundle(req.body.id, req.body.bundleName, req.body.hashTag, req.body.replyRate, req.body.commentOrder, req.body.delay, req.body.rushHour, req.body.commentContents, (err, dataRes_1) => {
                    if (err) {
                        errLog.addErrLog(req.user, 'cmt/updatecommentbundle-getCommentBundleById-updateCommentBundle', err.stack, 'na');
                        return res.json({success: false, msg: 'Cập Nhật Tệp Bình Luận Không Thành Công'});
                    }
                    commentCustomer.getAllCustomerByBundleId(req.body.id, (err, resBundles)=>{
                        if (err) {
                            errLog.addErrLog(req.user, 'cmt/updatecommentbundle-getCommentBundleById-updateCommentBundle-getAllCommentCustomerByBundleId', err.stack, 'na');
                        }
                        if(resBundles){
                            for(let fRes of resBundles){
                                let dayNum = (fRes.expiredTime - (new Date().getTime())) / (24 * 60 * 60 * 1000);
                                if (dayNum < 0) {
                                    dayNum = 0;
                                }
                                sendCommentBundleToBotSv(fRes._id, fRes.profileId, dayNum, fRes.commentNum, fRes.maxPerDay, fRes.commentBundles);
                            }
                        }
                    });
                    return res.json({success: true, msg: 'Cập Nhật Tệp Bình Luận Thành Công'});
                })
            } else {
                return res.json({success: false, msg: 'Bạn Không Có Quyền Thực Hiện Hành Động Này'});
            }
        } else {
            return res.json({success: false, msg: 'Cập Nhật Tệp Bình Luận Không Thành Công'});
        }
    });
});

router.post('/removebundle', AUTH, (req, res, next) => {
    commentBundle.getCommentBundleById(req.body.id, (err, resData) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/getCommentBundleById-' + req.body.id, err.stack, 'na');
            return res.json({success: false, msg: 'Xóa Tệp Bình Luận Không Thành Công'});
        }
        if (resData) {
            if (req.user.username === resData.username) {
                commentCustomer.getAllCustomerByBundleId(req.body.id, (err, respondData) => {
                    if (err) {
                        errLog.addErrLog(req.user, 'cmt/getCommentBundleById-getAllCustomerByBundleId-' + req.body.id, err.stack, 'na');
                        return res.json({success: false, msg: 'Xóa Tệp Bình Luận Không Thành Công'});
                    }
                    if (respondData) {
                        let resProfileId;
                        for (let x of respondData) {
                            if (resProfileId === undefined) {
                                resProfileId = x.profileId;
                            } else {
                                resProfileId = resProfileId + ',' + x.profileId;
                            }
                        }
                        return res.json({
                            success: false,
                            msg: 'Tệp Đang Được Dùng Bởi Profile ' + resProfileId + '. Xóa Không Thành Công!'
                        });
                    } else {
                        commentBundle.removeById(req.body.id, (err) => {
                            if (err) {
                                errLog.addErrLog(req.user, 'cmt/getCommentBundleById-getAllCustomerByBundleId-removeById-' + req.body.id, err.stack, 'na');
                                return res.json({success: false, msg: 'Xóa Tệp Bình Luận Không Thành Công'});
                            }
                            return res.json({success: true, msg: 'Xóa Tệp Bình Luận Thành Công'});
                        });
                    }
                });
            } else {
                return res.json({success: false, msg: 'Xóa Tệp Bình Luận Không Thành Công'});
            }
        }
    });
});

// CẬP NHẬT GIÁ DV
router.post('/updateprice', AUTH, roles.Auth(99), (req, res, next) => {
    let newPrice = {
        serviceName: 'comment',
        basePrice: req.body.basePrice,
        oneMonthDiscount: req.body.oneMonthDiscount,
        threeMonthDiscount: req.body.threeMonthDiscount,
        sixMonthDiscount: req.body.sixMonthDiscount,
        tenCmtDiscount: req.body.tenCmtDiscount,
        twentyCmtDiscount: req.body.twentyCmtDiscount,
        thirtyCmtDiscount: req.body.thirtyCmtDiscount,
    };
    commentPrice.updateCommentServicePrice(newPrice, (err) => {
        if (err) {
            errLog.addErrLog(req.user, 'cập nhật giá comment', err.stack, 'na');
            return res.json({success: false, msg: 'Update Comment Service Price Failed'});
        } else {
            return res.json({success: true, msg: 'Updated Comment Service Price'});
        }
    });
});

// PAUSE PROFILE LINK
router.post('/pauseprofilelink', AUTH, (req, res, next) => {
    commentCustomer.getCommentCustomerById(req.body.id, (err, dataRes) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/pauseprofilelink-getCommentCustomerById', err.stack, 'na');
            return res.json({success: false, msg: 'Tạm Dừng Không Thành Công'});
        }
        if (req.user.username === dataRes.username) {
            commentCustomer.pause(req.body.id, (err) => {
                if (err) {
                    errLog.addErrLog(req.user, 'cmt/pauseprofilelink-getCommentCustomerById-pause', err.stack, 'na');
                    return res.json({success: false, msg: 'Tạm Dừng Không Thành Công'});
                }
                reqToBotSv.pauseComment(dataRes._id, dataRes.profileId);
                return res.json({success: true, msg: 'Tạm Dừng Thành Công'});
            })
        } else {
            return res.json({success: false, msg: 'Bạn Không Có Quyền Thực Hiện Hành Động Này'});
        }
    });
});

// // PAUSE CMT BUNDLE
// router.post('/pausecommentbundle', AUTH, (req, res, next)=>{
//     commentBundle.getCommentBundleById(req.body.id, (err, dataRes)=>{
//         if(err){
//             errLog.addErrLog(req.user, 'cmt/pausecommentbundle-getCommentBundleById', err.stack, 'na');
//             return res.json({success: false, msg: 'Tạm Dừng Không Thành Công'});
//         }
//         if(req.user.username === dataRes.username){
//             commentBundle.pause(req.body.id, (err)=>{
//                 if(err){
//                     errLog.addErrLog(req.user, 'cmt/pausecommentbundle-getCommentBundleById-pause', err.stack, 'na');
//                     return res.json({success: false, msg: 'Tạm Dừng Không Thành Công'});
//                 }
//                 return res.json({success: true, msg: 'Tạm Dừng Thành Công'});
//             })
//         } else {
//             return res.json({success: false, msg: 'Bạn Không Có Quyền Thực Hiện Hành Động Này'});
//         }
//     });
// });
// UNPAUSE PROFILE LINK
router.post('/unpauseprofilelink', AUTH, (req, res, next) => {
    commentCustomer.getCommentCustomerById(req.body.id, (err, dataRes) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/unpauseprofilelink-getCommentCustomerById', err.stack, 'na');
            return res.json({success: false, msg: 'Chạy Lại Không Thành Công'});
        }
        if (req.user.username === dataRes.username) {
            let newExpiredTime = 0;
            if (dataRes.lastPauseTime > 0) {
                newExpiredTime = new Date().getTime() - dataRes.lastPauseTime + dataRes.expiredTime;
            }
            commentCustomer.unPause(req.body.id, newExpiredTime, (err) => {
                if (err) {
                    errLog.addErrLog(req.user, 'cmt/unpauseprofilelink-getCommentCustomerById-unpause', err.stack, 'na');
                    return res.json({success: false, msg: 'Chạy Lại Không Thành Công'});
                }
                reqToBotSv.unPauseComment(dataRes._id, (newExpiredTime - new Date().getTime()) / (1000 * 60 * 60 * 24), dataRes.profileId);
                return res.json({success: true, msg: 'Chạy Lại Thành Công'});
            })
        } else {
            return res.json({success: false, msg: 'Bạn Không Có Quyền Thực Hiện Hành Động Này'});
        }
    });
});

// UNPAUSE CMT BUNDLE
// router.post('/unpausecommentbundle', AUTH, (req, res, next)=>{
//     commentBundle.getCommentBundleById(req.body.id, (err, dataRes)=>{
//         if(err){
//             errLog.addErrLog(req.user, 'cmt/unpausecommentbundle-getCommentBundleById', err.stack, 'na');
//             return res.json({success: false, msg: 'Chạy Lại Không Thành Công'});
//         }
//         if(req.user.username === dataRes.username){
//             commentBundle.unPause(req.body.id, (err)=>{
//                 if(err){
//                     errLog.addErrLog(req.user, 'cmt/unpausecommentbundle-getCommentBundleById-unpause', err.stack, 'na');
//                     return res.json({success: false, msg: 'Chạy Lại Không Thành Công'});
//                 }
//                 return res.json({success: true, msg: 'Chạy Lại Thành Công'});
//             })
//         } else {
//             return res.json({success: false, msg: 'Bạn Không Có Quyền Thực Hiện Hành Động Này'});
//         }
//     });
// });

//get price
router.get('/getprice', AUTH, (req, res, next) => {
    commentPrice.getCommentServicePrice((err, price) => {
        if (err) {
            errLog.addErrLog(req.user, 'cmt/getprice-getCommentServicePrice', err.stack, 'na');
        }
        if (price) {
            return res.json({success: true, price});
        } else {
            return res.json({success: false});
        }
    })
});

// GET PROFILE LINK LIST
router.get('/getprofilelinklist', AUTH, (req, res) => {
    commentCustomer.getCommentCustomerByUsername(req.user.username, (err, bundle) => {
        if (err) {
            errLog.addErrLog(req.user, '/getprofilelinklist-getCommentCustomerByUsername', err.stack, 'na');
        }
        if (bundle) {
            for (let subBundle of bundle) {
                const remainingDays = (subBundle.expiredTime - (new Date().getTime())) / (1000 * 60 * 60 * 24);
                subBundle.expiredTime = parseFloat(remainingDays).toFixed(2);
                // let cmtBundleIds = [];
                // for (let id of subBundle.commentBundles) {
                //     cmtBundleIds.push(id);
                // }
                // commentBundle.getListCommentBundleById(cmtBundleIds, (err, respondData) => {
                //     if (err) {
                //         errLog.addErrLog(req.user, 'cmt/buynew-getCommentServicePrice-truTien-addCommentCustomer-getListCommentBundleById-' + requiredPrice, err.stack, 'na');
                //     }
                //     if (respondData) {
                //         for (let x of respondData) {
                //             if(subBundle._bundleName === undefined){
                //                 subBundle._bundleName = x.bundleName;
                //             } else {
                //                 subBundle._bundleName = subBundle._bundleName + ', ' + x.bundleName;
                //             }
                //
                //             console.log(subBundle._bundleName);
                //             console.log(bundle);
                //         }
                //     }
                //     return res.json({success: true, bundle});
                // });
            }
            return res.json({success: true, bundle});
        } else {
            return res.json({success: false});
        }
    })
});

// GET COMMENT BUNDLE LIST
router.get('/getcommentbundlelist', AUTH, (req, res) => {
    commentBundle.getCommentBundleByUsername(req.user.username, (err, bundle) => {
        if (err) {
            errLog.addErrLog(req.user, '/getcommentbundlelist-getCommentBundleByUsername', err.stack, 'na');
        }
        if (bundle) {
            return res.json({success: true, bundle});
        } else {
            return res.json({success: false});
        }
    })
});
module.exports = router;