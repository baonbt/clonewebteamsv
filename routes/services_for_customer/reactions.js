const express = require('express');
const router = express.Router();
const User = require('../../models/user');
const passport = require('passport');
var AUTH = passport.authenticate('jwt', {session:false});
const roles = require('../../guards/levels');
const reactionServicePrice = require('../../models/reactionServicePrice');
const reactionService = require('../../models/reactionService');
const moneyLog = require('../../models/moneyLog');
const request = require('request');
const errLog = require('../../models/errLog');
const reqToBotSv = require('../botSv');
const URI = require('urijs');


module.exports.getUidFromProfileLink = function (profileLink) {

    if (profileLink.indexOf("profile.php") === -1) {
        const segments = URI.parse(profileLink).path.split('/');

        return segments[1];
    } else {
        const params = URI(profileLink).query(true).id;

        return params;
    }
};

router.post('/botsvupdate5b6bd271c2944e07c078e8cc', (req, res, next)=>{
    console.log(req.body);
    reactionService.updateCookieState(req.body.id, req.body.cookieState, (err)=>{
        if(err){
            errLog.addErrLog(req.body, 'cập nhật giá cookieState', err.stack, 'na');
            return res.json({success: false});
        }
        return res.json({success: true});
    })
});
//register
router.post('/updateprice', AUTH, roles.Auth(99), (req, res, next) => {
    let newPrice = {
        basePrice: req.body.basePrice,
        oneMonthDiscount: req.body.oneMonthDiscount,
        threeMonthDiscount: req.body.threeMonthDiscount,
        sixMonthDiscount: req.body.sixMonthDiscount
    };
    reactionServicePrice.updateReactionServicePrice(newPrice, (err) => {
        if(err){
            errLog.addErrLog(req.user, 'cập nhật giá reactions', err.stack, 'na');
            return res.json({success: false, msg: 'Update Reaction Service Price Failed'});
        } else {
            return res.json({success: true, msg: 'Updated Reaction Service Price'});
        }
    })
});

router.post('/reactions', AUTH, function (req, res, next) {
    reactionServicePrice.getReactionServicePrice((err, price)=>{
        if(err){
            errLog.addErrLog(req.user, '/reactions-getReactionServicePrice', err.stack, 'na');
            return res.json({success: false, msg: 'Đăng Ký Dịch Vụ Tự Động TƯơng Tác Không Thành Công'});
        }
        let requiredPrice = price.basePrice;
        let newExpiredTime = 30*24*60*60*1000;
        if (req.body.period === 'oneMonth'){
            requiredPrice = requiredPrice*1;
        } else if (req.body.period === 'threeMonth') {
            newExpiredTime = 3*30*24*60*60*1000;
            requiredPrice = requiredPrice*3 - 3*requiredPrice*price.threeMonthDiscount;
        } else if (req.body.period === 'sixMonth') {
            newExpiredTime = 6*30*24*60*60*1000;
            requiredPrice = requiredPrice*6 - 6*requiredPrice*price.sixMonthDiscount;
        } else {
            return res.json({success: false, msg: 'Đăng Ký Dịch Vụ Tự Động Thả Tim Không Thành Công'});
        }
        if ( requiredPrice > req.user.money) {
            return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
        } else {
            User.truTien(req.user, requiredPrice , (err) =>{
                if(err){
                    errLog.addErrLog(req.user, '/reactions-getReactionServicePrice-truTien-'+requiredPrice, err.stack, 'na');
                    return res.json({success: false, msg: 'Lỗi Trừ Tiền, Vui Lòng Thực Hiện Lại'});
                }else {
                    const descrMoneyLog = req.user.username + ' Thanh Toán ' + requiredPrice + ' Auto Thả Tym';
                    moneyLog.addMoneyLog(req.user.username, req.user.username, -requiredPrice, descrMoneyLog);
                    reactionService.getReactionServiceByFbuid(req.body.fbUid, (err, fbUid)=>{
                        if(err){
                            errLog.addErrLog(req.user, '/reactions-getReactionServicePrice-truTien-getReactionServiceByFbUid-'+requiredPrice, err.stack, 'na');
                        }
                        if(fbUid){
                            let newData = {};
                            if (fbUid.cookieState === 'live'){
                                newData = {
                                    fbUid: req.body.fbUid,
                                    reactionRate: req.body.reactionRate,
                                    likePagePost: req.body.likePagePost,
                                    expiredTime: fbUid.expiredTime + newExpiredTime
                                };
                            } else {
                                newData = {
                                    fbUid: req.body.fbUid,
                                    cookie: req.body.cookie,
                                    cookieState: 'checking',
                                    reactionRate: req.body.reactionRate,
                                    likePagePost: req.body.likePagePost,
                                    expiredTime: fbUid.expiredTime + newExpiredTime
                                };
                            }
                            reactionService.updateReactionService(newData, (err, _resData)=>{
                                if(err){
                                    errLog.addErrLog(req.user, '/reactions-getReactionServicePrice-truTien-getReactionServiceByFbUid-updateReactionService-'+requiredPrice, err.stack, 'na');
                                    return res.json({success: false, msg: 'Gia Hạn Dịch Vụ Tự Động Thả Tim Không Thành Công'});
                                } else {
                                    const dayNum = (newData.expiredTime - new Date().getTime())/(1000*60*60*24);
                                    reqToBotSv.updateReaction(fbUid._id, req.body.cookie, req.body.reactionRate, dayNum, req.body.likePagePost);
                                    return res.json({success: true, msg: 'Gia Hạn Dịch Vụ Tự Động Thả Tim Thành Công'});
                                }
                            });
                        } else {
                            let newData = {
                                responsibler: req.user.username,
                                fbUid: req.body.fbUid,
                                cookie: req.body.cookie,
                                cookieState: 'checking',
                                reactionRate: req.body.reactionRate,
                                likePagePost: req.body.likePagePost,
                                expiredTime: newExpiredTime,
                            };
                            reactionService.addReactionService(newData, (err, _newData)=>{
                                if(err){
                                    errLog.addErrLog(req.user, '/reactions-getReactionServicePrice-truTien-getReactionServiceByFbUid-addReactionService-'+requiredPrice, err.stack, 'na');
                                    return res.json({success: false, msg: 'Đăng Ký Dịch Vụ Tự Động Thả Tim Không Thành Công'});
                                } else {
                                    const dayNum = (newExpiredTime)/(1000*60*60*24);
                                    reqToBotSv.buyReaction(_newData._id, req.body.cookie, req.body.reactionRate, dayNum, req.body.likePagePost);
                                    return res.json({success: true, msg: 'Đăng Ký Dịch Vụ Tự Động Thả Tim Thành Công'});
                                }
                            })
                        }
                    })
                }
            });
        }
    });
});

router.post('/giahan', AUTH, function (req, res, next) {
    reactionServicePrice.getReactionServicePrice((err, price)=>{
        if(err){
            errLog.addErrLog(req.user, '/reactions-getReactionServicePrice', err.stack, 'na');
            return res.json({success: false, msg: 'Gia Hạn Dịch Vụ Tự Động Tương Tác Không Thành Công'});
        }
        let requiredPrice = price.basePrice;
        let newExpiredTime = 30*24*60*60*1000;
        if (req.body.period === 'oneMonth'){
            requiredPrice = requiredPrice*1;
        } else if (req.body.period === 'threeMonth') {
            newExpiredTime = 3*30*24*60*60*1000;
            requiredPrice = requiredPrice*3 - 3*requiredPrice*price.threeMonthDiscount;
        } else if (req.body.period === 'sixMonth') {
            newExpiredTime = 6*30*24*60*60*1000;
            requiredPrice = requiredPrice*6 - 6*requiredPrice*price.sixMonthDiscount;
        } else {
            return res.json({success: false, msg: 'Gia Hạn Dịch Vụ Tự Động Thả Tim Không Thành Công'});
        }
        if ( requiredPrice > req.user.money) {
            return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
        } else {
            User.truTien(req.user, requiredPrice , (err) =>{
                if(err){
                    errLog.addErrLog(req.user, '/reactions-getReactionServicePrice-truTien-'+requiredPrice, err.stack, 'na');
                    return res.json({success: false, msg: 'Lỗi Trừ Tiền, Vui Lòng Thực Hiện Lại'});
                } else {
                    const descrMoneyLog = req.user.username + ' Thanh Toán ' + requiredPrice + ' Auto Thả Tym';
                    moneyLog.addMoneyLog(req.user.username, req.user.username, -requiredPrice, descrMoneyLog);
                    reactionService.getReactionServiceById(req.body.id, (err, fbUid)=>{
                        if(err){
                            errLog.addErrLog(req.user, '/reactions-getReactionServicePrice-truTien-getReactionServiceByFbUid-'+requiredPrice, err.stack, 'na');
                        }
                        if(fbUid){
                            let newData = {
                                expiredTime: fbUid.expiredTime + newExpiredTime
                            };
                            reactionService.renewReactionService(req.body.id, newData, (err, _resData)=>{
                                if(err){
                                    errLog.addErrLog(req.user, '/reactions-getReactionServicePrice-truTien-getReactionServiceByFbUid-updateReactionService-'+requiredPrice, err.stack, 'na');
                                    return res.json({success: false, msg: 'Gia Hạn Dịch Vụ Tự Động Thả Tim Không Thành Công'});
                                } else {
                                    const dayNum = (newData.expiredTime - new Date().getTime())/(1000*60*60*24);
                                    reqToBotSv.updateReaction(_resData._id, _resData.cookie, _resData.reactionRate, dayNum, _resData.likePagePost);
                                    return res.json({success: true, msg: 'Gia Hạn Dịch Vụ Tự Động Thả Tim Thành Công'});
                                }
                            });
                        } else {
                            return res.json({success: false, msg: 'Gia Hạn Dịch Vụ Tự Động Thả Tim Không Thành Công'});
                        }
                    })
                }
            });
        }
    });
});

router.post('/pause', AUTH, (req, res)=>{
        reactionService.pauseReactionService(req.body.id, (err) =>{
        if(err){
            errLog.addErrLog(req.user, '/pause-pauseReactionService', err.stack, req.body.id);
            return res.json({success: false, msg: 'Auto Tym Pause Failed'});
        } else {
            reqToBotSv.pauseReaction(req.body.id);
            return res.json({success: true, msg: 'Auto Tym Paused'});
        }
    })

});

router.post('/unpause', AUTH, (req, res)=>{
    reactionService.getReactionServiceById(req.body.id, (err, reactionServ)=>{
        if(err){
            return res.json({success: false, msg: 'Id Not Found'});
        }
        if(reactionServ){
            if(reactionServ.lastPauseTime === 0){
                reactionService.unPauseReactionService(req.body.id, reactionServ.expiredTime, (err) =>{
                    if(err){
                        errLog.addErrLog(req.user, '/unpause-getReactionServiceById-unPauseReactionService', err.stack, req.body.id);
                        return res.json({success: false, msg: 'Auto Tym unPause Failed'});
                    } else {
                        const dayNum = (reactionServ.expiredTime - new Date().getTime())/(1000*60*60*24);
                        reqToBotSv.unPauseReaction(reactionServ._id, dayNum);
                        return res.json({success: true, msg: 'Auto Tym unPaused'});
                    }
                });
            } else {
                const timeNow = new Date().getTime();
                const newExpTime = reactionServ.expiredTime - reactionServ.lastPauseTime + timeNow;
                reactionService.unPauseReactionService(req.body.id, newExpTime, (err) =>{
                    if(err){
                        errLog.addErrLog(req.user, '/unpause-getReactionServiceById-unPauseReactionService', err.stack, req.body.id);
                        return res.json({success: false, msg: 'Auto Tym unPause Failed'});
                    } else {
                        const dayNum = (newExpTime - new Date().getTime())/(1000*60*60*24);
                        reqToBotSv.unPauseReaction(reactionServ._id, dayNum);
                        return res.json({success: true, msg: 'Auto Tym unPaused'});
                    }
                });
            }
        }
    });
});

router.post('/update', AUTH, (req, res)=>{
    reactionService.getReactionServiceById(req.body.id, (err, reacservice) =>{
        if(err){
            errLog.addErrLog(req.user, '/update-getReactionServiceById', err.stack, req.body.id);
            return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Tự Động Tương Tác Không Thành Công'});
        }
        if(reacservice){
            if(reacservice.cookieState === 'live'){
            if(reacservice.responsibler === req.user.username){
                let cookieState = '';
                if(req.body.cookie === reacservice.cookie){
                    cookieState = reacservice.cookieState
                } else {
                    cookieState = 'checking';
                }
                reactionService.updateCookieAndReactionRate(req.body.id, req.body.fbUid, req.body.cookie, cookieState, req.body.reactionRate, req.body.likePagePost, (err)=>{
                    if(err){
                        errLog.addErrLog(req.user, '/update-getReactionServiceById-updateCookieAndReactionRate', err.stack, req.body.id);
                        return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Tự Động Tương Tác Không Thành Công'});
                    } else {
                        reqToBotSv.updateReaction(req.body.id, req.body.cookie, req.body.reactionRate, req.body.likePagePost);

                        return res.json({success: true, msg: 'Cập Nhật Dịch Vụ Tự Động Tương Tác Thành Công'});
                    }
                });
            }else {
                return res.json({success: false, msg: 'Bạn Không Có Quyền Sửa Trạng Thái Dịch Vụ Của Tài Khoản FB này'});
            }
        } else {
            let cookieState = '';
            if(req.body.cookie === reacservice.cookie){
                cookieState = reacservice.cookieState
            } else {
                cookieState = 'checking';
            }
            reactionService.updateCookieAndReactionRate(req.body.id, req.body.fbUid, req.body.cookie, cookieState, req.body.reactionRate, req.body.likePagePost, (err, updateRes)=>{
                if(err){
                    errLog.addErrLog(req.user, '/update-getReactionServiceById-updateCookieAndReactionRate', err.stack, req.body.id);
                    return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Tự Động Tương Tác Không Thành Công'});
                } else {
                    const newDayNum = (updateRes.expiredTime - new Date().getTime())/(1000*60*60*24);
                    reqToBotSv.updateReaction(req.body.id, req.body.cookie, req.body.reactionRate, newDayNum, req.body.likePagePost);

                    return res.json({success: true, msg: 'Cập Nhật Dịch Vụ Tự Động Tương Tác Thành Công'});
                }
            });
        }
        } else {
            errLog.addErrLog(req.user, '/update-getReactionServiceById', err.stack, req.body.id);
            return res.json({success: false, msg: 'Cập Nhật Dịch Vụ Tự Động Tương Tác Không Thành Công'});
        }
    });
});

router.get('/getprice', AUTH, (req, res)=>{
    reactionServicePrice.getReactionServicePrice((err, price)=>{
        if(err) {
            errLog.addErrLog(req.user, '/getprice-getReactionServicePrice', err.stack, 'na');
        }
        if(price){
            return res.json({success: true, price});
        } else {
            return res.json({success: false});
        }
    })
});

router.get('/getlist', AUTH, (req, res)=>{
    reactionService.getReactionServiceByResponsibler(req.user.username, (err, bundle)=>{
        if(err) {
            errLog.addErrLog(req.user, '/getlist-getReactionServiceByResponsibler', err.stack, 'na');
        }
        if(bundle){
            for(let subBundle of bundle){
                const remainingDays = (subBundle.expiredTime - (new Date().getTime()))/(1000*60*60*24);
                subBundle.expiredTime = parseFloat(remainingDays).toFixed(2);
            }
            return res.json({success: true, bundle});
        } else {
            return res.json({success: false});
        }
    })
});

router.post('/updatecookiestate', (req, res)=>{
    let updateData = {
        id: req.body.id,
        cookieState: req.body.cookieState,
        fbUid: req.body.fbUid
    }
    reactionService.updateReactionService(updateData);
});

module.exports = router;