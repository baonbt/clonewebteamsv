const express = require('express');
const router = express.Router();
const passport = require('passport');
var AUTH = passport.authenticate('jwt', {session:false});
const request = require('request');
const errLog = require('../models/errLog');

const _botsvAddress = 'http://localhost:8080/';

const botsvAddress = _botsvAddress+'/angularwebselling';

const updateReactionAdress = botsvAddress + '/buff/tym/buy';
const pauseReactionAdress = botsvAddress + '/buff/tym/onOff';

const updateBuffLikeAdress = botsvAddress + '/buff/like/buy';
const pauseBuffLikeAdress = botsvAddress + '/buff/like/onOff';

const updateCommentAdress = botsvAddress + '/buff/comment/buy';
const pauseCommentAdress = botsvAddress + '/buff/cmt/onOff';


module.exports.buyReaction = function (id, cookie, reactionRate, dayNum, likePagePost, callback) {
    const toBotSvBundle = {
        id: id,
        cookie: cookie,
        reactionRate: reactionRate,
        dayNum: dayNum,
        interactProfileOnly: likePagePost
    };
    console.log(toBotSvBundle);
    request.post({
        url: updateReactionAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-buyReaction', err.stack, 'na');
        }
    });
};

module.exports.updateReaction = function (id, cookie, reactionRate, dayNum, likePagePost, callback) {
    const toBotSvBundle = {
        id: id,
        cookie: cookie,
        reactionRate: reactionRate,
        dayNum: dayNum,
        interactProfileOnly: likePagePost
    };
    console.log(toBotSvBundle);
    request.post({
        url: updateReactionAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-updateReaction', err.stack, 'na');
        }
    });
};

module.exports.pauseReaction = function (id, callback) {
    const toBotSvBundle = {
        id: id,
        status: false
    };
    request.post({
        url: pauseReactionAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-pauseReaction', err, 'na');
        }
    });
};

module.exports.unPauseReaction = function (id, dayNum, callback) {
    const toBotSvBundle = {
        id: id,
        dayNum: dayNum,
        status: true
    };
    request.post({
        url: pauseReactionAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-unPauseReaction', err, 'na');
        }
    });
};

module.exports.buyBuffLike = function (id, fbUid, reactionRate, dayNum, quantity, rushHour, maxPerDay, delay, callback) {
    const toBotSvBundle = {
        id: id,
        fbUid: fbUid,
        reactionRate: reactionRate,
        dayNum: dayNum,
        quantity: quantity,
        rushHour: rushHour,
        maxPerDay: maxPerDay,
        duration: delay
    };
    request.post({
        url: updateBuffLikeAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-buyBuffLike', err.stack, 'na');
        }
    });
};

module.exports.updateBuffLike = function (id, fbUid, reactionRate, rushHour, maxPerDay, likeNum, delay, callback) {
    //TODO
    const toBotSvBundle = {
        id: id,
        fbUid: fbUid,
        reactionRate: reactionRate,
        rushHour: rushHour,
        maxPerDay: maxPerDay,
        quantity: likeNum,
        duration: delay,
        dayNum: 0
    };
    // console.log(toBotSvBundle);
    request.post({
        url: updateBuffLikeAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            console.log('sucaaa');
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-updateBuffLike', err.stack, 'na');
        }
        console.log('suc');
    });
};

module.exports.pauseBuffLike = function (id, callback) {
    const toBotSvBundle = {
        id: id,
        status: false
    };
    request.post({
        url: pauseBuffLikeAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-pauseBuffLike', err.stack, 'na');
        }
    });
};

module.exports.unPauseBuffLike = function (id, dayNum, callback) {
    const toBotSvBundle = {
        id: id,
        dayNum: dayNum,
        status: true
    };
    request.post({
        url: pauseBuffLikeAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-unPauseBuffLike', err.stack, 'na');
        }
    });
};

module.exports.commentUpdate = function (id, fbUid, dayNum, commentNum, maxPerDay, commentBundles, callback) {
    const toBotSvBundle = {
        id: id,
        fbUid: fbUid,
        dayNum: dayNum,
        quantity: commentNum,
        maxPerDay: maxPerDay,
        commentBundleDTOS: commentBundles
    };
    request.post({
        url: updateCommentAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-unPauseBuffLike', err.stack, 'na');
        }
    });
};

module.exports.pauseComment = function (id, fbUid, callback) {
    const toBotSvBundle = {
        id: id,
        status: false,
        fbUid: fbUid
    };
    request.post({
        url: pauseCommentAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-pauseBuffLike', err.stack, 'na');
        }
    });
};

module.exports.unPauseComment = function (id, dayNum, fbUid, callback) {
    const toBotSvBundle = {
        id: id,
        dayNum: dayNum,
        status: true,
        fbUid: fbUid
    };
    request.post({
        url: pauseCommentAdress,
        method: "POST",
        json: true,   // <--Very important!!!
        body: toBotSvBundle
    }, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(toBotSvBundle, '/requestToBotSv-unPauseBuffLike', err.stack, 'na');
        }
    });
};