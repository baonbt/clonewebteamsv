const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const auth_secret = require('../config/secrets');
var AUTH = passport.authenticate('jwt', {session:false});
const roles = require('../guards/levels');
const accountLog = require('../models/accountLog');
const moneyLog = require('../models/moneyLog');
const errLog = require('../models/errLog');
const request = require('request');

const _botsvAddress = 'http://localhost:8080/';

const botsvAddress = _botsvAddress+'/angularwebselling';

const checkPointsAddress = botsvAddress + '/account/na';
const botLogsAddress = botsvAddress + '/buff/cmt/onOff';

router.get('/accountlog', AUTH, function (req, res, next) {
    if(req.user.accountLevel >=99){
        accountLog.getAllAccountLog((err, user) => {
            if (err) throw err;
            if (!user){
                return res.json({success: false, msg: 'Responsibler Not Found'});
            }
            return res.json(user);
        });
    }else {
        accountLog.getAccountLogByResponsibler(req.user.username, (err, user) => {
            if (err) throw err;
            if (!user){
                return res.json({success: false, msg: 'Responsibler Not Found'});
            }
            return res.json(user);
        });
    }
});
router.get('/moneylog', AUTH, function (req, res, next) {
    if(req.user.accountLevel >=99){
        moneyLog.getAllMoneyLog((err, user) => {
            if (err) throw err;
            if (!user){
                return res.json({success: false, msg: 'Responsibler Not Found'});
            }
            return res.json(user);
        });
    }else {
        moneyLog.getMoneyLogAccount(req.user.username, (err, user) => {
            if (err) throw err;
            if (!user){
                return res.json({success: false, msg: 'Responsibler Not Found'});
            }
            return res.json(user);
        });
    }
});
router.get('/err', AUTH, roles.Auth(99), function (req, res, next) {
        errLog.getAllErrLog((err, user) => {
            if (err) throw err;
            if (!user){
                return res.json({success: false, msg: 'Responsibler Not Found'});
            }
            return res.json({success: true, user});
        });
});
router.get('/botlogs', AUTH, roles.Auth(99), function (req, res, next) {
        request.get(botLogsAddress, (err, botsvres, body) => {
            if (err) {
                errLog.addErrLog(req.user, '/botlogs', err, req.body.id);
                return res.json({success: false});
            }
            return res.json(body);
        });
});
router.get('/checkpoints', AUTH, roles.Auth(5), function (req, res, next) {
    request.get(checkPointsAddress, (err, botsvres, body) => {
        if (err) {
            errLog.addErrLog(req.user, '/checkpoints', err, req.body.id);
            return res.json({success: false});
        }
        if(body){
            let newArray = [];
            let newjs = JSON.parse(body);
            for(let cp of newjs){
                let subArray = [];
                subArray.push(cp.accountId);
                subArray.push(cp.uid);
                subArray.push(cp.password);
                subArray.push(cp.realName);
                subArray.push(cp.email);
                subArray.push(cp.phone);
                subArray.push(new Date(cp.birthday).toDateString());
                subArray.push(cp.address);
                newArray.push(subArray);
            }
            return res.json({data:newArray});
        } else {
            return res.json({data:[["khong co gi", "khong co gi", "khong co gi"]]});
        }
    });
});


module.exports = router;