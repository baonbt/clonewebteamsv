const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const auth_secret = require('../config/secrets');
var AUTH = passport.authenticate('jwt', {session:false});
const roles = require('../guards/levels');
const accountLog = require('../models/accountLog');
const moneyLog = require('../models/moneyLog');
const request = require('request');
const errLog = require('../models/errLog');
const reqToBotSv = require('./botSv');

//gen default user
router.get('/gendefaultadmin', (req, res, next)=>{
    let newUser = new User({
        username: 'defaultadmin',
        password: 'defaultpassword'
    });
    User.addUser(newUser, (err, data) => {
        if(err){
            return res.json({success: false, msg: 'Đăng Ký Không Thành Công, Vui Lòng Thử Lại'});
        } else {
            accountLog.addAccountLog(req.body.username, req.body.username);
            return res.json({success: true, msg: 'Đăng Ký Thành Công'});
        }
    });
});

//test connection
router.get('/botsvtestconnection', (req, res, next)=>{
    const address = 'http://192.168.137.1:8080/';
    // const address = 'http://45.77.32.223:8080';
    request(address, (err, botsvres, body) => {
        if (err) {
            return res.json({success: false, msg: 'Err On Connection'});
        }
        return res.json({success: true, body});
    });
});

//register
router.post('/register', AUTH, roles.Auth(6), function (req, res, next) {
    let newUser = new User({
        username: req.body.username,
        password: req.body.password
    });
    User.getUserByUsername(req.body.username, (err, user) => {
        if(err){
            errLog.addErrLog(req.user, '/register-getUserByUsername', err.stack, 'na');
            return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
        }
        if (user){
            return res.json({success: false, msg: 'Tài Khoản Đã Tồn Tại'});
        } else {
            User.addUser(newUser, (err, data) => {
                if(err){
                    return res.json({success: false, msg: 'Đăng Ký Không Thành Công, Vui Lòng Thử Lại'});
                } else {
                    accountLog.addAccountLog(req.user.username, req.body.username);
                    return res.json({success: true, msg: 'Đăng Ký Thành Công'});
                }
            });
        }
    });
});

//authen
router.post('/authen', function (req, res, next) {
    const username = req.body.username;
    const password = req.body.password;
    User.getUserByUsername(username, (err, user) => {
        if(err){
            errLog.addErrLog(req.user, '/authen-getUserByUsername', err.stack, 'na');
            return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
        }
        if (!user){
            return res.json({success: false, msg: 'khong tim thay nguoi dung'});
        }
        User.comparePassword(password, user.password, (err, isMatch) => {
            if(err){
                errLog.addErrLog(req.user, '/authen-getUserByUsername-comparePassword', err.stack, 'na');
                return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
            }
            if (isMatch) {
                const token = jwt.sign(user.toJSON(), auth_secret.secret, {
                    expiresIn: 60000
                });
                res.json({
                    success: true,
                    msg: 'Welcome Back ',
                    token: 'JWT ' + token,
                    user: {
                        id: user._id
                    }
                });
            } else {
                return res.json({success: false, msg: 'Mật Khẩu Không Chính Xác'});
            }
        });
    });
});
router.get('/profile', AUTH, (req, res, next) => {
    const user = {
        username: req.user.username,
        phone: req.user.phone,
        email: req.user.email,
        accountLevel: req.user.accountLevel,
        money: req.user.money
    };
    res.json(user);
});
router.post('/getinfo', AUTH, roles.Auth(6), function (req, res, next) {
    const username = req.body.username;
    const responsible = {
        username: req.user.username,
        phone: req.user.phone,
        email: req.user.email,
        accountLevel: req.user.accountLevel,
        money: req.user.money
    };
    User.getUserByUsername(username, (err, user) => {
        if(err){
            errLog.addErrLog(req.user, '/getinfo-getUserByUsername', err.stack, 'na');
            return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
        }
        if (!user){
            return res.json({success: false, msg: 'khong tim thay nguoi dung'});
        } else {
            const data = {
                username: user.username,
                email: user.email,
                phone: user.phone,
                name: user.name
            };
            return res.json({success: true, msg: 'user found', data, responsible});
        }
    });
});
router.post('/deposite', AUTH, roles.Auth(6), function (req, res, next) {
    const username = req.body.username;
    const money = req.body.money;
    User.getUserByUsername(username, (err, user) => {
        if(err){
            errLog.addErrLog(req.user, '/deposite-getUserByUsername', err.stack, 'na');
            return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
        }
        if (!user){
            return res.json({success: false, msg: 'khong tim thay nguoi dung'});
        }
        if(req.user.accountLevel >=99){
            User.congTien(user, money, (err) =>{
                console.log(err);
               if(err){
                   errLog.addErrLog(req.user, '/deposite-getUserByUsername-congTien', err.stack, 'na');
                   return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
               } else {
                   const descrMoneyLog = username + ' Nhận ' + money + ' Từ ' + req.user.username;
                   moneyLog.addMoneyLog(req.user.username, username, money, descrMoneyLog);
                   return res.json({success: true, msg: 'Đã Thêm '+money+' Vào Tài Khoản '+username});
               }
            });
        } else {
            if(req.user.money >= money){
                User.truTien(req.user, money, (err) =>{
                    if(err){
                        errLog.addErrLog(req.user, '/deposite-getUserByUsername-truTien', err.stack, 'na');
                        return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
                    }else {
                        const descriMoneyLog = req.user.username + ' Chuyển ' + money + ' Sang ' + username;
                        moneyLog.addMoneyLog(req.user.username, req.user.username, -money, descriMoneyLog);
                        User.congTien(user, money, (err) =>{
                            if(err){
                                errLog.addErrLog(req.user, '/deposite-getUserByUsername-truTien-congTien', err.stack, 'na');
                                return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
                            } else {
                                const descrMoneyLog = username + 'Nhận ' + money + 'Từ ' + req.user.username;
                                moneyLog.addMoneyLog(req.user.username, username, money, descrMoneyLog);
                                return res.json({success: true, msg: 'Đã Thêm '+money+' Vào Tài Khoản '+username});
                            }
                        });
                    }
                });

            }else {
                return res.json({success: false, msg: 'Số Tiền Trong Tài Khoản Không Đủ'});
            }
        }
    });
});

router.post('/changepassword', AUTH, (req, res, next)=>{
    if(req.body.oldPassword === undefined || req.body.newPassword === undefined){
        return res.json({success: true});
    }
    User.comparePassword(req.body.oldPassword, req.user.password, (err, isMatch)=>{
        if(err){
            errLog.addErrLog(req.user, '/changepassword-getUserByUsername', err.stack, 'na');
            return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
        }
        if(isMatch){
            User.updateUserPassword(req.user.username, req.body.newPassword, (err)=>{
                if(err){
                    return res.json({success: false, msg: 'Thay Đổi Mật Khẩu Không Thành Công, Vui Lòng Thử Lại'});
                } else {
                    return res.json({success: true, msg: 'Thay Đổi Mật Khẩu Thành Công'});
                }
            });
        } else {
            return res.json({success: false, msg: 'Mật Khẩu Cũ Không Chính Xác'});
        }
    });
});

router.post('/resetpassword', AUTH, roles.Auth(99), (req, res, next)=>{
    User.getUserByUsername(req.body.username, (err, user)=>{
        if(err){
            errLog.addErrLog(req.user, '/resetpassword-getUserByUsername', err.stack, 'na');
            return res.json({success: false, msg: 'Đã Xảy Ra Lỗi'});
        }
        if(user){
            const defaultPassword = '123@';
            User.updateUserPassword(req.body.username, defaultPassword, (err)=>{
                if(err){
                    return res.json({success: false, msg: 'Reset Mật Khẩu Không Thành Công, Vui Lòng Thử Lại'});
                } else {
                    return res.json({success: true, msg: 'Reset Mật Khẩu Thành Công'});
                }
            });
        } else {
            return res.json({success: false, msg: 'Username Không Chính Xác'});
        }
    });
});

module.exports = router;