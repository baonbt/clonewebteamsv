const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
//routes
const users = require('./routes/users');
const reactions_service = require('./routes/services_for_customer/reactions');
const logs = require('./routes/logs');
const buffLike_service = require('./routes/services_for_customer/buffLike');
const comment_service = require('./routes/services_for_customer/comments');
const loginTool = require('./routes/loginTool');

//database
const config_databse = require('./config/database');
mongoose.connect(config_databse.theRedDog);
//on connection
mongoose.connection.on('connected', function () {
    console.log('connect to database status: ' + config_databse.theRedDog);
});
//on connection to db error
mongoose.connection.on('error', function (err) {
    console.log('connected to database ' + err);
});

const port = process.env.PORT || 9999;
const app = express();

//CORS middleware
app.use(cors());

//set static folder
app.use(express.static(path.join(__dirname, 'public')));

// body parser middleware
app.use(bodyParser.json());

//pasport middleware
require('./config/passport')(passport);
app.use(passport.initialize());
app.use(passport.session());

app.use('/users', users);
app.use('/reactionserv', reactions_service);
app.use('/logs', logs);
app.use('/bufflikeserv', buffLike_service);
app.use('/commentserv', comment_service);
app.use('/logintool', loginTool);


//index route
// app.get('/*', function(req, res) {
//     return res.sendFile(path.join(config.root, 'index.html'));
// });

app.all('/*', function(req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendFile('public/index.html', { root: __dirname });
});

app.listen(port, function() {
    console.log('started ' + port );
});