const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const ReactionServiceSchema = mongoose.Schema({
    responsibler: {
        type: String,
        require: true
    },
    fbUid: {
        type: String,
        require: true
    },
    cookie: {
        type: String,
        require: true
    },
    cookieState: {
        type: String,
        require: true
    },
    reactionRate: {
        type: String,
        require: true
    },
    startTime: {
        type: Number,
        require: true
    },
    expiredTime:{
        type: Number,
        require: true,
    },
    runningState:{
        type: Boolean,
        require: true,
    },
    likePagePost:{
        type: String,
        require: true,
    },
    lastPauseTime:{
        type: Number,
        require: true,
    }
});

const ReactionService = module.exports = mongoose.model('ReactionService', ReactionServiceSchema);

module.exports.getReactionServiceById = function (id, callback) {
    ReactionService.findById(id, callback);
};

module.exports.getReactionServiceByResponsibler = function (responsibler, callback) {
    const query = {responsibler: responsibler};
    ReactionService.find(query, callback);
};

module.exports.getReactionServiceByFbuid = function (fbUid, callback) {
    const query = {fbUid: fbUid};
    ReactionService.findOne(query, callback);
};

module.exports.getReactionService = function (callback) {
    const query = {};
    ReactionService.find(query, callback);
};

module.exports.addReactionService = function (data, callback) {
    let newReactionService = new ReactionService({
        responsibler: data.responsibler,
        fbUid: data.fbUid,
        cookie: data.cookie,
        cookieState: 'checking',
        reactionRate: data.reactionRate,
        startTime: new Date().getTime(),
        expiredTime: new Date().getTime() + data.expiredTime,
        runningState: true,
        likePagePost: data.likePagePost
    });
    newReactionService.save(callback);
};

module.exports.updateReactionService = function (data, callback) {
    const query = {fbUid: data.fbUid};
    ReactionService.findOneAndUpdate(query, data, {new: true}, callback);
};
module.exports.renewReactionService = function (id, data, callback) {
    ReactionService.findByIdAndUpdate(id, data, {new: true}, callback);
};

module.exports.pauseReactionService = function (id, callback) {
    const update = {
        runningState: false,
        lastPauseTime: new Date().getTime()
    };
    ReactionService.findByIdAndUpdate(id, update, {new: true}, callback);
};

module.exports.unPauseReactionService = function (id, newExpTime, callback) {
    const update = {
        runningState: true,
        lastPauseTime: 0,
        expiredTime: newExpTime
    };
    ReactionService.findByIdAndUpdate(id, update, {new: true}, callback);
};

module.exports.updateCookieAndReactionRate = function (id, fbUid, cookie, cookieState, reactionRate, likePagePost, callback) {
    const update = {
        cookie: cookie,
        fbUid: fbUid,
        cookieState: cookieState,
        reactionRate: reactionRate,
        likePagePost: likePagePost
    };
    ReactionService.findByIdAndUpdate(id, update, {new: true}, callback);
};

module.exports.updateCookieState = function (id, cookieState, callback) {
    const update = {
        cookieState: cookieState
    };
    ReactionService.findByIdAndUpdate(id, update, {new: true}, callback);
};