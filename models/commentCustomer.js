const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const commentCustomerSchema = mongoose.Schema({
    username: {
        type: String,
        require: true
    },
    profileId: {
        type: String,
        require: true
    },
    profileIdStatus: {
        type: String,
        require: true
    },
    commentNum: {
        type: Number,
        require: true
    },
    maxCommentNum: {
        type: Number,
        require: true
    },
    startTime: {
        type: Number,
        require: true
    },
    expiredTime:{
        type: Number,
        require: true,
    },
    runningState:{
        type: Boolean,
        require: true,
    },
    lastPauseTime:{
        type: Number,
        require: true,
    },
    commentBundles:{
        type: Array,
        require: true,
    },
    maxPerDay:{
        type: Number,
        require: true,
    },
    maxMaxPerDay:{
        type: Number,
        require: true,
    }

});

const commentCustomer = module.exports = mongoose.model('commentCustomer', commentCustomerSchema);

module.exports.getCommentCustomerById = function (id, callback) {
    commentCustomer.findById(id, callback);
};

module.exports.getCommentCustomerByUsername = function (username, callback) {
    const query = {username: username};
    commentCustomer.find(query, callback);
};

module.exports.getCommentCustomerByProfileId = function (profileId, callback) {
    const query = {profileId: profileId};
    commentCustomer.find(query, callback);
};

module.exports.getAllCommentCustomer = function (callback) {
    const query = {};
    commentCustomer.find(query, callback);
};

module.exports.getAllCustomerByBundleId = function (id, callback) {
    const query = {commentBundles: id};
    commentCustomer.find(query, callback);
};

module.exports.addCommentCustomer = function (username, profileId, expiredTime, commentNum, commentBundles, maxPerDay, callback) {
    let newCommentCustomer = new commentCustomer({
        username: username,
        profileId: profileId,
        profileIdStatus: 'CHECKING',
        commentNum: commentNum,
        maxCommentNum: commentNum,
        startTime: new Date().getTime(),
        expiredTime: expiredTime,
        runningState: true,
        lastPauseTime: 0,
        commentBundles: commentBundles,
        maxPerDay: maxPerDay,
        maxMaxPerDay: maxPerDay
    });
    newCommentCustomer.save(callback);
};

module.exports.updateProfileId = function (id, profileId, commentBundles, commentNum, callback) {
    let updateCommentCustomer = {
        profileId: profileId,
        commentBundles: commentBundles,
        commentNum: commentNum
    };
    commentCustomer.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};

module.exports.giaHan = function (id, expiredTime, callback) {
    let updateCommentCustomer = {
        expiredTime: expiredTime
    };
    commentCustomer.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};

module.exports.muaThemComment = function (id, maxCommentNum, callback) {
    let updateCommentCustomer = {
        maxCommentNum: maxCommentNum,
        commentNum: maxCommentNum
    };
    commentCustomer.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};

module.exports.muaThemPost = function (id, maxMaxPerDay, callback) {
    let updateCommentCustomer = {
        maxMaxPerDay: maxMaxPerDay,
        maxPerDay: maxMaxPerDay
    };
    commentCustomer.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};

module.exports.updateProfileIdStatus = function (id, profileIdStatus, callback) {
    let updateCommentCustomer = {
        profileIdStatus: profileIdStatus
    };
    commentCustomer.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};

module.exports.pause = function (id, callback) {
    let updateCommentCustomer = {
        runningState: false,
        lastPauseTime: new Date().getTime()
    };
    commentCustomer.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};

module.exports.unPause = function (id, newExpiredTime, callback) {
    let updateCommentCustomer = {
        runningState: true,
        lastPauseTime: 0,
        expiredTime: newExpiredTime
    };
    commentCustomer.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};
