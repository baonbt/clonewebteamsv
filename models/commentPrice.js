const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const CommentServicePriceSchema = mongoose.Schema({
    serviceName: {
        type: String,
        require: true
    },
    basePrice: {
        type: Number,
        require: true
    },
    oneMonthDiscount: {
        type: Number,
        require: true
    },
    threeMonthDiscount: {
        type: Number,
        require: true
    },
    sixMonthDiscount: {
        type: Number,
        require: true
    },
    tenCmtDiscount: {
        type: Number,
        require: true
    },
    twentyCmtDiscount: {
        type: Number,
        require: true
    },
    thirtyCmtDiscount: {
        type: Number,
        require: true
    }
});

const CommentServicePrice = module.exports = mongoose.model('CommentServicePrice', CommentServicePriceSchema);

module.exports.getPriceListById = function (id, callback) {
    CommentServicePrice.findById(id, callback);
};

module.exports.getCommentServicePrice = function (callback) {
    const query = {serviceName: 'comment'};
    CommentServicePrice.findOne(query, callback);
};

module.exports.updateCommentServicePrice = function (newPrice, callback) {
    const query = {serviceName: 'comment'};
    CommentServicePrice.findOneAndUpdate(query, newPrice, {upsert:true, new: true}, callback);
};

