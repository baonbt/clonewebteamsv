const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const MoneyLogSchema = mongoose.Schema({
    responsibler: {
        type: String,
        require: true
    },
    account: {
        type: String,
        require: true
    },
    time: {
        type: Number,
        require: true
    },
    amount: {
        type: Number,
        require: true
    },
    description: {
        type: String,
        require: true
    }
});

const MoneyLog = module.exports = mongoose.model('MoneyLog', MoneyLogSchema);

module.exports.getMoneyLogById = function (id, callback) {
    MoneyLog.findById(id, callback);
};

module.exports.getMoneyLogAccount = function (account, callback) {
    const query = {account: account};
    MoneyLog.find(query, callback);
};

module.exports.getAllMoneyLog = function (callback) {
    const query = {};
    MoneyLog.find(query, callback);
};

module.exports.addMoneyLog = function (responsibler, account, amount, description, callback) {
    let newAccountLog = new MoneyLog({
        responsibler: responsibler,
        account: account,
        time: new Date().getTime(),
        amount: amount,
        description: description
    });
    newAccountLog.save(callback);
};

