const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const AccountLogSchema = mongoose.Schema({
    responsibler: {
        type: String,
        require: true
    },
    createdAccount: {
        type: String,
        require: true
    },
    time: {
        type: Number,
        require: true
    }
});

const AccountLog = module.exports = mongoose.model('AccountLog', AccountLogSchema);

module.exports.getAccountLogById = function (id, callback) {
    AccountLog.findById(id, callback);
};

module.exports.getAccountLogByResponsibler = function (responsibler, callback) {
    const query = {responsibler: responsibler};
    AccountLog.find(query, callback);
};

module.exports.getAllAccountLog = function (callback) {
    const query = {};
    AccountLog.find(query, callback);
};

module.exports.addAccountLog = function (responsibler, createdAccount, callback) {
    let newAccountLog = new AccountLog({
        responsibler: responsibler,
        createdAccount: createdAccount,
        time: new Date().getTime()
    });
    newAccountLog.save(callback);
};

