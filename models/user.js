const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const UserSchema = mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true
    },
    username: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    accountLevel: {
        type: Number,
        default: 1
    },
    money: {
        type: Number,
        default: 0
    },
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getUserById = function (id, callback) {
    User.findById(id, callback);
};

module.exports.getUserByUsername = function (username, callback) {
    const query = {username: username};
    User.findOne(query, callback);
};

module.exports.addUser = function (newUser, callback) {
    bcrypt.genSalt(10, (err, salt) =>{
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
};

module.exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
};

module.exports.updateUserLevel = function (username, newPassword, callback) {
    // console.log(user);
    const userName = {username: username};
    const accType = {password: newPassword};
    User.findOneAndUpdate(userName, accType, {new: true}, callback);
};

module.exports.updateUserPassword = function (username, newPassword, callback) {
    // console.log(user);
    const userName = {username: username};
    bcrypt.genSalt(10, (err, salt) =>{
        bcrypt.hash(newPassword, salt, (err, hash) => {
            if (err) throw err;
            const userPassowrd = {password: hash};
            User.findOneAndUpdate(userName, userPassowrd, {new: true}, callback);
        });
    });
};

module.exports.truTien = function (user, amount, callback) {
    const userName = {username: user.username};
    let newAmount = user.money - amount;
    const newMoney = {money: newAmount};
    User.findOneAndUpdate(userName, newMoney, {new: true}, callback);
};

module.exports.congTien = function (user, amount, callback) {
    // console.log(user);
    const userName = {username: user.username};
    User.findOne(userName, (err, newUser)=>{
        if(err) throw err;
        if(newUser){
            let newAmount = newUser.money + amount;
            const newMoney = {money: newAmount};
            User.findOneAndUpdate(userName, newMoney, {new: true}, callback);
        }
    });

};