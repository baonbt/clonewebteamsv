const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const ReactionServicePriceSchema = mongoose.Schema({
    serviceName: {
        type: String,
        require: true
    },
    basePrice: {
        type: Number,
        require: true
    },
    oneMonthDiscount: {
        type: String,
        require: true
    },
    threeMonthDiscount: {
        type: Number,
        require: true
    },
    sixMonthDiscount: {
        type: Number,
        require: true
    }
});

const ReactionServicePrice = module.exports = mongoose.model('ReactionServicePrice', ReactionServicePriceSchema);

module.exports.getPriceListById = function (id, callback) {
    ReactionServicePrice.findById(id, callback);
};

module.exports.getReactionServicePrice = function (callback) {
    const query = {serviceName: 'autoReaction'};
    ReactionServicePrice.findOne(query, callback);
};

module.exports.updateReactionServicePrice = function (newPrice, callback) {
    const query = {serviceName: 'autoReaction'};
    ReactionServicePrice.findOneAndUpdate(query, newPrice, {upsert:true, new: true}, callback);
};

