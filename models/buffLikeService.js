const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const BuffLikeServiceSchema = mongoose.Schema({
    responsibler: {
        type: String,
        require: true
    },
    profileId: {
        type: String,
        require: true
    },
    likeNum: {
        type: Number,
        require: true
    },
    maxLikeNum: {
        type: Number,
        require: true
    },
    reactionRate: {
        type: String,
        require: true
    },
    rushHour: {
        type: String,
        require: true
    },
    delay: {
        type: Number,
        require: true
    },
    maxPerDay: {
        type: Number,
        require: true
    },
    maxMaxPerDay: {
        type: Number,
        require: true
    },
    startTime: {
        type: Number,
        require: true
    },
    expiredTime:{
        type: Number,
        require: true,
    },
    runningState:{
        type: Boolean,
        require: true,
    },
    lastPauseTime:{
        type: Number,
        require: true,
    }
});

const BuffLikeService = module.exports = mongoose.model('BuffLikeService', BuffLikeServiceSchema);

module.exports.getBuffLikeServiceById = function (id, callback) {
    BuffLikeService.findById(id, callback);
};

module.exports.getBuffLikeServiceByResponsibler = function (responsibler, callback) {
    const query = {responsibler: responsibler};
    BuffLikeService.find(query, callback);
};

module.exports.getBuffLikeServiceByProfiledId = function (profileId, callback) {
    const query = {profileId: profileId};
    BuffLikeService.findOne(query, callback);
};

module.exports.getBuffLikeService = function (callback) {
    const query = {};
    BuffLikeService.find(query, callback);
};

module.exports.addBuffLikeService = function (data, callback) {
    let newBuffLikeService = new BuffLikeService({
        responsibler: data.responsibler,
        profileId: data.profileId,
        likeNum: data.likeNum,
        maxLikeNum: data.maxLikeNum,
        maxPerDay: data.maxPerDay,
        maxMaxPerDay: data.maxMaxPerDay,
        rushHour: data.rushHour,
        delay: data.delay,
        reactionRate: data.reactionRate,
        startTime: new Date().getTime(),
        expiredTime: new Date().getTime() + data.expiredTime,
        runningState: true,
        lastPauseTime: 0
    });
    newBuffLikeService.save(callback);
};
module.exports.updateBuffLikeService = function (data, callback) {
    const query = {fbUid: data.fbUid};
    BuffLikeService.findOneAndUpdate(query, data, {new: true}, callback);
};

module.exports.pauseBuffLikeService = function (id, callback) {
    const update = {
        runningState: false,
        lastPauseTime: new Date().getTime()
    };
    BuffLikeService.findByIdAndUpdate(id, update, {new: true}, callback);
};

module.exports.unPauseBuffLikeService = function (id, newExpTime, callback) {
    const update = {
        runningState: true,
        expiredTime: newExpTime
    };
    BuffLikeService.findByIdAndUpdate(id, update, {new: true}, callback);
};


module.exports.updateProfileLinkAndReactionRate = function (id, profileId, reactionRate, maxPerDay, maxMaxPerDay, delay, rushHour, newLikeNum, callback) {
    const update = {
        profileId: profileId,
        reactionRate: reactionRate,
        maxMaxPerDay: maxMaxPerDay,
        maxPerDay: maxPerDay,
        delay: delay,
        rushHour: rushHour,
        likeNum: newLikeNum
    };
    BuffLikeService.findByIdAndUpdate(id, update, {new: true}, callback);
};

module.exports.renewalRecord = function (id, maxLikeNum, maxMaxPerDay, expiredTime, callback) {
    const update = {
        likeNum: maxLikeNum,
        maxLikeNum: maxLikeNum,
        maxMaxPerDay: maxMaxPerDay,
        maxPerDay: maxMaxPerDay,
        expiredTime: expiredTime
    };
    BuffLikeService.findByIdAndUpdate(id, update, {new: true}, callback);
};