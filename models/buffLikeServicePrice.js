const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const BuffLikeServicePriceSchema = mongoose.Schema({
    serviceName: {
        type: String,
        require: true
    },
    basePrice: {
        type: Number,
        require: true
    },
    oneMonthDiscount: {
        type: Number,
        require: true
    },
    threeMonthDiscount: {
        type: Number,
        require: true
    },
    sixMonthDiscount: {
        type: Number,
        require: true
    },
    fiftyLikeDiscount: {
        type: Number,
        require: true
    },
    oneHundredLikeDiscount: {
        type: Number,
        require: true
    },
    twoHundredLikeDiscount: {
        type: Number,
        require: true
    },
    threeHundredLikeDiscount: {
        type: Number,
        require: true
    },
    sixHundredLikeDiscount: {
        type: Number,
        require: true
    },
    oneThousandTwoHundredLikeDiscount: {
        type: Number,
        require: true
    }
});

const BuffLikeServicePrice = module.exports = mongoose.model('BuffLikeServicePrice', BuffLikeServicePriceSchema);

module.exports.getPriceListById = function (id, callback) {
    BuffLikeServicePrice.findById(id, callback);
};

module.exports.getBuffLikeServicePrice = function (callback) {
    const query = {serviceName: 'buffLike'};
    BuffLikeServicePrice.findOne(query, callback);
};

module.exports.updateBuffLikeServicePrice = function (newPrice, callback) {
    const query = {serviceName: 'buffLike'};
    BuffLikeServicePrice.findOneAndUpdate(query, newPrice, {upsert:true, new: true}, callback);
};

