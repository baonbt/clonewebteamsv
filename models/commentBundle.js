const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const commentBundleSchema = mongoose.Schema({
    username: {
        type: String,
        require: true
    },
    bundleName: {
        type: String,
        require: true
    },
    hashTag: {
        type: String,
        require: true
    },
    replyRate: {
        type: String,
        require: true
    },
    commentOrder: {
        type: String,
        require: true
    },
    delay: {
        type: Number,
        require: true
    },
    rushHour: {
        type: String,
        require: true
    },
    commentContents:{
        type: String,
        require: true
    }
});

const commentBundle = module.exports = mongoose.model('commentBundle', commentBundleSchema);

module.exports.getCommentBundleById = function (id, callback) {
    commentBundle.findById(id, callback);
};

module.exports.removeById = function (id, callback) {
    commentBundle.findByIdAndRemove(id, callback);
};

module.exports.getListCommentBundleById = function (ids, callback) {
    const query = {
        '_id': { $in: ids}
    };
    commentBundle.find(query, callback);
};

module.exports.getCommentBundleByUsername = function (username, callback) {
    const query = {username: username};
    commentBundle.find(query, callback);
};

module.exports.getAllCommentBundle = function (callback) {
    const query = {};
    commentBundle.find(query, callback);
};

module.exports.addCommentBundle = function (username, bundleName, hashTag, replyRate, commentOrder, delay, rushHour, commentContents, callback) {
    let newCommentBundle = new commentBundle({
        username: username,
        bundleName: bundleName,
        hashTag: hashTag,
        replyRate: replyRate,
        commentOrder: commentOrder,
        delay: delay,
        rushHour: rushHour,
        commentContents:commentContents
    });
    newCommentBundle.save(callback);
};

module.exports.updateCommentBundle = function (id, bundleName, hashTag, replyRate, commentOrder, delay, rushHour, commentContents, callback) {
    let updateCommentCustomer = {
        bundleName: bundleName,
        hashTag: hashTag,
        replyRate: replyRate,
        commentOrder: commentOrder,
        delay: delay,
        rushHour: rushHour,
        commentContents:commentContents
    };
    commentBundle.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};


module.exports.pause = function (id, callback) {
    let updateCommentCustomer = {
        runningState: false
    };
    commentBundle.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};

module.exports.unPause = function (id, callback) {
    let updateCommentCustomer = {
        runningState: true
    };
    commentBundle.findByIdAndUpdate(id, updateCommentCustomer, {new: true}, callback);
};

