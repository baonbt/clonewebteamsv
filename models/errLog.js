const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

//use schema
const ErrLogSchema = mongoose.Schema({
    time: {
        type: Number,
        require: true
    },
    user: {},
    infoDes: {
        type: String
    },
    errDes: {
        type: JSON
    },
    bundleId:{},
    fixStatus:{
        type: Boolean,
        require: true
    }
});

const ErrLog = module.exports = mongoose.model('ErrLog', ErrLogSchema);

module.exports.getErrLogById = function (id, callback) {
    ErrLog.findById(id, callback);
};

module.exports.getAllErrLog = function (callback) {
    const query = {};
    ErrLog.find(query, callback);
};

module.exports.addErrLog = function (user, infoDes, errDes, bundleId, callback) {
    let newErrLog = new ErrLog({
        time: new Date().getTime(),
        username: user,
        infoDes: infoDes,
        errDes: errDes,
        bundleId: bundleId,
        fixStatus: false
    });
    newErrLog.save(callback);
};

module.exports.errFixed = function (errId, callback) {
    const update = {fixStatus: true};
    ErrLog.findByIdAndUpdate(errId, update, {upsert:false, new: true}, callback);
};