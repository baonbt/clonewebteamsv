module.exports.Auth = function(roles){
    return function(req, res, next){
        if(req.user.accountLevel >= roles){
            return next();
        }
        res.json({error: 'require higher permission'});
        return next('Unauthorized');
    }
};